<?php

namespace App;
use App\User;

use Illuminate\Database\Eloquent\Model;

class Returned extends Model
{
    //
     //table
     protected $table = 'returned';
     //fields
     public function category(){
        return $this->belongsTo('App\Settings\Categories','category_id');
    }
    public function returnasset(){
        return $this->belongsTo('App\User','name');
    }
 
}
