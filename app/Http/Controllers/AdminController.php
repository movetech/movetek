<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PHPMailer;
use DB;
use App\User;
use App\Projects\Projects;
use App\Setttings\Categories;
use App\Assets\Inventory;
use App\Assets\Issuance;
use App\Settings\DepartmentsModel;
//use ConsoleTVs\Charts\Facades\Charts;
//use LaravelDaily\LaravelCharts\Classes\LaravelChart;
class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get all the completed projects
        $completed_projects = Projects::where('status','complete')->orderBy('subcounty','asc')->get();
       
        $department = DepartmentsModel::orderBy('id','asc')->get();
        //$test=DB::table('issuance')->select('*')->groupBy('department')->get();
        $departments = DB::table('issuance')
                 ->select('department', DB::raw('count(*) as total'))
                 ->groupBy('department')
                 ->get();
       
       $allprojects = DB::table('projects')
                 ->select('subcounty', DB::raw('count(*) as totals'))
                 ->groupBy('subcounty')
                 ->get();

           
      
        return view('admin.home', compact('allprojects','department','departments')); 

      
    }


}
