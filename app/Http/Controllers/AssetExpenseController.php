<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Assets\AssetsModel as Assets; //Assets Model
use App\Settings\Categories\CategoryTypeModel as Type;
use App\Assets\AssetStatusModel as AssetStatus;
use App\Settings\Categories; //Categories model
use App\Settings\ManufacturersModel as Manufacturers;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Settings\LocationsModel as Location;
use App\User;
use App\Assets\Issuance as Issue;
use App\Returned;
use App\Settings\SuppliersModel as Suppliers;
use App\Settings\FinancialYears as Year;
use App\Assets\Inventory as Inventory;
use App\Projects\Projects as Project;
use DB;

class AssetExpenseController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
    }
    public function showEpenses(){
          //get a list of all the available asset information
          //return 
          $latest_year=Year::max('id');
         $years=Year::where('id',$latest_year)->first();
         $financial_years=Year::all();
          $allassets = DB::table('inventory')
          ->join('categories', 'categories.id', '=', 'inventory.category_id')
          ->where('financial_year',$years->year)
          ->select('categories.name','financial_year', DB::raw('count(*) as totals'),DB::raw('sum(cost) as cost'))
          ->groupBy('category_id')
          ->get();
          //return $allassets;

          $budgets=Project::where('status','complete')->where('financial_year',$years->year)->orwhere('status','partially complete')->where('financial_year',$years->year)->orderBy('id','asc')->paginate(10);
          
          return view('admin.assets.asset-expenditure', compact('years','budgets','allassets','financial_years'));
    }
    public function filterYears($id){
        //get a list of all the available asset information
        //return 
       $years=Year::where('id',$id)->first();
       $year=$years->year;
       $financial_years=Year::all();
        $allassets = DB::table('inventory')
        ->join('categories', 'categories.id', '=', 'inventory.category_id')
        ->where('financial_year',$year)
        ->select('categories.name','financial_year', DB::raw('count(*) as totals'),DB::raw('sum(cost) as cost'))
        ->groupBy('category_id')
        ->get();
       if(!$allassets){
        $request->session()->flash('error','no information about that financial year!!');
        return redirect()->back();
       }

        $budgets=Project::where('status','complete')->where('financial_year',$year)->orwhere('status','partially complete')->where('financial_year',$year)->orderBy('id','asc')->paginate(10);
        
        return view('admin.assets.asset-expenditure', compact('years','budgets','allassets','financial_years'));
  }
}
