<?php

namespace App\Http\Controllers\Assets;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Assets\AssetsModel as Assets; //Assets Model
use App\Settings\Categories\CategoryTypeModel as Type;
use App\Assets\AssetStatusModel as AssetStatus;
use App\Settings\Categories; //Categories model
use App\Settings\ManufacturersModel as Manufacturers;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Settings\LocationsModel as Location;
use App\User;
use App\Assets\Issuance as Issue;
use App\Settings\FinancialYears as Year;
use App\Returned;
use App\Settings\SuppliersModel as Suppliers;
use App\Assets\Inventory as Inventory;
use App\Assets\AssetStatusModel as Status;
use App\Settings\Ministries;
use App\Settings\DepartmentsModel;
use App\Settings\LocationsModel;
use App\Asset; //Assets Model
use PDF;
class AssetsController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show_import_form(){
        return view('admin.assets.importexcelform');
    }
    public function import(){
        //import the excel file and store into the db

    }
    public function charts(){
       
        // return view('admin.home', compact('chart1'));
    }
    public function index()
    {
        //get a list of all the available assets information
        $asset = Assets::orderBy('id','asc')->paginate(10);
        return view('admin.assets.index', compact('asset'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //get the asset categories
        $categories = Categories::all();
        $types = Type::all();
        return view('admin.assets.create', compact(['categories', 'types']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //set rules for the incoming requests
        $asset_rules = array(
            'name'=>'required',
            'category_id'=>'required'
        );
        //perform the validations based on the set rules above
        $validator = Validator::make($request->all(), $asset_rules);
        if($validator->fails()){
            //validation rules not met, throw error
            $request->session()->flash('error',$validator->errors());
            return redirect()->back()->withInput();
        }else{
            //validation rules met, proceed to add new asset
            $asset  = new Asset;
            $asset->name = $request->input('name');
            $asset->category_id = $request->input('category_id');
            if($asset->save()){
                //new asset added successfully
                $request->session()->flash('success','Asset added');
                return redirect()->back();
            }else{
                //failed to add new asset, try again
                $request->session()->flash('error','Failed to add the asset, try again');
                return redirect()->back()->withInput();//->with('error','Failed to add asset'.''.$asset->errors());
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //find a single instance of an existing asset 
        $asset = Assets::find($id);
        $category_name = $asset->category->name;
        return view('admin.assets.show', compact('asset','category_name'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //create an instace of an Asset and show edit view
        $asset = Assets::find($id);
        $category_name =$asset->category->name;
        $category = Categories::all();
        return view('admin.assets.edit',compact('asset','category_name','category'));//->with(['asset'=>$asset, 'category'=>$category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //set the validation rules before performing any update
        $rules = [
            'name'=>'required',
            'available_balance'=>'required',
            'given_out'=>'required',
            'total'=>'required',
            'category_id'=>'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            //validation rules not met
            $request->session()->flash('error',$validator->errors());
            return redirect()->back()->withInput();
        }else{
            //validation rules met
            $asset = Assets::find($id);
            $asset->name = $request->input('name');
            $asset->available_balance = $request->input('available_balance');
            $asset->given_out = $request->input('given_out');
            $asset->total = $request->input('total');
            $asset->category_id = $request->input('category_id');
            if($asset->save()){
                $request->session()->flash('success','Information updated successfully');
                return redirect()->route('assets.list');
            }else{
                $request->session()->flash('error','Failed to perform the request');
                return redirect()->back()->withInput();
            }

        }
    }
    public function checkin(){
        $categories = Categories::all();
        $assets = Asset::all();
        $users=User::All();
        $locations=Location::All();
        return view('admin.assets.checkin',compact('categories','assets','users','locations'));
    }
    //issue the asset to user check if thhe asset is in stock
    public function Issue(Request $request){
     
        $rules = [
            'name'=>'required',
            'tag'=>'required',
            'location'=>'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            //validation rules not met
            return redirect()->back()->withInput()->with('error',$validator->errors());
        }

        if(strlen($request->input('tag'))<15){
            return redirect()->back()->withInput()->with('error',"wrong asset tag please scan again!!");
        }
        $user=User::where('id',$request->name)->first();
        //scanner used
        $tagged_asset=Inventory::where('tag',$request->tag)->where('state','not-deployed')->first();
        if(!$tagged_asset){
            return redirect()->back()->withInput()->with('error',"that item is not in our inventory, please check the asset tag again!");
        }
        $category_id=$tagged_asset->category_id;
        if($tagged_asset){
              //check if stock exist before you issue
        $available=Assets::where('category_id',$category_id)->first();
        $checkmodel=Issue::where('modelnumber',$tagged_asset->modelnumber)->first();
        if($available->available_balance==0){
            return redirect()->back()->withInput()->with('error',"that item no longer exist in stock");
        }
        $dept=User::where('id',$request->input('name'))->first();
        $issued = new Issue;
        $issued->name = $request->input('name');
        $issued->category_id=$tagged_asset->category_id;
        $issued->asset = $tagged_asset->asset_name;
        $issued->modelnumber = $tagged_asset->modelnumber;
        $issued->tag =$tagged_asset->tag;
        $issued->department =$dept->department;
        $issued->serialnumber = $tagged_asset->serialnumber;
        $issued->department_code=$user->department;
        $issued->ministry=$user->ministry;
        $issued->location = $request->input('location');

        if($issued->save()){
            $newbalance=$available->available_balance-1;
            $newgivenout=$available->given_out+1;
            Assets::where('category_id',$category_id)->first()->update(['available_balance'=>$newbalance,'given_out'=>$newgivenout]);
            Inventory::where('tag',$tagged_asset->tag)->first()->update(['state'=>'deployed']);
            return redirect()->route('assets.show.issued')->with('success','information updated successfully');
        }else{
            return redirect()->back()->withInput()->with('error','Failed to update asset information');
        }

        }
        //manual input
        else{
              //check if stock exist before you issue
        $asset_id=$request->asset_name;
        $available=Assets::where('category_id',$category_id)->first();
        $checkmodel=Issue::where('modelnumber',$request->modelnumber)->first();
        if($checkmodel){
            return redirect()->back()->withInput()->with('error',"that item is non-deployable");
        }
        if($available->available_balance==0){
            return redirect()->back()->withInput()->with('error',"that item no longer exist in stock");
        }
        $dept=User::where('name',$request->input('name'))>first();
        $issued = new Issue;
        $issued->name = $request->input('name');
        $issued->category= $request->input('category');
        $issued->asset = $available->name;
        $issued->modelnumber = $request->input('modelnumber');
        $issued->tag =$tagged_asset->tag;
        $issued->serialnumber = $request->input('serialnumber');
        $issued->department =$dept->department;
        $issued->ministry=$dept->ministry;
        $issued->location = $request->input('location');
        if($issued->save()){
            $newbalance=$available->available_balance-1;
            $newgivenout=$available->given_out+1;
            Assets::where('category_id',$category_id)->first()->update(['available_balance'=>$newbalance,'given_out'=>$newgivenout]);
            Inventory::where('tag',$tagged_asset->tag)->first()->update(['state'=>'deployed']);
            return redirect()->route('assets.show.issued')->with('success','information updated successfully');
        }else{
            return redirect()->back()->withInput()->with('error','Failed to update asset information');
        }

        }
      

    }
    public function showIssueList(){
        $issued=Issue::orderBy('id','asc')->paginate(10);
        return view('admin.assets.issuance-list',compact('issued'));
    }
    public function viewEachIssuance($id){
     $eachissuence=Issue::findorfail($id);
     return view('admin.assets.issuance-view',compact('eachissuence'));
    }
    public function checkOut(Request $request){
        $eachcheckout=Issue::where('tag',$request->tag)->first();
        $checkexisting=Returned::where('tag',$request->tag)->first();
        if($eachcheckout&&!$checkexisting){
        $checkout=new Returned;
        $checkout->name = $eachcheckout->name;
        $checkout->category_id= $eachcheckout->category_id;
        $checkout->asset = $eachcheckout->asset;
        $checkout->modelnumber = $eachcheckout->modelnumber;
        $checkout->serialnumber =$eachcheckout->serialnumber;
        $checkout->serialnumber =$eachcheckout->department_code;
        $checkout->tag =$eachcheckout->tag;
        $checkout->location =$eachcheckout->ministry;
        $checkout->location =$eachcheckout->location;
        
        $checkout->save();
        $available=Assets::where('category_id',$eachcheckout->category_id)->first();
        $newbalance=$available->available_balance+1;
        $newgivenout=$available->given_out-1;
        //return $eachcheckout->id;
        Assets::where('category_id',$eachcheckout->category_id)->first()->update(['available_balance'=>$newbalance,'given_out'=>$newgivenout]);
        Inventory::where('tag',$eachcheckout->tag)->first()->update(['state'=>'not-deployed']);
        $eachcheckout->delete();
        //return view('admin.assets.issuance-list')->with('success','checkout successful');
        $request->session()->flash('success','checkout successful');
                return redirect()->back();
        }
        else{
            $request->session()->flash('error','Failed to checkout the issued asset, try again');
                return redirect()->back();
        }
       
       }
       //delete an issued asset upon request
       public function removeIssuedAsset(Request $request, $id){
            $issued_asset = Issue::find($id);
            if($issued_asset->delete()){
                $request->session()->flash('success','The issued asset has been deleted successfully');
                return rediret()->back();
            }else{
                //if the action failed to be executed
                $request->session()->flash('error', 'Failed to delete the issued asset, try again');
                return redirect()->back();
            }
       }
       //show returned assets
     public function showReturned(){
     $returned_assets=Returned::orderBy('id','asc')->paginate(10);
     return view('admin.assets.returned-assets',compact('returned_assets'));
       }
       //implement search
       public function searchreturned(Request $request){
            if($request->ajax()){
                $output = "";
               
                $results = Returned::where('asset','LIKE','%'.$request->search.'%')
                                       ->orWhere('tag','LIKE','%'.$request->search.'%')
                                       ->orWhere('serialnumber','LIKE','%'.$request->search.'%')
                                       ->orWhere('location','LIKE','%'.$request->search.'%')
                                       ->orWhere('department_code','LIKE','%'.$request->search.'%')
                                       ->orWhere('ministry','LIKE','%'.$request->search.'%')
                                       ->orWhere('modelnumber','LIKE','%'.$request->search.'%')->get();
                                        
                if($results){
                    foreach($results as $value){
                        $output.="<tr>". 
                           '<td>'.$value->returnasset->name.'</td>'.
                            '<td>'.$value->asset.'</td>'.
                            '<td>'.$value->category->name.'</td>'.
                            '<td>'.$value->serialnumber.'</td>'.
                            '<td>'.$value->modelnumber.'</td>'.
                            '<td>'.$value->tag.'</td>'.
                           
                            '<td>'.$value->department_code.'</td>'.
                            '<td>'.$value->ministry.'</td>'.
                            '<td>'.$value->location.'</td>'.
                            '<td>'.$value->created_at->format('d/m/Y').'</td>'.
                           
                            "</tr>";
                            return Response($output);
                    }
                }else{
                    $output .='<tr>'.  
                   '<td align="center" colspan="5">'.'No matching records found'.'</td>'.
                    '</tr>';
                    return Response($output);
                }

            }
       }
       //list all the non deployable assets
       public function listAllNonDeployableAssets(){
           $non_deployable_assets = Inventory::where('status','Non-deployable')->get();
           return view('admin.assets.non-deployable-assets', compact('non_deployable_assets'));
       }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //show form to create new asset type
    public function createNewAssetType(){
        return view('admin.settings.categories.createNewType');
    }
    //creation of a new asset type
    public function addNewAssetType(Request $request){
        $validator = Validator::make($request->all(), array('asset_type'=>'required'));
        if($validator->fails()){
            $request->session()->flash('error',$validator->errors());
            return redirect()->back()->withInput();
        }else{
            $type = new Type;
            $type->type = $request->input('asset_type');
            if($type->save()){
                $request->session()->flash('success','Asset type added successfully');
                return redirect()->back();
            }else{
                $request->session()->flash('error','Failed to add Asset type');
                return redirect()->back();
            }
        }
    }
    //live search all  data
public function search(Request $request)
 
{
 
if($request->ajax())
 
{
 
$output="";
    $issues=Issue::where('serialnumber','LIKE','%'.$request->search."%")
          ->orWhere('asset','LIKE','%'.$request->search."%")
          ->orWhere('modelnumber','LIKE','%'.$request->search."%")
          ->orWhere('location','LIKE','%'.$request->search."%")
          ->orWhere('department','LIKE','%'.$request->search."%")
          ->orWhere('ministry','LIKE','%'.$request->search.'%')->get();
    if($issues)
    {
    foreach ($issues as $key => $issue) {
     
    $output.='<tr>'.
     
    '<td>'.$issue->id.'</td>'.
    '<td>'.$issue->category->name.'</td>'.
    '<td>'.$issue->asset.'</td>'.
    '<td>'.$issue->serialnumber.'</td>'.
    '<td>'.$issue->modelnumber.'</td>'.
    '<td>'.$issue->tag.'</td>'.
    '<td>'.$issue->user->name.'</td>'.
    '<td>'.$issue->department.'</td>'.
    '<td>'.$issue->ministry.'</td>'.
    '<td>'.$issue->location.'</td>'.
    '<td  class="pt-2-half">'. 
     '<a href="" style="float:left;" data-placement="top" data-toggle="tooltip" title="view">'.'<button class="btn btn-info btn-xs pull-right " data-title="view" data-toggle="modal" data-target="#view" >'.'<span class="fa fa-eye">'.'</span>'.'</button>'.'</a>'.
    '</td>'.
    '<td  class="pt-2-half">'.
        '<a href="" style="float:left;" data-placement="top" data-toggle="tooltip" title="view">'.'<button class="btn btn-info btn-xs pull-right " data-title="view" data-toggle="modal" data-target="#view" >'.'<span class="fa fa-edit">'.'</span>'.'</button>'.'</a>'.
    '</td>'.
    '<td  class="pt-2-half">'.
        '<p style="float:left;" data-placement="top" data-toggle="tooltip" title="Delete">'.'<button class="btn btn-danger btn-xs pull-right" data-title="Delete" data-toggle="modal" data-target="#delete" >'.'<span class="glyphicon glyphicon-trash">'.'</span>'.'</button>'.'</p>'.
    '</td>'.
    '<td  class="pt-2-half">'.
        '<a href="" style="float:left;" data-placement="top" data-toggle="tooltip" title="returned">'.'<button class="btn btn-sm btn-info btn-xs pull-right " data-title="view" data-toggle="modal" data-target="#view" >'.'Checkin(Returned)'.'</button>'.'</a>'.
    '</td>'.
  
    '</tr>';
    }
    return Response($output);
       }
        
   else{
    $output.='<tr>'.
    '<td align="center" colspan="5">'.'No Data matches your Search'.'</td>'.
'</tr>';
return Response($output);
   }
   }
  
}
/********************************** Inventory Logic ******************************** */
    public function Inventory(){
        $suppliers=Suppliers::All();
        $assets=Asset::All();
        $manufacturers = Manufacturers::all();
        $categories = Categories::all();
        $statuses = Status::all();
        $years = Year::where('id',1)->first();
        $types=Type::All();
        //$asset_statuses = AssetStatus::all();
        return view('admin.assets.inventory',compact(['suppliers','assets','manufacturers','categories','statuses','years','types']));
        //return view('admin.assets.inventory',compact(['suppliers','assets','manufacturers','categories','statuses']));

    }
    public function InventoryShow(){
    $stocks=Inventory::orderBy('id','asc')->paginate(10);
    return view('admin.assets.stock',compact('stocks'));
    }
    public function InventoryPost(Request $request){
        $rules = [
            'manufacturer'=>'required',
            'supplier'=>'required',
            'cost'=>'required',
            'tag'=>'nullable',
            'asset'=>'required',
            'purchase_date'=>'required',
            'warrant'=>'nullable',
            'status'=>'required',
            'delivery_date'=>'required',
            'model_number'=>'nullable',
            'serial_number'=>'nullable',
            'category'=>'required',
            'lifespan'=>'nullable',
            'asset_type'=>'nullable',
            'financial_year'=>'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            //validation rules not met
            return redirect()->back()->withInput()->with('error',$validator->errors());
        }
        $balance=Assets::where('category_id',$request->input('category'))->first();
        $inventory = new Inventory;
        $inventory->manufacturer= $request->input('manufacturer');
        $inventory->supplier= $request->input('supplier');
        $inventory->asset_name= $request->input('asset');
        $inventory->tag = $request->input('tag');
        $inventory->cost = $request->input('cost');
        $inventory->purchase_date= $request->input('purchase_date');
        $inventory->warrant= $request->input('warrant');
        $inventory->status= $request->input('status');
        $inventory->state="not-deployed";
        $inventory->delivery_date= $request->input('delivery_date');
        $inventory->modelnumber = $request->input('model_number');
        $inventory->serialnumber = $request->input('serial_number');
        $inventory->category_id = $request->input('category');
        $inventory->lifespan = $request->input('lifespan');
        $inventory->asset_type = $request->input('asset_type');
        //$inventory->category = $request->input('category');
        $inventory->financial_year = $request->input('financial_year');
        if($inventory->save()){
            if($balance){
                $newtotal=(int)$balance->total+1;
                $newcost=(int)$balance->cost+$request->input('cost');
                Assets::where('category_id',$request->input('category'))->first()->update(['total'=>$newtotal,'cost'=>$newcost]);

            }
            else{
                //save the asset to total assets if it is not found
                $asset  = new Assets;
                $asset->available_balance = 1;
                $asset->given_out = 0;
                $asset->total = 1;
                $asset->cost =$request->input('cost');
                $asset->category_id = $request->input('category');  
                $asset->save();
            }
            return redirect()->route('assets.inventory')->with('success','New Stock Added!');
        }else{
            return redirect()->back()->withInput()->with('error','Failed to update asset information');
        }

    }

    public function editInventory(Request $request, $id){
        $assets = Assets::all();
        $suppliers = Suppliers::all();
        $inventory = Inventory::find($id);
        $categories = Categories::all();
        $manufacturers = Manufacturers::all();
        $financial_years = Year::all();
        return view('admin.assets.inventoryEdit', compact(['assets','inventory','suppliers','categories','manufacturers','financial_years']));
    }

    public function updateInventory(Request $request, $id){
        $rules = array(
            'manufacturer'=>'required',
            'supplier'=>'required',
            'asset'=>'required',
            'tag'=>'required',
            'cost'=>'required',
            'model_number'=>'nullable',
            'serial_number'=>'nullable',
            'purchase_date'=>'date|required',
            'warrant'=>'required',
            'delivery_date'=>'required',
            'category'=>'nullable',
            'financial_year'=>'required'
        );
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            //failed to meet the set validation rules
            $request->session()->flash('error',$validator->errors());
            return redirect()->back();
        }else{
            //success on validations
            $inventory = Inventory::findOrFail($id);
            $inventory->manufacturer = $request->input('manufacturer');
            $inventory->supplier = $request->input('supplier');
            $inventory->asset_name = $request->input('asset');
            $inventory->cost = $request->input('cost');
            $inventory->modelnumber = $request->input('model_number');
            $inventory->serialnumber = $request->input('serial_number');
            $inventory->category = $request->input('category');
            $inventory->tag = $request->input('tag');
            $inventory->purchase_date = $request->input('purchase_date');
            $inventory->warrant = $request->input('warrant');
            $inventory->financial_year = $request->input('financial_year');
            $inventory->delivery_date = $request->input('delivery_date');
            if($inventory->save()){
                $request->session()->flash('success','Inventory information updated successfully');
                return redirect()->to(route('inventory.view'));
            }else{
                $request->session()->flash('error','Failed to update inventory information');
                return redirect()->back();
            }
        }
    }
      //live search all  data
public function searchAll(Request $request)
{
 
    if($request->ajax()){
 
$output="";
    $issues=Inventory::where('serialnumber','LIKE','%'.$request->search."%")
          ->orWhere('asset_name','LIKE','%'.$request->search."%")
          ->orWhere('modelnumber','LIKE','%'.$request->search."%")
          ->orWhere('manufacturer','LIKE','%'.$request->search."%")
          ->orWhere('supplier','LIKE','%'.$request->search."%")
          ->orWhere('asset_type','LIKE','%'.$request->search."%")
          ->orWhere('status','LIKE','%'.$request->search."%")
          ->orWhere('state','LIKE','%'.$request->search."%")
          ->orWhere('financial_year','LIKE','%'.$request->search."%")
          ->orWhere('cost','LIKE','%'.$request->search."%")
          ->orWhere('tag','LIKE','%'.$request->search."%")->get();
    if(count($issues)>0)
    {
    foreach ($issues as $issue) {
     
    $output.='<tr>'.
     
    '<td>'.$issue->manufacturer.'</td>'.
    '<td>'.$issue->supplier.'</td>'.
    '<td>'.$issue->cost.'</td>'.
    '<td>'.$issue->tag.'</td>'.
    '<td>'.$issue->asset_name.'</td>'.
    '<td>'.$issue->asset_type.'</td>'.
    '<td>'.$issue->category_id.'</td>'.
    '<td>'.$issue->serialnumber.'</td>'.
    '<td>'.$issue->modelnumber.'</td>'.
    '<td>'.$issue->purchase_date.'</td>'.
    '<td>'.$issue->warrant.'</td>'.
    '<td>'.$issue->status.'</td>'.
    '<td>'.$issue->state.'</td>'.
    '<td>'.$issue->lifespan.'</td>'.
    '<td>'.$issue->delivery_date.'</td>'.
    '<td>'.$issue->financial_year.'</td>'.
    '<td  class="pt-2-half">'. 
     '<a href="" style="float:left;" data-placement="top" data-toggle="tooltip" title="view">'.'<button class="btn btn-info btn-xs pull-right " data-title="view" data-toggle="modal" data-target="#view" >'.'<span class="fa fa-eye">'.'</span>'.'</button>'.'</a>'.
    '</td>'.
    '<td  class="pt-2-half">'.
        '<a href="" style="float:left;" data-placement="top" data-toggle="tooltip" title="view">'.'<button class="btn btn-info btn-xs pull-right " data-title="view" data-toggle="modal" data-target="#view" >'.'<span class="fa fa-edit">'.'</span>'.'</button>'.'</a>'.
    '</td>'.
    '<td  class="pt-2-half">'.
        '<p style="float:left;" data-placement="top" data-toggle="tooltip" title="Delete">'.'<button class="btn btn-danger btn-xs pull-right" data-title="Delete" data-toggle="modal" data-target="#delete" >'.'<span class="glyphicon glyphicon-trash">'.'</span>'.'</button>'.'</p>'.
    '</td>'.
  
    '</tr>';
    }
    return Response($output);
       }
        
   else{
    $output.='<tr>'.
    '<td align="center" colspan="5">'.'No Data matches your Search'.'</td>'.
    '</tr>';
   return Response($output);
   }
   }
  
}
    public function inventoryDetail($id){
        $inventory = Inventory::find($id);
        return view('admin.assets.inventoryDetail',compact('inventory'));
    }
    public function deleteInventory(Request $request, $id){
        $inventory = Inventory::find($id);
        if($inventory->delete()){
            //success
            $request->session()->flash('success','Inventory information deleted successfully');
            return redirect()->back();
        }else{
            //failed to delete inventory
            $request->session()->flash('error','Failed to delete the requested inventory information');
            return redirect()->back();
        }
    }
//*****************************  End of Inventory Logic ***********************/

//***************************** Financial Years  *******************************/
    //render to the admin the form to create financial years
    public function yearCreationForm(){
        return view('admin.settings.financialyears.financialYearCreationForm');
    }
    //list all the available financial Years
    public function getAllFinancialYears(){
        $financial_years = Year::latest()->paginate(1);
        return view('admin.settings.financialyears.financialYearsListing', compact('financial_years'));
    }
    //post a new financial year
    public function addNewFinancialYear(Request $request){
        //set and perform validations on the incoming request data
        //$validator = Validator::make($request->all(), array('financial_year'=>'required|max:7|unique'));
        $rules = array(
            'financial_year'=>'required|max:7',
        );
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            //validation rules not met, throw an error and show the user the same form
            $request->session()->flash('error',$validator->errors());
            return redirect()->back()->withInput();
        }else{
            $check=Year::where('year',$request->input('financial_year'))->first();
            if($check){
                $request->session()->flash('error','The financial year already exists, please try again');
                return redirect()->back()->withInput();
            }
            //the validation rules have been met, add this reuest data to db
            $financial_year = new Year;
            $financial_year->year = $request->input('financial_year');
            if($financial_year->save()){
                $request->session()->flash('success','The '.$request->input('financial_year').' '.'added successfully');
                return redirect()->back();
            }else{
                //failed to add the financial year, throw error and redirect the user
                $request->session()->flash('error','There was a problem adding the finacial year, please try again');
                return redirect()->back()->withInput();
            }
        }
    }
    
    //delete an existing financial year upon request
    public function removeExistingFinancialYear(Request $request, $id){
        $fyear = Year::findOrFail();
        if($fyear->delete()){
            //if the financial year is successfully deleted
            $request->session()->flash('success','The '.$fyear->first().' '.'has been deleted successfully');
            return redirect()->back();
        }else{
            //failed to delete the requested finacial year
            $request->session()->flash('error','Failed to delete the finacial year, try again');
            return redirect()->back();
            
    }
    }
    //update a financial year
    public function updateExistingFinancialYear(Request $request){
        //validate input data
        $validator = Validator::make($request->all(), array('financial_year'=>'required'));
        if($validator->fails()){
            $request->session()->flash('error', $validator->errors());
            return redirect()->back()->withInput();
        }else{
            $year = $request->id;
            if(Year::where('id',1)->update(['year'=>$request->input('financial_year')])){
                //if the update was successfull
                $request->session()->flash('success', 'The finacial year updated successfully');
                return redirect()->back();
            }else{
                //if the update was not successful
                $request->session()->flash('error','Failed to update the financial year, try again ');
                return redirect()->back();
            }
        }
                
    }
//***************************** End Financial Years ****************************/



    /***************************** Asset Statuses  ******************/
    //render the view that contains the asset status creation form
        public function showAssetStatusForm(){
            return view('admin.settings.statuses.statusCreationForm');
        }
        //add new asset status
        public function addNewAssetStatus(Request $request){
            //create validatio rules and check to confirm that the rules are met before submitting data
            $validator = Validator::make($request->all(), array(
                'status'=>'required|unique:asset_statuses',
                'description'=>'nullable'
            ));
            if($validator->fails()){
                //the validation rules have not been met, throw errow and redirect user
                $request->session()->flash('error',$validator->errors());
                return redirect()->back();
            }else{
                //the set rules have been met, proceed to add the request data
                $status = new AssetStatus;
                $status->status = $request->input('status');
                $status->description = $request->input('description');
                if($status->save()){
                    //if the new status has been added sucessfully, do this
                    $request->session()->flash('success','the '.$request->input('status').' '.'asset status has been added successfully ');
                    return redirect()->back();
                }else{
                    //if the request informatio failed ot be added, do this
                    $request->session()->flash('error','Failed to add the new asset type, try again');
                    return redirect()->back();
                }

            }
        }
        //list all the asset statuses
        public function getAssetStatuses(){
            $asset_statuses = AssetStatus::orderBy('id','asc')->paginate(10);
            return view('admin.settings.statuses.listAllAssetStatuses', compact('asset_statuses'));
        }
        //show the details of a single asset status
        public function getAssetStatus(){
            $status = AssetStatus::find($id);
            return view('admin.settings.statuses.listAllAssetStatuses', compact('status'));
        }
        //show the form to edit an existing asset status
        public function showAssetStatusEditForm($id){
            
        }
        //delete an existing asset status
        public function removeExistingAssetStatus(Request $request, $id){
            $asset_status = AssetStatus::find($id);
            if($asset_status->delete()){
                //if the action to delete the status wa successfull
                $request->session()->flash('success','The status has been deleted successfully');
                return redirect()->back();
            }else{  
                //if the action fails
                $request->session()->flash('error','Failed to delete the asset status');
                return redirect()->back();
            }
        }

    /****************************  End Assets Satuses ***************/
    public function destroy(Request $request, $id)
    {
        //$id=$request->id;
        //delete an instance of an asset class
       $asset = Assets::find($id);
        if($asset->delete()){
            //asset deleted 
            $request->session()->flash('success','Asset deleted successfully');
            return redirect()->to(route('assets.list'));
        }else{
            //if failed to delete asset, throw an error
            $request->session()->flash('error','Failed to perform th action, try again');
            return redirect()->back();
        }
    }
    //FILE DOWNLOADS
        /*************** Download all inventory data  */
    public function downloadAllAssets(){
        $stocks = Inventory::all();
        $data = array(
            'stocks'=>$stocks
        );
        $pdf = PDF::loadView('admin.assets.inventoryPDFDownload', $data);
        return $pdf->download('Inventory.pdf');
    }
    /**************** Download a list of all returned assets **********************/
    public function downloadReturnedAssets(){
        $returned_assets = Returned::all();
        $data = array(
            'returned_assets'=>$returned_assets
        );
        $pdf = PDF::loadView('admin.assets.returned-assets-downloads',$data);
        $filename = 'ReturnedAsssets.pdf';
        return $pdf->download('ReturnedAssets.pdf');
    }
    /****************** Downloads a list of all deployed/issued assets **************/
    public function pdfDownloadOfIssuedAssets(){
       $issued = Issue::all();
       $data = array(
            'issued'=>$issued
       );
       $pdf = PDF::loadView('admin.assets.issued-assets-downloads', $data);
       return $pdf->download('IssuedAssets.pdf');
    }
    /**************** Downloads pdf view of all stocked assets ********************/
    public function pdfDownloadOfStockedAssets(){
        $assets = Assets::all();
        $data = array('assets'=>$assets);
        $pdf = PDF::loadView('admin.assets.stocked-assets-downloads', $data);
        return $pdf->download('StockedAssets.pdf');
    }
}