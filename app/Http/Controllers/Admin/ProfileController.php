<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Iluminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use App\User;
use App\Admin;
use Image;
class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->middleware('auth:admin');
    }
    public function index()
    {
        $user = Auth::user();
        return view('user.profile', compact('user'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Admin::where('id',$id)->first();
        return view('admin.users.profile', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //get the currently logged in user
        $validator = Validator::make($request->all(),array(
            'name'=>'required',
            'email'=>'email|required',
            'photo'=>'nullable|image|mimes:jpeg,jpg,png,svg|max:2048'
        ));
        if($validator->fails()){
            $request->session()->flash('error', $validator->errors());
            return redirect()->back()->withInput();
        }else{
            $user = Admin::find($id);
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            if($request->hasFile('photo'))
        {
            //get file name with extension
            $fileNameWithExtension = $request->file('photo')->getClientOriginalName();
            //get just the file name
            $filename = pathinfo($fileNameWithExtension, PATHINFO_FILENAME);
            //get just the file extension
            $fileExtension = $request->file('photo')->getClientOriginalExtension();
            //file name to store to the database
            $fileNameToStore = $filename.'_'.time().'.'.$fileExtension; 
            //upload the image
            $imagePathToBoBeStored = $request->file('photo')->storeAs('public/images/admins/',$fileNameToStore);
            //$pathToFile = Storage::disk('public')->put('uploads/', $fileNameToStore);
    }

    else{
        $fileNameToStore = 'noimage.jpg';
    }
    $user->avartar = $fileNameToStore;
            if($user->save()){
                //user details updated successfully
                $request->session()->flash('success','Profile updated successfully');
                return redirect()->back();
            }else{
                //failed to update the user details
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
