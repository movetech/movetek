<?php

namespace App\Http\Controllers\User;

use App\Credit;
use App\User;
use PDF;
use App\Assets\AssetsModel as Assets;
use App\Assets\Issuance;
use App\Projects\Projects;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Settings\FinancialYears;
class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('web');
    }

    public function index()
    {
         $name=Auth::user()->name;
         $users=User::where('name', $name)->first();
        return view('user.home')->with(compact('users'));
    }
    public function showAllProjects(){
        $projects = Projects::orderBy('id','asc')->paginate(10);
        return view('user.projects.index', compact('projects'));
    }
    //show to the user a list of the completed projects
    public function getAllCompletedProjects(){
        $completed_projects = Projects::where('status','complete')->orderBy('id','asc')->get();
        return view('user.projects.completedProjectsListing', compact('completed_projects'));
    }
    //pdf download of completed projects
    public function downloadPdfViewOfCompletedProjects(){
        $completed_projects = Projects::where('status','complete')->orderBy('id','asc')->get();
        $year = FinancialYears::where('id',1)->first();
        $data = array(
            'completed_projects'=>$completed_projects,
            'year'=>$year
        );
        $pdf = PDF::loadView('user.projects.completedProjectsListingDownloads',$data);
        return $pdf->download('completed-projects.pdf');
    }
    //enable the user to doenload a pdf view of all patially completed projects
    public function downloadPdfViewOfPartiallyCompletedProjects(){
        $partially_completed_projects = Projects::where('status','partially complete')->orderBy('id','asc')->get();
        $year = FinancialYears::where('id',1)->first();
        $data = array(
            'projects'=>$partially_completed_projects,
            'financial_year'=>$year
        );
        $pdf = PDF::loadView('user.projects.partiallyCompletedProjectsListingDownloads', $data);
        return $pdf->download('Partially-completed-projects.pdf');
    }
    //get a full list of all the partially completed projects
    public function listAllPartiallyCompletedProjects(){
        $partially_completed_projects = Projects::where('status','partially complete')->orderBy('id','asc')->get();
        return view('user.projects.partiallyCompletedProjectsListing', compact('partially_completed_projects'));
    }
    public function getUserProfile(){
        $user = Auth::user();
        return view('user.profile.index', compact('user'));
    }
    public function getIssuedAssets(){
        //$issuance = Issuance::where('user_id',Auth::user()->id)->get();
        $user_id = Auth::user()->id;
       $issued_assets = Issuance::where('name',$user_id)->get();
        return view('user.Assets.index', compact('issued_assets'));
    }
    public function updateUserProfile(Request $request, $id)
    {
        //get the currently logged in user
        $validator = Validator::make($request->all(),array(
            'name'=>'required',
            'email'=>'email|required',
            'photo'=>'nullable|image|mimes:jpeg,jpg,png,svg|max:2048'
        ));
        if($validator->fails()){
            $request->session()->flash('error', $validator->errors());
            return redirect()->back()->withInput();
        }else{
            $user = User::find($id);
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            if($request->hasFile('photo'))
        {
            //get file name with extension
            $fileNameWithExtension = $request->file('photo')->getClientOriginalName();
            //get just the file name
            $filename = pathinfo($fileNameWithExtension, PATHINFO_FILENAME);
            //get just the file extension
            $fileExtension = $request->file('photo')->getClientOriginalExtension();
            //file name to store to the database
            $fileNameToStore = $filename.'_'.time().'.'.$fileExtension; 
            //upload the image
            $imagePathToBoBeStored = $request->file('photo')->storeAs('public/images/users/',$fileNameToStore);
            //$pathToFile = Storage::disk('public')->put('uploads/', $fileNameToStore);
    }

    else{
        $fileNameToStore = 'noimage.jpg';
    }
    $user->avartar = $fileNameToStore;
            if($user->save()){
                //user details updated successfully
                $request->session()->flash('success','Profile updated successfully');
                return redirect()->back();
            }else{
                //failed to update the user details
            }
        }
    }

}
