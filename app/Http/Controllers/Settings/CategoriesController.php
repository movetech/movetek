<?php

namespace App\Http\Controllers\Settings;
use App\Http\Controllers\Controller;
use DataTables;
use App\Settings\Categories;
use App\Settings\Categories\CategoryTypeModel as Type;
use Illuminate\Support\Facades\Validator;
use Redirect;
use Response;
use Session;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Categories::orderBy('id','asc')->paginate(10);
        return view('admin.settings.categories.index', compact('categories'));

    }
    public function  get_category_data(){
        $categories = Categories::select(
            'id',
            'name',
            'type'
        );
        return  DataTables::of($categories)->addColumn('action', function(){
            return "
                <a href='#' class='btn btn-success btn-sm'><i class='fa fa-eye'></i> View More</a>
            ";
        })->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category_types = Type::all();
        return view('admin.settings.categories.create', compact('category_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //perform validations of the incoming request data
        $rules = [
            'name'=>'required|unique:categories',
            'type'=>'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            //validaton rules not met, throw error
            $request->session()->flash('alert-danger',$validator->errors());
            return redirect()->back()->withInput();
        }else{
            //validation rules met, proceed to add new category
            $category = new Categories;
            $category->name = $request->input('name');
            $category->type = $request->input('type');
            if($category->save()){
                //success
                $request->session()->flash('alert-success','Category added successfully');
                return redirect()->back();
            }else{
                //failed to add category info
                $request->session()->flash('alert-danger','Failed to add asset');
                return redirect()->back()->withInput();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //query an instance of a single category
        $category = Categories::findOrFail($id);
        return view('admin.settings.categories.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //get a sinlge category
        $category = Categories::find($id);
        return view('admin.settings.categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validate request data
        $validation_rules = array(
            'name'=>'required',
            'type'=>'required'
        );
        $validator = Validator::make($request->all(), $validation_rules);
        if($validator->fails()){
            //failed to satisfy the set rules
            $request->session()->flash('alert-danger',$validator->errors());
            return redirect()->route('categories.edit')->withInput();
        }else{
            //update info, rules met
            $category = Categories::find($id);
            $category->name = $request->get('name');
            $category->type = $request->get('type');
            if($category->save()){
                //updation successful
                $request->session()->flash('alert-success','Category updated successfully');
                return redirect()->to(route('categories.index'));
            }else{
                //failed to update, throw error
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $category = Categories::find($id);
        if($category->delete()){
            //$request->session()->flash('alert-success','Category information deleted successfully');
            $request->session()->flash('alert-danger','Category deleted successfully');
            return redirect()->route('categories.index');
        }else{
            $request->session()->flash('alert-danger','Failed to delete the category information, try again');
            return redirect()->back();
        }
    }
}
