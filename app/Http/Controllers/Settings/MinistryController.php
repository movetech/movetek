<?php

namespace App\Http\Controllers\Settings;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use App\Settings\Ministries; //the ministry model
class MinistryController extends Controller
{
    //the constructor that ensures that the user accesing any page here has admin permissions
    public function __construct(){
        $this->middleware('auth:admin');
    }
    //list all the existing ministries
    public function  listAllMinistries(){
        $ministries = Ministries::orderBy('id','asc')->paginate(10);
        return view('admin.settings.ministries.index', compact('ministries'));
    }
    //show the form to enable the admin add new ministries
    public function showMinistryCreationForm(){
        return view('admin.settings.ministries.create');
    }
    //add a new ministry
    public function addNewMinistry(Request $request){
        //perform validation on the incoming request data
        $validator = Validator::make($request->all(), array('name'=>'required|unique:ministries'));
        if($validator->fails()){
            //the above set validation rule was not met
            $request->session()->flash('error',$validator->errors());
            return redirect()->back()->withInput();
        }else{
            //the validation rule has been met, proceed to add a new ministry
            $ministry = new Ministries;
            $ministry->name = $request->input('name');
            if($ministry->save()){
                //ministry added successfully
                $request->session()->flash('success',$ministry->name.' ministry added successfully');
                return redirect()->back();
            }else{
                //failed to add a new ministry
                $request->session()->flash('error','Failed to add ministry, please try again');
                return redirect()->back()->withInput();
            }
        }
    }
    //show the details of a single ministry
    public function ministryDetail($id){
        $ministry = Ministries::find($id);
        return view('admin.settings.ministries.ministryDetail', compact('ministry'));
    }
    //show the form to enable the admin edit existing ministry informtion
    public function  showMinistryEditForm($id){
        $ministry = Ministries::find($id);
        return view('admin.settings.ministries.editMinistryData', compact('ministry'));
    }
    //update the details of an existing ministry
    public function modifyMinistryDetails(Request $request, $id){
        //set and check validation rules
        $validator = Validator::make($request->all(), array('name'=>'required|unique:ministries'));
        if($validator->fails()){
            //rules not met
            $request->session()->flash('error',$validator->errors());
            return redirect()->back()->withInput();
        }else{
            //rules met proceed to update this particular ministry's details
            $ministry = Ministries::find($id);
            $ministry->name = $request->input('name');
            if($ministry->save()){
                $request->session()->flash('success','The information for the'.' '.$ministry->name.' '.'ministry has been updated succesfully');
                return redirect()->to(route('list-all-ministries'));
            }else{
                //failed to update the miistry information, throw error and show the form again
                $request->session()->flash('error', 'Failed to edit the ministry information, try again');
                return redirect()->back()->withInput();
            }

        }
    }
    //search for ministry
    public function searchMinistry(Request $request){
        if($request->ajax()){
            $output = "";
            $results = Ministries::where('name','LIKE','%'.$request->search.'%')->get();
            if($results){
                //if the search results to true
               foreach($results as $key=>$value){
                    $output.="<tr>".
                            '<td>'.$value->id .'</td>'.
                            '<td>'.$value->name.'</td>'.
                             '<td>'."<a href='{{ route('show-single-ministry' , $value->id) }' class='btn btn-info'><i class='fa fa-eye'></i> View</a>".'</td>'.
                            // '<td class="pt-2-half"><a href="{{ route("show-single-ministry") }}" class="btn btn-info"><i class="fa fa-eye"></i></a>.'</td>'
                        "</tr>";
                return Response($output);
               }
            }else{
                 //If no records found
            $output.= "<tr>".
            '<td class="alert alert-danger text-center">'.'No matching records found'.'</td>'.
            "</tr>";
            return Response($output);
            }
        }else{
            //do something else if the request is not an ajax request
            return Response('No matching records found');
        }
    }
    //delete en existing ministy upon request
    public function removeMinistry(Request $request, $id){
        $ministry = Ministries::find($id);
        if($ministry->delete()){
            //if the delete request has been perform successfully
            $request->session()->flash('success','The '.$ministry->name.' '.'ministry has been sucessfully deleted');
            return redirect()->back();
        }else{
            //if the delete request is not successfull
            $request->session()->flash('error','Failed to delete the '.$ministry->name.','.'please try again');
        }
    }
}
