<?php

namespace App\Http\Controllers\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Settings\LocationsModel as Locations;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Session;
use Redirect;
class LocationsController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $location = Locations::orderBy('id','asc')->paginate(10);
        return view('admin.settings.locations.index', compact('location'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.settings.locations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validations
        $location_rules = array(
            'name'=>'required|unique:locations',
            'manager'=>'required',
            'address'=>'required',
            'town'=>'required',
        );
        $validator = Validator::make($request->all(), $location_rules);
        if($validator->fails()){
            $request->session()->flash('alert-danger', $validator->errors());
            return redirect()->back()->withInput();
        }else{
            $new_location = new Locations;
            $new_location->name = $request->input('name');
            $new_location->manager = $request->input('manager');
            $new_location->address = $request->input('address');
            $new_location->town = $request->input('town');
            if($new_location->save()){
                $request->session()->flash('success',$request->input('name').' '.'Sub-County added successfully');
                return redirect()->back();
            }else{
                $request->session()->flash('error','Failed to add Sub-County, try again');
                return redirect()->back()->withInput();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $location = Locations::find($id);
        return view('admin.settings.locations.show', compact('location'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $location = Locations::find($id);
        return view('admin.settings.locations.edit', compact('location'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validations
        $location_rules = array(
            'name'=>'required',
            'manager'=>'required',
            'address'=>'required',
            'town'=>'required',
            'country'=>'required'
        );
        $validator = Validator::make($request->all(), $location_rules);
        if($validator->fails()){
            $request->session()->flash('error', $validator->errors());
            return redirect()->back()->withInput();
        }else{
            $location = Locations::find($id);
            $location->name = $request->name;
            $location->manager = $request->manager;
            $location->address = $request->address;
            $location->town = $request->town;
            $location->country = $request->country;
            if($location->save()){
                $request->session()->flash('success','Sub-County data updated sucessfully');
                return redirect()->to(route('locations.index'));
            }else{  
                $request->session()->flash('error','Failed to upate the Sub-County details,try again');
                return redirect()->back()->withInput();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $location = Locations::find($id);
        if($location->delete()){
            $request->session()->flash('success','Sub-County deleted successfully');
            return redirect()->route('locations.index');
        }else{
            $request->session()->flash('error','Failed to delete the particular Sub-County information');
            return redirect()->back();
        }
    }
}
