<?php

namespace App\Http\Controllers\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Settings\ManufacturersModel;
use Validator;
use Response;
use Redirect;
use Session;
class ManufacturersController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $manufacturer = ManufacturersModel::orderBy('id','asc')->paginate(10);
        return view('admin.settings.manufacturers.index', compact('manufacturer'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.settings.manufacturers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        //validate the data
        $rules = [
            'name'=>'required',
            'url'=>'required',
            'phone'=>'required',
            'email'=>'required','email',
            'image'=>'image','mimes:jpg, png, svg','max:2048'
          
        ];

        $validator = Validator::make($request->all(), $rules);
        //check if the validation rules are met
        if($validator->fails()){
            //rules not met, throw error
            $request->session()->flash('alert-danger', $validator->errors());
            return redirect()->back()->withInput();
        }else{  
            //rules met, proceed
            $manufacturer =  new ManufacturersModel;
            $manufacturer->name = $request->input('name');
            $manufacturer->url = $request->input('url');
            $manufacturer->phone = $request->input('phone');
            $manufacturer->email = $request->input('email');
            
              
        if($request->hasFile('image'))
        {
            //get file name with extension
            $fileNameWithExtension = $request->file('image')->getClientOriginalName();
            //get just the file name
            $filename = pathinfo($fileNameWithExtension, PATHINFO_FILENAME);
            //get just the file extension
            $fileExtension = $request->file('image')->getClientOriginalExtension();
            //file name to store to the database
            $fileNameToStore = $filename.'_'.time().'.'.$fileExtension; 
            //upload the image
            $imagePathToBoBeStored = $request->file('image')->storeAs('public/images/manufacturers/',$fileNameToStore);
            //$pathToFile = Storage::disk('public')->put('uploads/', $fileNameToStore);
    }

    else{
        $fileNameToStore = 'noimage.jpg';
    }
    $manufacturer->image = $fileNameToStore;
            if($manufacturer->save()){
                //success
                $request->session()->flash('alert-success','Manufacturer added successfully');
                return redirect()->back();
            }else{
                //failed to add maufacturer information
                $request->session()->flash('alert-danger','Failed to add manufacturer');
                return redirect()->back()->withInput();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $manufacturer = ManufacturersModel::find($id);
        return view('admin.settings.manufacturers.show', compact('manufacturer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $manufacturer = ManufacturersModel::findOrFail($id);
        return view('admin.settings.manufacturers.edit', compact('manufacturer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validations
        $rules = [
            'name'=>'required',
            'email'=>'required',
            'phone'=>'required',
            'url'=>'required',
            'image'=>'image|nullable|mimes:jpeg,jpg,png|max:2048'
        ];
        //validator
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            //failed the validation rules
            $request->session()->flash('alert-danger',$validator->errors());
            return redirect()->back()->withInput();
        }else{  
            //passed the validation rules, proceed to update information
            $manufacturer = ManufacturersModel::findOrFail($id);
            $manufacturer->name = $request->input('name');
            $manufacturer->email = $request->input('email');
            $manufacturer->phone = $request->input('phone');
            $manufacturer->url = $request->input('url');
            $prev_image = $manufacturer->image;
            if($request->hasFile('image')){
                $image = $request->file('image');
                $image_name = time().'.'.$image->getClientOriginalExtension();
                $path = public_path('/uploads/manufacturers/'.$image_name);
                $manufacturer->image = $image_name;
            }else{
                $manufacturer->image = $prev_image;
            }
            if($manufacturer->save()){
                $request->session()->flash('alert-success','data updated successfully');
                return redirect()->to(route('manufacturers.index'));
            }else{
                //failed to update manufacturer information
                $request->session()->flash('alert-danger','Failed to update the manufacturer information');
                return redirect()->back()->witnInput();
            }
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $manufacturer = ManufacturersModel::find($id);
        if($manufacturer->delete()){
            $request->session()->flash('alert-success','Manufacturer Information deleted successfully');
            return redirect()->route('manufacturers.index');
        }else{
            //failed to delete
            $request->session()->flash('alert-danger','Failed to delete information');
            return redirect()->back();
        }
    }
}
