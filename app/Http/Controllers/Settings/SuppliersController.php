<?php

namespace App\Http\Controllers\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Settings\SuppliersModel;
use Redirect;
use Response;
use Session;
use Illuminate\Support\Facades\Validator;
class SuppliersController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $supplier = SuppliersModel::orderBy('id','asc')->paginate(10);
        return view('admin.settings.suppliers.index', compact('supplier'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.settings.suppliers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validations
        $rules = [
            'name'=>'required',
            'contact_name'=>'nullable',
            'phone'=>'required',
            'email'=>'email|required',
            'url'=>'required',
            'notes'=>'nullable',
            'image'=>'image|mimes:jpeg,jpg,png,svg|max:2048'
        ];
        $request_validator = Validator::make($request->all(), $rules);
        if($request_validator->fails()){
            $request->session()->flash('alert-danger',$data_validator->errors());
            return redirect()->back()->withInput();
        }
        else
        {

            $supplier = new SuppliersModel;
            $supplier->name = $request->input('name');
            $supplier->contact_name = $request->input('contact_name');
            $supplier->phone = $request->input('phone');
            $supplier->email = $request->input('email');
            $supplier->url = $request->input('url');
            $supplier->notes = $request->input('notes');

            if($request->hasFile('image'))
        {
            //get file name with extension
            $fileNameWithExtension = $request->file('image')->getClientOriginalName();
            //get just the file name
            $filename = pathinfo($fileNameWithExtension, PATHINFO_FILENAME);
            //get just the file extension
            $fileExtension = $request->file('image')->getClientOriginalExtension();
            //file name to store to the database
            $fileNameToStore = $filename.'_'.time().'.'.$fileExtension; 
            //upload the image
            $imagePathToBoBeStored = $request->file('image')->storeAs('public/images/suppliers/',$fileNameToStore);
            //$pathToFile = Storage::disk('public')->put('uploads/', $fileNameToStore);
    }

    else{
        $fileNameToStore = 'noimage.jpg';
    }
    $supplier->image = $fileNameToStore;
            if($supplier->save()){
                $request->session()->flash('alert-success','Supplier added successfully');
                return redirect()->back();
            }else{
                $request->session()->flash('alert-danger','Failed to add supplier details, try again');
                return redirect()->back()->withInput();
            }

        }   

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $supplier = SuppliersModel::find($id);
        return view('admin.settings.suppliers.show', compact('supplier'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $supplier = SuppliersModel::findOrFail($id);
        return view('admin.settings.suppliers.edit', compact('supplier'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //perform validation on the incoming request data
        $rules = [
            'name'=>'required',
            'contact_name'=>'nullable',
            'phone'=>'required',
            'email'=>'email|required',
            'url'=>'required',
            'image'=>'image|mimes:jpeg,jpg,svg,png|max:2048|nullable'
        ];
        $data_validator = Validator::make($request->all(), $rules);
        if($data_validator->fails()){
            $request->session()->flash('alert-danger',$data_validator->errors());
            return redirect()->back()->withInput();
        }else{
            $supplier = SuppliersModel::find($id);
            $supplier->name = $request->input('name');
            $supplier->contact_name = $request->input('contact_name');
            $supplier->phone = $request->input('phone');
            $supplier->email = $request->input('email');
            $supplier->url = $request->input('url');
            $supplier->notes = $request->input('notes');
            $old_image = $supplier->image;
            //determine if the request has an image
            if($request->hasFile('image'))
            {
                //get file name with extension
                $fileNameWithExtension = $request->file('image')->getClientOriginalName();
                //get just the file name
                $filename = pathinfo($fileNameWithExtension, PATHINFO_FILENAME);
                //get just the file extension
                $fileExtension = $request->file('image')->getClientOriginalExtension();
                //file name to store to the database
                $fileNameToStore = $filename.'_'.time().'.'.$fileExtension; 
                //upload the image
                $imagePathToBoBeStored = $request->file('image')->storeAs('public/images/suppliers/',$fileNameToStore);
                //$pathToFile = Storage::disk('public')->put('uploads/', $fileNameToStore);
                $supplier->image = $fileNameToStore;
        }
    
        else{
            $supplier->image = $old_image;
        }
            if($supplier->save()){
                $request->session()->flash('alert-success','Supplier information updated');
                return redirect()->to(route('suppliers.index'))->with('success','Supplier information updated successfully');
            }else{
                return redirect()->back()->withInput()->with('error','Failed to upadte data, try again');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $supplier = SuppliersModel::find($id);
        if($supplier->delete()){
            return redirect()->to(route('suppliers.index'))->with('success','Supplier Detail deleted successfully');
        }else{
            //failed to delete the request
            $request->response()->flash('alert-danger','Failed to delete the supplier information');
            return redirect()->back()->withInput()->withErrors($supplier->errors());
        }
    }
}
