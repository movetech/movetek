<?php

namespace App\Http\Controllers\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Settings\DepartmentsModel as Department;
use Illuminate\Support\Facades\Validator;
use Session;
use Redirect;
use Response;
use DataTables;
class DepartmentsController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $department = Department::orderBy('id','asc')->paginate(10);
        return view('admin.settings.departments.index', compact('department'));
    }
    public function get_table_data(){
        $dep_data = Department::select(
            'id',
            'department_code',
            'manager'
        );
        return DataTables::of($dep_data)
                    ->addColumn('action', function($dep){
                        return "
                            <a href='{{ route('department.show', $dep->id) }}' class='btn btn-info btn-sm'><i class='fa fa-eye'></i> View</a>
                            <a href='{{ route('departement.edit', $dep->id) }}' class='btn btn-success btn-sm'><i class='fa fa-edit'></i> Edit</a>
                            
                        ";
                    })->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.settings.departments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //perform data validations
        $dep_rules = array(
            'department_code'=>'required|unique:departments',
            'manager'=>'nullable'
        );
        $dep_validator = Validator::make($request->all(), $dep_rules);
        if($dep_validator->fails()){
            $request->session()->flash('error',$dep_validator->errors());
            return redirect()->back()->withInput();
        }else{
            $dep = new Department;
            $dep->department_code = $request->input('department_code');
            $dep->manager = $request->input('manager');
            if($dep->save()){
                $request->session()->flash('success',$request->input('department_code').' '.'Department added successfuly');
                return redirect()->back();
            }else{
                $request->session()->flash('error','Failed to add the department, try again');
                return redirect()->back()->withInput();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dep_detail = Department::findOrFail($id);
        return view('admin.settings.departments.show')->with('department', $dep_detail);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $department = Department::find($id);
        return view('admin.settings.departments.edit', compact('department'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //data validations
        $validation_rules = [
            'department_code'=>'required|unique:departments',
            'manager'=>'nullable'
        ];
        $validator = Validator::make($request->all(), $validation_rules);
        if($validator->fails()){
            //validation rules not met, throw error
            $request->session()->flash('error', $validator->errors());
            return redirect()->back()->withInput();
        }else{
            //validations met, proceed to update info
            $departent = Department::find($id);
            $departent->department_code = $request->department_code;
            $departent->manager = $request->manager;
            if($departent->save()){
                $request->session()->flash('success', $request->input('department_code').' '.'Department information updated successfully');
                return redirect()->to(route('departments.index'));
            }else{
                $request->session()->flash('error','Failed to update the department information, try again');
                return redirect()->back()->withInput();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        //deleet as single instance aof an existing department information
        $departent = Department::find($id);
        if($departent->delete()){
            $request->session()->flash('success','Departments deleted successfully');
            return redirect()->to(route('departments.index'));
        }else{
            $request->session()->flash('error','Failed to delete department, try again');
            return redirect()->route('departments.index');
        }
    }
}
