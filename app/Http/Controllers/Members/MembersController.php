<?php

namespace App\Http\Controllers\Members;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Roles\Roles; //roles model
use App\Admin; //the admin model
use App\Members\Members; //members model
//use App\Users; //the users model
use App\Settings\DepartmentsModel; //departments model
use App\Settings\LocationsModel; //locations model
use App\Settings\Ministries as Ministry; //locations model
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Yajra\DataTables\DataTables;
use App\MailSetting;
//use Illuminate\Foundation\Auth\RegistersUsers;
use App\User;
class MembersController extends Controller
{
    //use RegistersUsers;
    //constructor
    public function __construct(){
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     */
    public function sendmailnotification($email,$pass,$text)
    {
        //require('../../../vendor/phpmailer');

        $emails=$email;
        $pass=$pass;
        $text=$text;

        $settings=MailSetting::all()->first();
        $host=$settings->host;
        $port=$settings->port;
        $username=$settings->username;
        $password=$settings->password;
        $fromaddress=$settings->fromaddress;
        $fromname=$settings->fromname;
        $subject=$settings->subject;

       /* $text->MsgHTML("Dear <b>$emails</b>, <br>Your login credentials are: User email: <br> 
        <b> $emails</b> and <br><br> User Password: <b> $pass</b><br>
        $footer<br>");*/

        $mail = new PHPMailer(true);
        try{

            // $mail->isSMTP();
            $mail->CharSet = 'utf-8';
            $mail->SMTPAuth =true;
            $mail->SMTPSecure = 'tls';
            $mail->Host = $host; //gmail has host > smtp.gmail.com
            $mail->Port = $port; //gmail has port > 587 . without double quotes
            $mail->Username = $username; //your username. actually your email
            $mail->Password = $password; // your password. your mail password
            //$mail->setFrom($request->email, $request->name);
            $mail->From = ($fromaddress);
            $mail->FromName = $fromname;
            // $mail->Subject = $request->subject;
            $mail->Subject = "Password Credentials";
            // $mail->MsgHTML($request->text);
            $mail->MsgHTML($text);

            $mail->addAddress($email );
            set_time_limit(60);

            $mail->send();
        }catch(phpmailerException $e){
            dd($e);
        }catch(Exception $e){
            dd($e);
        }
    }

    public function index()
    {
        $users = Members::orderBy('id','asc')->paginate(10);
       return view('admin.members.index', compact('users'));
    }
    public function get_all_members(){
        $users = User::select
        (
           'id', 'name','username','email','phone','location','department','status'
        );
        return DataTables::of($users)
                ->addColumn('action', function($user){
                    return 
                        "<a href='{{ route('member.show', $user->id) }}' class='btn btn-primary btn-sm'><i class='fas fa-eye'></i> View</a>
                        <a href='{{ route('member.edit', $user->id) }}' class='btn btn-success btn-sm'><i class='fas fa-edit'></i> Edit</a>
                        <a href='{{ route('member.delete', $user->id) }}' class='btn btn-danger btn-sm'><i class='fas fa-trash-alt'></i> Delete</a>
                    ";
                    
                })-make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $department = DepartmentsModel::all();
        $location = LocationsModel::all();
        $ministries = Ministry::all();
        return view('admin.members.create', compact('department','location','ministries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //set validations for the incoming requests
        $rules = array(
            'name'=>'required|string',
            'email'=>'required|string|email|unique:users',
            'username'=>'nullable|string',
            'phone'=>'required|phone|min:10|max:13|unique:users',
            'location'=>'required|string',
            'department' =>'required|string',
            'ministry' =>'nullable|string',
            'password'=>'required|string|min:6',
            'confirm_password'=>'required|string'
        );
        //check if the above rules have been met or not
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            $request->session()->flash('alert-danger',$validator->errors());
            return redirect()->back();
        }else{
            //the rules have been met, proceed to add user
            $user = new User;
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->username = $request->input('username');
            $user->phone = $request->input('phone');
            $user->location = $request->input('location');
            $user->department = $request->input('department');
            $user->ministry = $request->input('ministry');
            $password = $request->input('password');
            $confirm_password = $request->input('confirm_password');
            if($password != $confirm_password){
                $request->session()->flash('alert-danger','password mismatch');
                return redirect()->back()->withInput();
            }else{
                //password match
                $user->password = bcrypt($password);
            }
            $user->password = bcrypt($password);
            if($user->save()){
                //require_once('./vendor/autoload.php');
                //$this->sendmailnotification($user->email, $user->password, '');

                $request->session()->flash('alert-success','User added successfully');
                
                return redirect()->back();                
                //return redirect()->to(route('members.all'))->with('success','User added successfully');
            }else{
                $request->session()->flash('alert-danger','Failed to save the details');
                return redirect()->back();
                //failed to add new user
                //return redirect()->back()->withInput()->withErrors($user->errors());

            }
            
        }
    }
   
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $member = Members::find($id);
        $location = LocationsModel::all();
        $department = DepartmentsModel::all();
        return view('admin.members.edit', compact('member','location','department'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //set validation rules to be checked before updating details
        $this->validate($request, array(
            'name'=>'required',
            'username'=>'required',
            'email'=>'required',
            'phone'=>'required|phone',
            'location'=>'required',
            'department'=>'required'
        ));
        $user = Members::find($id);
        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->location = $request->location;
        $user->department = $request->department;
        if($user->save()){
            //user details updated successfully
            $request->session()->flash('alert-success','Staff details updated successfully');
            return redirect()->route('members.all');
        }else{
            //failed to update user details
            $request->session()->flash('alert-danger');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $user = Members::find($id);
        if($user->delete()){
            $request->session()->flash('alert-success','Staff member deleted successfully');
            return redirect()->route('members.all');
        }else{
            $request->session()->flash('alert-danger', 'Failed to delete staff member, try again');
            return redirect()->back();
        }
    }
    //******************************** */ADMINS****************************
    public function viewAdmins(){
        //view all admins
        $admins = Admin::orderBy('id','desc')->paginate(10);
        return view('admin.members.admins.index', compact('admins'));
    }
    public function createAdmin(){
        //render form to create an admin
        //get the roles
        $roles = Roles::all();
        return view('admin.members.admins.create', compact('roles'));
    }
    public function addAdmin(Request $request){
        //set rules for the incoming request data
        $rules = array(
            'name'=>'required',
            'email'=>'required|email',
            'rolesname'=>'required',
            'password'=>'required',
            'confirm_password'=>'required'
        );
        //perform validations
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            $request->session()->flash('alert-danger',$validator->errors());
            return redirect()->back();
        }else{
            //validation rules met, proceed
            $admin = new Admin;
            $admin->name = $request->name;
            $admin->email = $request->email;
            $admin->rolesname = $request->rolesname;
            $password = $request->input('password');
            $confirm_password = $request->input('confirm_password');
            if($password != $confirm_password){
                $request->session()->flash('alert-danger','Passwords do not match');
            }
            $admin->password = bcrypt($password);
            if($admin->save()){
                $request->session()->flash('alert-success','Admin added successfully');
                return redirect()->back();
            }else{
                //failed to add new admin
                $request->session()->flash('alert-danger','Failed to add new admin, try again');
                return redirect()->back();
            }
        }

    }
    public function showAdmin($id){
        //shiw a single adin
        $admin = Admin::find($id);
        return view('admin.members.admins.show', compact('admin'));
    }
    public function editAdmin($id){
        //show form to edit admin
        $admin = Admin::find($id);
        $roles = Roles::all();
        return view('admin.members.admins.edit', compact('admin','roles'));
    }
    public function updateAdmin(Request $request, $id){
        //edit an existing admin
        $rules = [
            'name'=>'required',
            'email'=>'required',
            'rolesname'=>'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            $request->session()->flash('alert-dager',$validator->errors());
            return redirect()->back();
        }else{
            $admin = Admin::find($id);
            $admin->name = $request->name;
            $admin->email = $request->email;
            $admin->rolesname = $request->rolesname;
            if($admin->save()){
                $request->session()->flash('alert-success','Admin info updated successfully');
                return redirect()->back();
            }else{
                $request->session()->flash('alert-danger','Failed to update the information');
                return redirect()->back();
            }
        }

    }
    public function deleteAdmin(Request $request,$id){
        //delete an admin upon request
        $admin = Admin::find($id);
        if($admin->delete()){
            $request->session()->flash('alert-success','Information deleted successfully');
            return redirect()->to(route('admins'));
        }else{
            $request->session()->flash('alert-danger','Failed to delete admin data');
            return redirect()->back();
        }
    }
}
