<?php

namespace App\Http\Controllers\Roles;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Roles\Roles;
use App\Admin;
use PHPMailer;
class RolesController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Roles::orderBy('id','asc')->paginate(10);
        return view('admin.roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //vaidation rules
        $rules = [
            'role'=>'required|unique:roles',
        ];
        //rules validator
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            //request do not meet the set rules
                $request->session()->flash('alert-danger',$validator->errors());
            return redirect()->back();
        }else{
            //requests succcessfuly met the validation rules
            $role = new Roles;
            $role->role = $request->input('role');
            if($role->save()){
                //role added successfully
                $request->session()->flash('alert-success', ' Addedd Successfully');
                return redirect()->back();
            }else{
                //failed to add role, show error message
                $request->session()->flash('alert-error', 'Failed to add the new role, try again');
                return redirect()->back();
            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Roles::find($id);
        return view('admin.roles.edit', compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,['role'=>'required']);
        $role = Roles::find($id);
        $role->role = $request->role;
        if($role->save()){
            $request->session()->flash('alert-success','Role updated successfully');
            return redirect()->route('roles.all');
        }else{
            $request->session()->flash('alert-danger','Role updated successfully');
            return redirect()->back();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

   /*  public function deleterole(Request $request,$id){    
        $role = Roles::findOrFail($id);
        if($role->delete()){
            $request->session()->flash('alert-success','Role deleted successfully');
        }else{
            $request->session()->flash('alert-danger','Faild to delete the role');
            return redirect()->to(route('roles.all'));
        }

    } */
    public function destroy(Request $request,$id){
        //delete role
        $role = Roles::findOrFail($id);
        if($role->delete()){
            $request->session()->flash('alert-success','Information deleted successfully');
            return redirect()->to(route('roles.all'));
        }else{
            $request->session()->flash('alert-danger','Failed to delete admin data');
            return redirect()->back();
        }
    }
    public function assigntask(){
        $tasks=Roles::all();

        return view('admin.roles.assigntask')->with(compact('tasks'));
    }
    public function submittaskrole(Request $request)
    
    {
        $role=$request->role;
        $task = implode(",", $request->get('task'));

        Admin::where('rolesname', $role)->update(['task' => $task ]);
        Roles::where('role', $role)->update(['task' => $task ]);
            $request->session()->flash('alert-success', ' Assigned Successfully! ');
            return redirect()->back();
    }
}
