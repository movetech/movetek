<?php

namespace App\Http\Controllers\Projects;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Settings\LocationsModel as Location;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Phases;
use App\Projects\SupplementaryBudgetsModel as Budget;
use App\Settings\DepartmentsModel;
use App\Projects\Projects as Project;
use App\Settings\FinancialYears;
use App\Settings\Ministries; //the ministry model
use PdfReport;
use DB;
use PDF;
class ProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index()
    {
        $projects = Project::orderBy('id','desc')->paginate(10);
        return view('admin.projects.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        $departments = DepartmentsModel::all();
        $ministries =Ministries::all(); //the ministry model
        $sub_counties=Location::all();
        //$financial_years=['2019/2020','2020/2021','2021/2022'];
        $financial_years= FinancialYears::all();

        return view('admin.projects-views.create', compact('departments','financial_years','ministries','sub_counties'));
    }
    //initiate project
    public function store(Request $request){
       
        $now = Carbon::now();
        $now->format('d/m/Y');
        $phases=['II','III','IV','V','VI','VII','VIII','IX','X'];
        //generate reference for projects
        $ref="REF:".$now.rand(1000,9999);
        $project_rules = array(
            'name' =>'required',
            'description'=>'required|max:50',
            'financial_year'=>'required',
            'contractor'=>'required',
            'total_amount' =>'required',
            'award_date'=>'required',
            'amount_paid'=>'required',
            'date_paid'=>'required',
            'amount_remaining'=>'nullable',
            'date_topay'=>'nullable',
            'sub_county'=>'required',
            'duration'=>'required',
        );
        $validator = Validator::make($request->all(), $project_rules);
        //perform the validations based on the set rules above
       
        if($validator->fails()){
            //validation rules not met, throw error
            $request->session()->flash('error',$validator->errors());
            return redirect()->back()->withInput();
        }
        else{
            if(!($request->payment)) {
                if(!($request->total_amount==$request->amount_paid)){
                    $request->session()->flash('error','For one phase project, the total amount must equal amount paid');
                    return redirect()->back()->withInput();
                }
            }
            //check if phases exist so that you add the array and the amount paid to equal the total budget
            if($request->payment) {
            $budget=(array_sum($request->payment)+$request->amount_paid)-$request->total_amount;
            if(!($budget==0)){
                $request->session()->flash('error','For one phase project, the total amount set aside for project must equal project cost!!');
                return redirect()->back()->withInput();
            }
        }
            //validation rules met, proceed to add new project
            $supplementary_budget = 0;
            $project  = new Project;
            $project->name = $request->input('name');
            $project->user = Auth::user()->id;
            $project->ref= $ref;
            $project->description = $request->input('description');
            $project->financial_year = $request->input('financial_year');
            $project->contractor = $request->input('contractor');
            $project->amount = $request->input('total_amount');
            $project->date_awarded = $request->input('award_date');
            $project->amount_paid= $request->input('amount_paid');
            $project->date_paid = $request->input('date_paid');
            $project->amount_remaining = $request->input('amount_remaining');
            $project->date_topay = $request->input('date_topay');
            $project->ministry = $request->input('ministry');
            $project->subcounty = $request->input('sub_county');
            $project->department_code = $request->input('department');
            $project->supplementary_budget = $supplementary_budget;
            $project->total_budget = $supplementary_budget + $request->input('total_amount');
            $project->duration = $request->input('duration');
            $project->status = "initiated";
            $project->save();
            //save phases
            if($request->next_payment_date){
                $supplementary_budget = 0;
                foreach($request->next_payment_date as $key=>$date){
                    $data[] =[
                        'REF'=>$ref,
                        'name' =>$request->input('name').'Phase:'.$phases[$key],
                        'amount' => $request->payment[$key],
                        'supplementary_budget'=>$supplementary_budget,
                        'phase_cost'=> $request->payment[$key] + $supplementary_budget,
                        'date_to_be_paid' =>$date,
                        'status' =>'pending',
                        'created_at'=>$now,
                        'updated_at'=>$now,
                       ]; 
                }
                Phases::insert($data);
            }
            
            if($project->save()){
                //new asset added successfully
                $request->session()->flash('success',$request->input('name').',Project initiated');
                return redirect()->back();
            }else{
                //failed to add new asset, try again
                $request->session()->flash('error','Failed to initiate project, try again');
                return redirect()->back()->withInput();//->with('error','Failed to add asset'.''.$asset->errors());
            }
        }
    }
    public function showAll(){
       $projects=Project::orderBy('id','asc')->paginate(10);
        return view('admin.projects-views.viewall',compact('projects'));
    }
    public function showPending(){
        $pendings=Project::where('status',"pending")->orderBy('id','asc')->paginate(10);
        return view('admin.projects-views.pending',compact('pendings'));
    }
    public function ongoingProjects(){
        //get a list of all the ongoing projects
        $ongoings=Project::where('status',"initiated")->orderBy('id','asc')->paginate(10);
        return view('admin.projects-views.ongoing',compact('ongoings'));
    }
    public function delayedProjects(){
        //list all the delayed projects
       
        return view('admin.projects-views.delayed');
    }
    public function completedProjects(){
        // show a list of all the completed projects
        $completes=Project::where('status',"complete")->orderBy('id','asc')->paginate(10);
        return view('admin.projects-views.completed',compact('completes'));
    }
   
    public function pending_projects(){
        //some logic here
    }
    public function  search_project(Request $request){
        //peroform the search
        //name, financial_year, contructor,date_awarded, date_paid, status
        if($request->ajax()){
            $output = "";
            $search_results = Project::where('name','LIKE','%'.$request->search.'%')
                            ->orWhere('ministry','LIKE','%'.$request->search.'%')
                            ->orWhere('contractor','LIKE','%'.$request->search.'%')
                             ->orWhere('subcounty','LIKE','%'.$request->search.'%')
                            ->orWhere('department_code','LIKE','%'.$request->search.'%')
                            ->orWhere('status','LIKE','%'.$request->search.'%')
                            ->get();
        if(count($search_results) > 0){
            //if matching records are found
            foreach($search_results as $key=>$value){
                $phases = App\Phases::where('REF', $value->REF)->get();
                $output.='<tr>'.
                '<td>'.$value->id.'</td>'.
                '<td>'.$value->name.'</td>'.
                // '<td>'.$value->description.'</td>'.
                '<td>'.$value->financial_year.'</td>'.
                '<td>'.$value->ministry.'</td>'.
                '<td>'.$value->subcounty.'</td>'.
                '<td>'.$value->department_code.'</td>'.
                '<td>'.$value->contractor.'</td>'.
                '<td>'.$value->amount.'</td>'.
                '<td>'.$value->date_awarded.'</td>'.
                '<td>'.$value->amount_paid.'</td>'.
                // '<td>'.$value->date_paid.'</td>'.
                '<td>'.$value->amount_remaining.'</td>'.
                // '<td>'.$value->date_topay.'</td>'.
                '<td>'.$value->duration.'</td>'.
                '<td>'.$value->supplementary_budget.'</td>'.
                '<td>'.$value->phase. '</td>'
    
                    .'</td>'.
                '<td>'.$value->total_budget.'</td>'.
                '<td>'.$value->status.'</td>'.
                '<td>'."<a href='{{ route('projects.show', $value->id) }}' class='btn btn-info btn-xs'><span class='fa fa-eye'></span></a>".'</td>'.
                '<td>'."<a href='{{ route('edit-project', $value->id) }' class='btn btn-success btn-xs'><span class='fa fa-pencil'></span></a>".'</td>'.
                '<td>'."<form method='post' action='{{ route('delete-project', $value->id) }}'>
                                <input type='hidden' name='_method' value='DELETE'/>
                                <input type='hidden' name='_token' value='{{ csrf_token() }}'/>
                                <button  type='submit' class='btn btn-danger btn-xs pull-right' onclick='if (!confirm('Are you sure you want to proceed?')) { return false }'><span class='fa fa-trash'></span></button>
                                </form>".
                '</td>'.
                '</tr>';

                return Response($output);

            }
        }else{
            //If no records found
            $output.= "<tr>".
            '<td class="alert alert-danger text-center">'.'No matching records found'.'</td>'.
            "</tr>";
            return Response($output);
        }
        }
        
    }
    public function show($id)
    {
        //
        $specific_project=Project::findorfail($id);
        $ref=$specific_project->REF;
        //get phases associated with the project
        $phases=Phases::where('REF',$ref)->get();
        return view('admin.projects-views.show',compact('specific_project','phases'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$ref)
    {
        $projects  = Project::where('REF',$ref)->first();
        $departments = DepartmentsModel::all();
        return view('admin.projects-views.edit', compact(['projects','departments']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $ref)
    {
        $now = Carbon::now();
        $now->format('d/m/Y');
        $phases=['II','III','IV','V','VI','VII','VIII','IX','X'];
        //$ref="REF:".$now.rand(1000,9999);
        //$ref="REF:".$now.rand(1000,9999);

        $project_rules = array(
            'name' =>'required',
            'description'=>'required',
            'financial_year'=>'required',
            'contractor'=>'required',
            'total_amount' =>'required',
            'award_date'=>'required',
            'amount_paid'=>'required',
            'date_paid'=>'required',
            'amount_remaining'=>'nullable',
            'date_topay'=>'nullable',
            'duration'=>'required',    
        );
        $validator = Validator::make($request->all(), $project_rules);
        if($validator->fails()){
            $request->session()->flash('error', $validator->errors());
        }else{
            $project  = Project::where('REF',$ref)->first()->update([
                'name' => $request->input("name"),
                'description' => $request->input('description'),
                'financial_year' => $request->input('financial_year'),
                'contractor' => $request->input('contractor'),
                'amount' => $request->input('total_amount'),
                'date_awarded' => $request->input('award_date'),
                'amount_paid' => $request->input('amount_paid'),
                'date_paid'  => $request->input('date_paid'),
                'amount_remaining' => $request->input('amount_remaining'),
                'date_topay' => $request->input('date_topay'),
                'ministry' => $request->input('ministry'),
                'subcounty' => $request->input('subcounty'),
                'department_code' => $request->input('department_code'),
                'duration' => $request->input('duration'),
                'status' => "initiated"

            ]);
           
//            $project->save();
            if($request->next_payment_date){
                /* foreach($request->next_payment_date as $key=>$date){
                    $data[] =[
                        'ref'=>$ref,
                        'name' =>$request->input('name').'Phase,'.$key,
                        'amount' => $request->payment[$key],
                        'disbursed'=>0,
                        'balance'=> $request->payment[$key],
                        'date_to_be_paid' =>$date,
                        'status' =>'pending',
                        'created_at'=>$now,
                        'updated_at'=>$now,
                       ]; 
                } */
                $phases_count = Phases::where('REF',$ref)->count();
                for($i = 0; $i<= $phases_count; $i++){
                   /*  $data[] =[
                        'ref'=>$ref,
                        'name' =>$request->input('name').'Phase,'.$i,
                        'amount' => $request->input('payment'),
                        'disbursed'=>0,
                        'balance'=> $request->input('amount_remaining'),
                        'date_to_be_paid' =>$request->input('date_paid'),
                        'status' =>'pending',
                        'created_at'=>$now,
                        'updated_at'=>$now,
                       ];  */
                 $phase =   Phases::where('REF',$ref)->update([
                        'ref'=>$ref,
                        'name' =>$request->input('name').'Phase,'.$i,
                        'amount' => 4000,
                        'disbursed'=>0,
                        'balance'=> 3000,
                        'date_to_be_paid' =>$request->input('date_paid'),
                        'status' =>'pending',
                        'created_at'=>$now,
                        'updated_at'=>$now,
                    ]);
                    
                }
            }
            if($phase){
                $request->session()->flash('success',$request->input('name').',Project updated');
                return redirect()->to(route('projects.showall'));
            }else{
                $request->session()->flash('error','Failed to update project, try again');
                return redirect()->back();
            }
        }
    }
    public function Mark(Request $request,$ref){
        $checkpending=Phases::where('REF',$ref)->where('status','pending')->first();
        $checkinitiated=Phases::where('REF',$ref)->where('status','initiated')->first();
   if($checkpending||$checkinitiated){

       Project::where('REF',$ref)->first()->update(['status'=>'partially complete']);
       $request->session()->flash('success','The project is marked as partially complete due to incomplete phases!!');
       return redirect()->back();
   }
   else{
    Project::where('REF',$ref)->first()->update(['status'=>'complete']);
    $request->session()->flash('success','The project is marked complete!!');
    return redirect()->back();
   }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$ref)
    {
        $project = Project::where('REF',$ref)->first()->delete();
        $phases=Phases::where('REF',$ref)->delete();
        if($project){
            $request->session()->flash('success','Project deleted succesfully');
            return redirect()->back();
        }else{
            $request->session()->flash('error','Faied to delete project');
            return redirect()->back();
        }
    }
    //******************************** */ Supplementary Budgeting section **********************//
    public function create_supplementary_budget($ref,$count){
        //show form for the supplmenyary budgetting
        $project = Project::where('REF',$ref)->first();
        $project_phases = Phases::where('REF',$project->REF)->get();
        return view('admin.projects-views.new-supplementary-budget', compact(['project','project_phases','count']));
    }
    //activate phase payment
    public function ActivatePayment(Request $request,$id,$ref){
        $check_ongoing=Project::where('REF',$ref)->first();
        if($check_ongoing){
           if($check_ongoing->status=="initiated"){
            $request->session()->flash('error',"You cannot Activate this phase before the first phase is complete!");
            return redirect()->back()->withInput();
           }
           $activated=Phases::where('id',$id)->first()->update(['status'=>'initiated']);
           $request->session()->flash('success','Initiated successfully');
                  return redirect()->back();
        }
   
    }
    public function MarkPhase(Request $request,$id,$ref){
        //check if the phase is the last one and mark as complete project and phases
        $check_last_phase=Phases::where('REF',$ref)->max('id');
        if($check_last_phase==$id){
            Project::where('REF',$ref)->first()->update(['status'=>'complete']);
            $check_incomplete=Phases::where('id',$id)->first()->update(['status'=>'complete']);
            $request->session()->flash('success','phase marked as complete');
            return redirect()->back();  
        }
        else{
            $check_incomplete=Phases::where('id',$id)->first()->update(['status'=>'complete']);
            $request->session()->flash('success','phase marked as complete');
            return redirect()->back();
        }
        
    }
    //budget information
   public function Budget(){
    $budgets=Project::where('status','complete')->orwhere('status','partially complete')->orderBy('id','desc')->paginate(10);
    return view('admin.projects-views.budget',compact('budgets'));
   }
   //downlods of all the county projects
   public function downloadAllProjects(Request $request){
       $projects = Project::all();
       $financialyear = FinancialYears::all();
       if(count($projects)>0){
        $data = array(
            'projects'=>$projects,
            'year'=>$financialyear
        );
        $pdf = PDF::loadView('admin.projects-views.downloadAllProjects', $data);
        return $pdf->download('ManderaCountyProjects.pdf');
       }
       else{
        $request->session()->flash('error',"No running projects!");
        return redirect()->back(); 
       }
       
   }
   //downloads a pdf view of all the completed projects
   public function downloadAllCompletedProjects(){
       $completes = Project::where('status','complete')->get();
       $data = array('completes'=>$completes);
       $pdf = PDF::loadView('admin.projects-views.downloadAllCompletedProjects', $data);
       return $pdf->download('CompletedProjects.pdf');
   }
   //downloads a pdf view all ogoing porjects(intitiated and partially complete)
   public function downloadAllOngoingProjects(){
        $ongoings = Project::where('status','initiated')
                            ->orWhere('status','partially complete')->get();
        $data = array('ongoings'=>$ongoings);
        $pdf = PDF::loadView('admin.projects-views.downloadAllOngoingProjects', $data);
        return $pdf->download('AllOngoingProjects.pdf');
   }
   public function pdfviewBudget(Request $request)
   {
    //$budgets=Project::where('status','complete')->orwhere('status','partially complete')->orderBy('id','asc')->paginate(10); 
    $projects=Project::orderBy('id','asc')->paginate(10);
    if(count($projects)>0){
        view()->share('projects', $projects);  
        if($request->has('download')){
               $pdf = PDF::loadView('admin/projects-views/viewall');
               return $pdf->download('budget.pdf');
           }
           return view('pdfview');
    }
    else{
        $request->session()->flash('error',"No running projects!");
        return redirect()->back();   
    }
   
   }
    public function addBudget(Request $request, $ref){
        //add a new supplementary budget
        $rules = array(
            'project'=>'required',
            'phase'=>'nullable',
            'amount'=>'required'
        );
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            //validation rules not met, throw error
            $request->session()->flash('error',$validator->errors());
            return redirect()->back()->withInput();
        }else{
            $project = Project::where('REF',$ref)->first();
            //check if the project has phases
            if($request->count==1){
               
                //check if the project is not complete
                if($project->status=="initiated"){
                    //check if the project has not been supplemented before
                    if($project->supplementary_budget==0){
                        $supplementary_budget = new Budget;
                        $supplementary_budget->REF = $ref;
                        $supplementary_budget->project = $request->input('project');
                        $supplementary_budget->phase = $request->input('phase');
                        $supplementary_budget->amount = $request->input('amount');
                        $supplementary_budget->save();
                        $new_projectcost=$project->amount_paid+$request->input('amount');
                        Project::where('REF',$project->REF)->where('status','initiated')->first()->update(['supplementary_budget'=>$request->input('amount'),'total_budget'=>$new_projectcost]);
                        $request->session()->flash('success','Supplementary Budget added successfully');
                        return redirect()->back();
                    }
                    else{
                        $request->session()->flash('error',"You cannot supplement a project more than once!");
                        return redirect()->back()->withInput(); 
                    }
                  
                }
                else{
                    $request->session()->flash('error',"You cannot supplement a completed project !");
                    return redirect()->back()->withInput(); 
                }
            }
            //if the project has phases and either the first phase or the other phases is selected
            else{
                //check phase to be supplemented
                $child_phases=Phases::where('REF',$ref)->where('name',$request->input('phase'))->first();
                if(!$child_phases){
                 //check if the project is not complete
                if(!($project->status=="partially complete")||!($project->status=="complete")){
                    //check if the project has not been supplemented before
                    if($project->supplementary_budget==0){
                        $supplementary_budget = new Budget;
                        $supplementary_budget->REF = $ref;
                        $supplementary_budget->project = $request->input('project');
                        $supplementary_budget->phase = $request->input('phase');
                        $supplementary_budget->amount = $request->input('amount');
                        $supplementary_budget->save();
                        $new_projectcost=$project->amount_paid+$request->input('amount');
                        Project::where('REF',$project->REF)->first()->update(['supplementary_budget'=>$request->input('amount'),'total_budget'=>$new_projectcost]);
                        $request->session()->flash('success','Supplementarys Budget added successfully');
                        return redirect()->back();
                    }
                    else{
                        $request->session()->flash('error',"You cannot supplement a project more than once!");
                        return redirect()->back()->withInput(); 
                    }
                  
                }
                else{
                    $request->session()->flash('error',"You cannot supplement a completed project !");
                    return redirect()->back()->withInput(); 
                }
                }
                //the selected phase is either second or third....nth
                else{
                    //check if the phase has been initiated
                    if($child_phases->status=="initiated"){
                        //check whether it has been supplemented before
                        if($child_phases->supplementary_budget==0){
                       $supplementary_budget = new Budget;
                       $supplementary_budget->REF = $ref;
                       $supplementary_budget->project = $request->input('project');
                       $supplementary_budget->phase = $request->input('phase');
                       $supplementary_budget->amount = $request->input('amount');
                       $supplementary_budget->save();
                       $newphasecost=$request->amount+$child_phases->amount;
                       Phases::where('REF',$ref)->where('name',$request->input('phase'))->first()->update(['supplementary_budget'=>$request->input('amount'),'phase_cost'=>$newphasecost]);
                       $request->session()->flash('success','Supplement Budget added successfully'); 
                       return redirect()->back();
                        }
                        //the phase has been supplemented before
                        else{
                            $request->session()->flash('error',"You cannot supplement a project more than once!");
                            return redirect()->back()->withInput(); 
                        }
                        

                    }
                    //phase not initiated
                    else{
                        $request->session()->flash('error',"You cannot supplement a completed phase or phase not yet started!");
                        return redirect()->back()->withInput(); 
                    }
                    
                  

                }

            }
           
            //validation rules met, proceed to add new supplementary budget
            //check if the project to be supplemented is complete or not
           //$project = Project::where('REF',$ref)->first();
           //
           //
           // //check if the project to be supplemented had been supplemented
           //$project_with_phases = Project::where('REF',$ref)->first();
           //if($project_with_phases->status=="initiated"||$project_with_phases->status=="partially complete"){
           //    if(!($project_with_phases->supplementary_budget==0)){
           //        $request->session()->flash('error',"You cannot supplement a project more than once!");
           //        return redirect()->back()->withInput(); 
           //    }
           //}
           ////check if the phase is complete or not
           //$phase_incomplete=Phases::where('REF',$ref)->where('name',$request->input('phase'))->first();
           //if($phase_incomplete){
           //    if($phase_incomplete->status=="complete"){
           //        $request->session()->flash('error',"You cannot supplement a completed phase or phase not yet started!");
           //        return redirect()->back()->withInput(); 
           //    }
           //}
           //
           ////check if the phase has been supplemented before
           //if($phase_incomplete){
           //    if(!($phase_incomplete->disbursed==0)){
           //        $request->session()->flash('error',"You cannot supplement a phase more than once!");
           //        return redirect()->back()->withInput(); 
           //    }
           //}
           //if($project->status=="complete"){
           //    $request->session()->flash('error',"You cannot supplement a completed project !");
           //    return redirect()->back()->withInput(); 
           //}
           //$supplementary_budget = new Budget;
           //$supplementary_budget->project = $request->input('project');
           //$supplementary_budget->phase = $request->input('phase');
           //$supplementary_budget->amount = $request->input('amount');
           //if($supplementary_budget->save()){
           //   //
           //   $check_continuing_phases=Phases::where('REF',$ref)->where('name',$request->input('phase'))->first();
           //    if(!($check_continuing_phases=="initiated")){
           //        $new_projectcost=$project->amount_paid+$request->input('amount');
           //        Project::where('REF',$project->REF)->where('status','initiated')->first()->update(['supplementary_budget'=>$request->input('amount'),'total_budget'=>$new_projectcost]);
           //        $request->session()->flash('success','Supplementary Budget added successfully');
           //    }
           //    else{
           //        $newphasecost=$request->amount+$phase_incomplete->amount;
           //        Phases::where('REF',$ref)->where('name',$request->input('phase'))->where('status','initiated')->first()->update(['disbursed'=>$request->input('amount'),'phase_cost'=>$newphasecost]);
           //        $request->session()->flash('success','Supplementary Budget added successfully'); 
           //    }
           //    return redirect()->back();
           //}else{
           //    //failed to add supplemenary budget
           //    $request->session()->flash('error','Failed to add supplementary budget');
           //    return redirect()->back();
           //}
        }
    }
    //********************************* End Supplementary Budgetting Section *****************//
}
