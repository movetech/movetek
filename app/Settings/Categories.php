<?php

namespace App\Settings;

use Illuminate\Database\Eloquent\Model;
use App\Assets\Inventory;
use App\Assets\AssetsModel;
use App\Assets\Issuance;
use App\Returned;
class Categories extends Model
{
    //the table referenced by this model
    protected $table = 'categories';
    //add fillable table columns
    protected $fillable = [
        'name',
        'type'
    ];
    
    //category_asset relationship
    public function asset(){
        return $this->hasMany(AssetsModel::class);
    }
    public function inventory(){
        return $this->hasMany('App\Assets\Inventory');
    }
    public function returned(){
        return $this->hasMany('App\Assets\Issuance');
    }
}
