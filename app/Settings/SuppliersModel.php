<?php

namespace App\Settings;

use Illuminate\Database\Eloquent\Model;

class SuppliersModel extends Model
{
    //table
    protected $table = 'suppliers';
    //filds
    protected $fillable = [
        'name','contact_name','phone','email','url','notes','image'
    ];
}
