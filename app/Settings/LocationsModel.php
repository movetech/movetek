<?php

namespace App\Settings;

use Illuminate\Database\Eloquent\Model;

class LocationsModel extends Model
{
    //table
    protected $table = 'locations';
    //fields
    protected $fillable = [
        'name','manager','address','town','country'
    ];
}
