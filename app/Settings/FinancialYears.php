<?php

namespace App\Settings;

use Illuminate\Database\Eloquent\Model;

class FinancialYears extends Model
{
    //table
    protected $table = 'financial_years';

    //fields fillable
    protected $fillable = ['year'];
}
