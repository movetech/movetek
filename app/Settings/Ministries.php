<?php

namespace App\Settings;

use Illuminate\Database\Eloquent\Model;

class Ministries extends Model
{
    //declare the table used by this model
    protected $table = 'ministries';

    //the fillable data
    protected $fillable = ['name'];
}
