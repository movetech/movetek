<?php

namespace App\Settings;

use Illuminate\Database\Eloquent\Model;

class ManufacturersModel extends Model
{
    //declare the table associated with this model
    protected $table = 'manufacturers';
    //fillable fields
    protected $fillable = [
        'name', 'url','phone','email','image'
    ];
}
