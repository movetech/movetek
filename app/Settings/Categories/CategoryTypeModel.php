<?php

namespace App\Settings\Categories;

use Illuminate\Database\Eloquent\Model;

class CategoryTypeModel extends Model
{
    protected $table = 'category_types';

    protected $fillable = ['type'];
}
