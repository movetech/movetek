<?php

namespace App\Settings;

use Illuminate\Database\Eloquent\Model;

class DepartmentsModel extends Model
{
    //table 
    protected $table = 'departments';
    //fields
    protected $fillable = [
        'name','manager'
    ];
    
}
