<?php

namespace App\Assets;

use Illuminate\Database\Eloquent\Model;
use App\Settings\Categories;
class AssetsModel extends Model
{
    //table
    protected $table = 'category_cost';
    //fields
    protected $fillable = [
        'name','available_balance','given_out', 'total','cost'
    ];

    //asset_category relationship
    public function category(){
        return $this->belongsTo(Categories::class);
    }
}
