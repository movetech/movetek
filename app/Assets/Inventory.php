<?php

namespace App\Assets;

use Illuminate\Database\Eloquent\Model;
use App\Settings\Categories;
class Inventory extends Model
{
    //table
    protected $table = 'inventory';
    //fields
    protected $fillable = ['model_number','serial_number',
                            'supplier','cost','tag','asset',
                            'purchase_date','warrant','status',
                            'delivery_date','state'
                        ];
                        public function category(){
                            return $this->belongsTo('App\Settings\Categories','category_id');
                        }
}