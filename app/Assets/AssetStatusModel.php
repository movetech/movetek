<?php

namespace App\Assets;

use Illuminate\Database\Eloquent\Model;

class AssetStatusModel extends Model
{
    //the table
    protected $table = 'asset_statuses';
    //fillable data
    protected $fillable = ['status','description'];
}
