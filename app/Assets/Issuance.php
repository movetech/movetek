<?php

namespace App\Assets;
use App\User;

use Illuminate\Database\Eloquent\Model;
class Issuance extends Model
{
    //table
    protected $table = 'issuance';
    //fields
    
    public function user()
    {
        return $this->belongsTo('App\User','name');
    }
    public function category(){
        return $this->belongsTo('App\Settings\Categories','category_id');
    }
}
