<?php

namespace App\Roles;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    //declare the table that is used by this particular model
    protected $table = 'roles';
    //the table fields
    protected $fillable = ['role','task'];
}

