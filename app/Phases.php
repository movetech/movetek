<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phases extends Model
{
    //
    protected $table = 'phases';
    protected $fillable = ['status','phase_cost','supplementary_budget'];
}
