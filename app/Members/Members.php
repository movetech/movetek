<?php

namespace App\Members;

use Illuminate\Database\Eloquent\Model;

class Members extends Model
{
    //the table referenced by this model
    protected $table = 'users';

    //add table fields
    protected $fillable = [
        'name','username','phone','email','location','department',
    ];
}
