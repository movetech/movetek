<?php

namespace App\Projects;

use Illuminate\Database\Eloquent\Model;
class Projects extends Model
{
    //table
    protected $table = 'projects';
    //fields
    protected $fillable = [
       'name' ,'status','description','financial_year','contractor','total_amount','award_date',
        'amount_paid','date_paid','amount_remaining','date_topay','ministry','subcounty','department_code',
        'duration','supplementary_budget','total_budget',
    ];
}
