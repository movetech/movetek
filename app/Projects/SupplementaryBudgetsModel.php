<?php

namespace App\Projects;

use Illuminate\Database\Eloquent\Model;

class SupplementaryBudgetsModel extends Model
{
    //add the table asssociated with this model
    protected $table = 'supplementary_budgets';
    //the fillable data
    protected $fillable = ['project','phase','amount'];
}
