<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Asset Management Software
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy <?php echo date('Y') ;?> <a href="">Mustaqim Limited</a>.</strong> All rights reserved.
</footer>