@extends('mainlayout')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class=" content-header text-center">
            <h4><i class="fa fa-asset bg-secondary"></i>
            Edit Supplier
            </h4>
          </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
<div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="box box-primary">
                    <div class="box-body">
                       <div class="card">
                         
                           <div class="card-body">
                                @include('admin.includes.logs')
                               <form action="{{ route('supplier.update', $supplier->id) }}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                   
                                    <div class="form-group row {{ $errors->has('name')? 'has-error':''}}">
                                        <label for="name" class="col-md-4 form-label text-md-right">Name</label>
                                        <div class="col-md-8">
                                            <input type="text" name="name" class="form-control" value="{{ $supplier->name}}">
                                            {{-- <span class="alert alert-danger">{{ $errors->first('name') }}</span> --}}
                                        </div>
                                    </div>
                                    <div class="form-group row {{ $errors->has('contact_name')? 'has-error':''}}">
                                        <label for="contact_name" class="col-md-4 form-label text-md-right">Contact Name</label>
                                        <div class="col-md-8">
                                            <input type="text" name="contact_name" class="form-control" value="{{ $supplier->contact_name }}">
                                            {{-- <span class="alert alert-danger">{{ $errors->first('name') }}</span> --}}
                                        </div>
                                    </div>
                                    <div class="form-group row {{ $errors->has('phone') ? 'has-error':''}}">
                                        <label for="phone" class="col-md-4 form-label text-md-right">Phone</label>
                                        <div class="col-md-8">
                                            <input type="text" name="phone" class="form-control" value="{{ $supplier->phone }}">
                                             {{-- <span class="alert alert-danger">{{ $errors->first('phone') }}</span> --}}
                                        </div>
                                </div>
                                <div class="form-group row {{ $errors->has('email') ? 'has-error':''}}">
                                    <label for="email" class="col-md-4 form-label text-md-right">Email</label>
                                    <div class="col-md-8">
                                        <input type="email" name="email" class="form-control" value="{{ $supplier->email }}"/>
                                         {{-- <span class="alert alert-danger">{{ $errors->first('email') }}</span> --}}
                                    </div>
                                </div>
                                    <div class="form-group row {{ $errors->has('url') ? 'has-error':''}}">
                                        <label for="url" class="col-md-4 form-label text-md-right">URL</label>
                                        <div class="col-md-8">
                                            <input type="text" name="url" class="form-control" value="{{ $supplier->url }}">
                                             {{-- <span class="alert alert-danger">{{ $errors->first('url') }}</span> --}}
                                        </div>
                                    </div>
                                   
                                    <div class="form-group row {{ $errors->has('notes') ? 'has-error':''}}">
                                        <label for="notes" class="col-md-4 form-label text-md-right">Notes</label>
                                        <div class="col-md-8">
                                           <textarea  name="notes" id="notes" cols="30" rows="10" class="form-control">
                                                    {{ $supplier->notes }}
                                           </textarea>
                                             {{-- <span class="alert alert-danger">{{ $errors->first('email') }}</span> --}}
                                        </div>
                                </div>
                                    <div class="form-group row">
                                        <hr>
                                        <img src="/storage/images/suppliers/{{ $supplier->image }}" alt="no-image" class="img-thumbnail" style="width:300;height:300;margin-left:25%">
                                        <hr>
                                            <label for="image" class="col-md-4 form-label text-md-right">Upload Image</label>
                                            <div class="col-md-8">
                                                <input type="file" name="image">
                                            </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-8 col-md-offset-4">
                                                <form action="{{ route('supplier.update',$supplier->id ) }}" method="POST">
                                                        <input type="hidden" name="_method" value="PUT">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <input type="submit" class="btn btn-primary btn-sm" value="Update">
                                                    </form>
                                                    <a style="float:right" href="{{ route('suppliers.index') }}" class="btn btn-danger btn-sm"><i class="fa fa-window-close"></i> Cancel</a>
                                        </div>
                                    </div>
                              </form>
                           </div>
                       </div>
                    </div>


                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
        


    </section>



</div>

@endsection