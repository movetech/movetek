@extends('mainlayout')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class=" content-header text-center">
            <h4><i class="fa fa-asset bg-secondary"></i>
             Add Supplier 
            </h4>
          </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
<div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="box box-primary">
                    <div class="box-body">
                       <div class="card">
                           {{-- <div class="card-header bg-info text-white">
                               Add manufacturers
                           </div> --}}
                           <div class="card-body">
                              @include('admin.includes.logs')
                               <form action="{{ route('suppliers.post') }}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                   
                                    <div class="form-group row {{ $errors->has('name')? 'has-error':''}}">
                                        <label for="name" class="col-md-4 form-label text-md-right">Name</label>
                                        <div class="col-md-8">
                                            <input type="text" name="name" class="form-control">
                                            {{-- <span class="alert alert-danger">{{ $errors->first('name') }}</span> --}}
                                        </div>
                                    </div>
                                    <div class="form-group row {{ $errors->has('contact_name')? 'has-error':''}}">
                                        <label for="contact_name" class="col-md-4 form-label text-md-right">Contact Name</label>
                                        <div class="col-md-8">
                                            <input type="text" name="contact_name" class="form-control">
                                            {{-- <span class="alert alert-danger">{{ $errors->first('name') }}</span> --}}
                                        </div>
                                    </div>
                                    <div class="form-group row {{ $errors->has('phone') ? 'has-error':''}}">
                                        <label for="phone" class="col-md-4 form-label text-md-right">Phone</label>
                                        <div class="col-md-8">
                                            <input type="text" name="phone" class="form-control">
                                             {{-- <span class="alert alert-danger">{{ $errors->first('phone') }}</span> --}}
                                        </div>
                                </div>
                                <div class="form-group row {{ $errors->has('email') ? 'has-error':''}}">
                                    <label for="email" class="col-md-4 form-label text-md-right">Email</label>
                                    <div class="col-md-8">
                                        <input type="email" name="email" class="form-control"/>
                                         {{-- <span class="alert alert-danger">{{ $errors->first('email') }}</span> --}}
                                    </div>
                                </div>
                                    <div class="form-group row {{ $errors->has('url') ? 'has-error':''}}">
                                        <label for="url" class="col-md-4 form-label text-md-right">URL</label>
                                        <div class="col-md-8">
                                            <input type="text" name="url" class="form-control">
                                             {{-- <span class="alert alert-danger">{{ $errors->first('url') }}</span> --}}
                                        </div>
                                    </div>
                                   
                                    <div class="form-group row {{ $errors->has('notes') ? 'has-error':''}}">
                                        <label for="notes" class="col-md-4 form-label text-md-right">Notes</label>
                                        <div class="col-md-8">
                                           <textarea name="notes" id="notes" cols="30" rows="10" class="form-control">

                                           </textarea>
                                             {{-- <span class="alert alert-danger">{{ $errors->first('email') }}</span> --}}
                                        </div>
                                </div>
                                    <div class="form-group row">
                                            <label for="image" class="col-md-4 form-label text-md-right">Upload Image</label>
                                            <div class="col-md-8">
                                                <input type="file" name="image">
                                            </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-8 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                <i class="fa fa-arrow-right"></i>
                                                Submit
                                            </button>
                                            <a href="{{ route('suppliers.index') }}" class="btn btn-danger" style="float:right"><i class="fa fa-window-close"></i> Cancel</a>
                                        </div>
                                    </div>
                              </form>
                           </div>
                       </div>
                    </div>


                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
        


    </section>



</div>

@endsection