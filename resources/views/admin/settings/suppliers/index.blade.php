@extends('mainlayout')
<link rel="stylesheet" href="{{ asset('assets/datatables/css/lib/datatable/dataTables.bootstrap.min.css') }}">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css"/>

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class=" content-header text-center">
            <h4><i class="fa fa-asset bg-secondary"></i>
             Supplliers
            </h4>
          </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
{{-- <div class="col-md-2"></div> --}}
            <div class="col-md-12">
                <div class="box box-default">
                    <div class="box-header">
                        <div class="box-title">
                     <a href="{{ route('suppliers.create') }}" class="btn btn-info btn-sm">New</a>
                    </div>
                    <div class="box-body">
                        @include('admin.includes.logs')
                        @if(isset($supplier))
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Image</th>
                                        <th>Name</th>
                                        <th>Contact Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Url</th>
                                        <th>Notes</th>
                                        <th>View</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                @foreach($supplier as $info)
                                    <tbody>
                                        <tr>
                                            <td>{{ $info->id }}</td>
                                            <td><img src="/storage/images/suppliers/{{ $info->image }}" alt="no image" class="img-thumbnail" class="img-thumbnail" style="width:40;height20"></td>
                                            <td>{{ $info->name }}</td>
                                            <td>{{ $info->contact_name }}</td>
                                            <td>{{ $info->email }}</td>
                                            <td>{{ $info->phone }}</td>
                                            <td><a href="{{ $info->url }}" target="_blabk">{{ $info->url }}</a></td>
                                            <td>{{ $info->notes }}</td>
                                            <td class="pt-2-half">
                                                <a href="{{ route('supplier.show', $info->id ) }}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i> View</a>
                                            </td>
                                            <td class="pt-2-half">
                                                <a href="{{ route('supplier.edit', $info->id ) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</a>
                                            </td>
                                            <td class="pt-2-half">
                                                <form action="{{ route('supplier.delete',$info->id ) }}" method="POST">
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <button  type="submit" class="btn btn-danger" onclick="if (!confirm('Are you sure you want to proceed?')) { return false }"><span>Delete</span></button>
                                                </form>
                                            </td>
                                        </tr>
                                    </tbody>
                                @endforeach
                            </table>
                            {{ $supplier->links() }}
                        @endif
                        
                        </table> 
                    </div>


                </div>
            </div>
            {{-- <div class="col-md-2"></div> --}}
        </div>
        


    </section>



</div>
@endsection