@extends('mainlayout')
<link rel="stylesheet" href="{{ asset('assets/datatables/css/lib/datatable/dataTables.bootstrap.min.css') }}">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css"/>

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class=" content-header text-center">
            <h4><i class="fa fa-asset bg-secondary"></i>
            All Asset Statuses
            </h4>
          </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
{{-- <div class="col-md-2"></div> --}}
            <div class="col-md-12">
                <div class="box box-default">
                    <div class="box-header">
                        <div class="box-title">                        
                            
                            <a href="{{ route('show-asset-status-form') }}" class="btn btn-info btn-sm">New</a>
                    </div>
                    </div>
                    <div class="box-body">
                        @include('messages.custom')
                        @if(isset($asset_statuses))
                            <table class="table table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Status</th>
                                        <th>Description</th>
                                        <th>View</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                @foreach($asset_statuses as $key=>$value)
                                    <tbody>
                                        <tr>
                                            <td>{{ $value->id }}</td>
                                            <td>{{ $value->status }}</td>
                                            <td>{{ $value->description }}</td>
                                            <td class="pt-2-half">
                                                     <button data-toggle="modal" data-target="#exampleModalCenter" class="btn btn-xs btn-primary">
                                                        <i class="fa fa-eye"></i>     
                                                    </button> 
                                                {{-- <a href="{{ route('show-asset-status-details', $value->id) }}" class="btn btn-info btn-sm"><i class="fa fa-eye"></i> View</a> --}}
                                            </td>
                                            <td class="pt-2-half">
                                                <a href="{{ route('show-asset-status-edit-form', $value->id) }}" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></a>
                                            </td>
                                        <td class="pt-2-half">
                                                <form action="{{ route('delete-existing-asset-status',$value->id ) }}" method="POST">
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <button  type="submit" class="btn btn-danger btn-xs" onclick="if (!confirm('Are you sure you want to proceed?')) { return false }"><i class="fa fa-trash"></i></button>
                                                </form>
                                            </td>
                                            
                                        </tr>
                                    </tbody>
                                @endforeach
                            </table>
                            {{ $asset_statuses->links() }}
                        @endif
                       
                        </table> 
                    </div>


                </div>
            </div>
            {{-- <div class="col-md-2"></div> --}}
        </div>
        


    </section>

      <!-- Modal to enable the user view the asset status details -->
      <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                    @include('messages.custom')
                    <form action="" method="post">
                                 {{ csrf_field() }}
                        <div class="form-group row">
                            <label for="status" class="col-md-4 col-form-label text-md-right">Status</label>
                            <div class="col-md-8">
                                <input type="text" name="status" class="form-control" value="{{ old('status') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="description" id="description" class="col-md-4 form-label text-md-right">Description</label>
                            <div class="col-md-8">
                                <textarea name="description" id="description" cols="30" rows="10" class="form-control">
                                    {{ old('description') }}
                                </textarea>
                            </div>
                        </div>
                        
                        <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-add"></i> Submit
                                    </button>
                                    <a href="{{ route('get-assest-statuses') }}" class="btn btn-info pull-right"><i class="fa fa-arrow-left"></i> Back</a>

                                </div>

                        </div>
                    </form>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-window-close"></i>  Close</button>
            </div>
          </div>
        </div>
      </div>
</div>
@endsection