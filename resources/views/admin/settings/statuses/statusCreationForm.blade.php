@extends('mainlayout')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
        <section class=" content-header text-center">
                <h4><i class="fa fa-asset bg-secondary"></i>
                 Add New Asset Status
                </h4>
              </section>
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">
        <div class="row">
{{-- <div class="col-md-2"></div> --}}
            <div class="col-md-12">
                <div class="box box-default">
                    <div class="box-header">
                    </div>
                    <div class="box-body">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            @include('messages.custom')
                            <form action="{{ route('new-asset-status') }}" method="post">
                                         {{ csrf_field() }}
                                <div class="form-group row">
                                    <label for="status" class="col-md-4 col-form-label text-md-right">Status</label>
                                    <div class="col-md-8">
                                        <input type="text" name="status" class="form-control" value="{{ old('status') }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="description" id="description" class="col-md-4 form-label text-md-right">Description</label>
                                    <div class="col-md-8">
                                        <textarea name="description" id="description" cols="30" rows="10" class="form-control">
                                            {{ old('description') }}
                                        </textarea>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                        <div class="col-md-8 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                <i class="fa fa-add"></i> Submit
                                            </button>
                                            <a href="{{ route('get-assest-statuses') }}" class="btn btn-info pull-right"><i class="fa fa-arrow-left"></i> Back</a>

                                        </div>

                                </div>
                            </form>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>  
            </div>
        </div>
    </section>
</div>
@endsection