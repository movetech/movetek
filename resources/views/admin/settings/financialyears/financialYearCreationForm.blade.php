@extends('mainlayout')

@section('content')
    <div class="content-wrapper">
            <section class=" content-header text-center">
                    <h4><i class="fa fa-asset bg-secondary"></i>
                      Add Financial Year
                    </h4>
                  </section>
        <section class="content">
            <div class="row">
                {{-- <div class="col-md-2"></div> --}}
                <div class="col-md-12">
                    <div class="box box-default">
                        <div class="box-header bg-primary  text-uppercase">
                            {{-- <a  href="{{ route('show-financial-year-creation-form') }}" class="btn btn-success">New</a> --}}
                           
                        </div>
                        <div class="box-body">
                            @include('messages.custom')
                           <div class="card">
                               <div class="card-header ">

                                            <div class="row">
                                               <div class="col-md-4"></div>
                                                <div class="col-md-4">
                                                        <form action="{{ route('add-new-financial-year') }}" method="post">
                                                            {{ csrf_field() }}
                                                            <div class="form-group row">
                                                                <label for="year" class="col-md-4 form-label text-md-right">Year</label>
                                                                <div class="col-md-8">
                                                                    <input type="text" name="financial_year" class="form-control" value="{{ old('financial_year') }}">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-8 col-md-offset-4">
                                                                       
                                                                       <button type="submit" class="btn btn-primary">
                                                                           <i class="fa fa-send"></i> Add
                                                                       </button>
                                                                
                                                            </div>
                                                        </form>
                                                </div>
                                                <div class="col-md-4">
                                                       
                                                    </div>
                                            </div>
                              {{-- <h4>{{ $financial_years->year}}</h4>      --}}
                                </div>
                               <div class="card-body">

                               </div>
                           </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="col-md-2"></div> --}}
            </div>
        </section>
    </div>
    
@endsection