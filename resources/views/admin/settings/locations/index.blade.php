@extends('mainlayout')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class=" content-header text-center">
            <h4><i class="fa fa-asset bg-secondary"></i>
                Sub-Counties
            </h4>
          </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
{{-- <div class="col-md-2"></div> --}}
            <div class="col-md-12">
                <div class="box box-default">
                    <div class="box-header">
                        <div class="box-title">
                        <a href="{{ route('locations.create') }}" class="btn btn-info btn-sm">New</a>
                    </div>
                    <div class="box-body">
                        @include('messages.custom')
                        @if(isset($location))
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Manager</th>
                                        <th>Address</th>
                                        <th>Town</th>
                                        <th>View</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                @foreach($location as $info)
                                    <tbody>
                                        <tr>
                                            <td>{{ $info->id }}</td>
                                            <td>{{ $info->name }}</td>
                                            <td>{{ $info->manager }}</td>
                                            <td>{{ $info->address }}</td>
                                            <td>{{ $info->town }}</td>
                                            <td class="pt-2-half">
                                                <a href="{{ route('location.show', $info->id ) }}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i> View</a>
                                            </td>
                                            <td class="pt-2-half">
                                                <a href="{{ route('location.edit', $info->id ) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</a>
                                            </td>
                                            <td class="pt-2-half">
                                                <form action="{{ route('location.delete',$info->id ) }}" method="POST">
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <button  type="submit" class="btn btn-danger" onclick="if (!confirm('Are you sure you want to proceed?')) { return false }"><span>Delete</span></button>
                                                </form>
                                            </td>

                                        </tr>
                                    </tbody>
                                @endforeach
                            </table>
                            {{ $location->links() }}
                        @endif
                        
                        </table> 
                    </div>


                </div>
            </div>
            {{-- <div class="col-md-2"></div> --}}
        </div>
        


    </section>



</div>
@endsection