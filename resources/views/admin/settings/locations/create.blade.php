@extends('mainlayout')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class=" content-header text-center">
            <h4><i class="fa fa-asset bg-secondary"></i>
              Add New Sub-County
            </h4>
          </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
<div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="box box-primary">
                    <div class="box-body">
                       <div class="card">
                           <div class="card-body">
                             @include('messages.custom')
                               <form action="{{ route('locations.post') }}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                   
                                    <div class="form-group row {{ $errors->has('name')? 'has-error':''}}">
                                        <label for="name" class="col-md-4 form-label text-md-right">Name</label>
                                        <div class="col-md-8">
                                            <input type="text" name="name" class="form-control">
                                            {{-- <span class="alert alert-danger">{{ $errors->first('name') }}</span> --}}
                                        </div>
                                    </div>
                                    <div class="form-group row {{ $errors->has('manager') ? 'has-error':''}}">
                                        <label for="manager" class="col-md-4 form-label text-md-right">Manager</label>
                                        <div class="col-md-8">
                                            <input type="text" name="manager" class="form-control">
                                             {{-- <span class="alert alert-danger">{{ $errors->first('url') }}</span> --}}
                                        </div>
                                    </div>
                                    <div class="form-group row {{ $errors->has('address') ? 'has-error':''}}">
                                            <label for="address" class="col-md-4 form-label text-md-right" class="form-control">Address</label>
                                            <div class="col-md-8">
                                                <textarea name="address" id="address" cols="30" rows="10" class="form-control">

                                                </textarea>
                                                 {{-- <span class="alert alert-danger">{{ $errors->first('phone') }}</span> --}}
                                            </div>
                                    </div>
                                    <div class="form-group row {{ $errors->has('town') ? 'has-error':''}}">
                                            <label for="town" class="col-md-4 form-label text-md-right">Town</label>
                                            <div class="col-md-8">
                                                <input type="text" name="town" class="form-control"/>
                                                 {{-- <span class="alert alert-danger">{{ $errors->first('email') }}</span> --}}
                                            </div>
                                    </div>
                                  
                                    <div class="form-group row">
                                        <div class="col-md-8 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                <i class="fa fa-arrow-right"></i>
                                                Submit
                                            </button>
                                            <a href="{{ route('locations.index') }}" class="btn btn-info" style="float:right"><i class="fa fa-arrow-left"></i> Back</a>
                                        </div>
                                    </div>
                              </form>
                           </div>
                       </div>
                    </div>


                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
        


    </section>



</div>

@endsection