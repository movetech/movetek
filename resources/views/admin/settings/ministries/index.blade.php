@extends('mainlayout')
@section('content')
<link rel="stylesheet" href="{{asset('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css')}}">
<script src="{{asset('https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js')}}"></script> 
    <div class="content-wrapper">
            <section class=" content-header text-center">
                    <h4><i class="fa fa-asset bg-secondary"></i>
                        Ministries
                    </h4>
                  </section>
                  @include('messages.custom')
        <section class="content">
            <div class="row">
                {{-- <div class="col-md-2"></div> --}}
                <div class="col-md-12">
                    <div class="box box-default">
                        
                               {{-- <a style="float:right" href="{{route('assets.assign')}}" class="btn btn-info btn-sm">New</a> --}}
                        
                        <!-- Search form -->
  <div class="md-form mt-0">
        <input class="form-control" id="search" type="text" placeholder="Search&hellip;" aria-label="Search">
      </div>
                        <div class="box-body">
                            <div class="col-md-1"></div>
                            <div class="col-md-10">
                                    @if(count($ministries) >0)
                            <table class="table table-bordered table-responsive" id="search">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Action</th>
                                        
                                    </tr>
                                </thead>
                                @foreach($ministries as $ministry)
                                     
                                        <tr>
                                        
                                            <td>{{ $ministry->id }}</td>
                                            <td>{{ $ministry->name }}</td>
                                                    <td  class="pt-2-half">
                                                            <a href="{{ route('show-single-ministry', $ministry->id) }}" style="float:left;" data-placement="top" data-toggle="tooltip" title="view"><button class="btn btn-info btn-xs pull-right " data-title="view" data-toggle="modal" data-target="#view" ><span class="fa fa-eye"></span></button></a>
                                                            <a href="{{ route('show-ministry-edit-form', $ministry->id) }}" style="float:left;" data-placement="top" data-toggle="tooltip" title="view"><button class="btn btn-info btn-xs pull-right " data-title="view" data-toggle="modal" data-target="#view" ><span class="fa fa-edit"></span></button></a>
                                                            <form action="{{ route('delete-existing-ministry', $ministry->id) }}" method="post">
                                                                    <input type="hidden" name="_method" value="DELETE"/>
                                                                    <input type="hidden" name="_token" value="{{  csrf_token() }}"/>
                                                                    <p style="float:left;" data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-danger btn-xs pull-right" data-title="Delete" data-toggle="modal" data-target="#delete" onclick="if (!confirm('Are you sure you want to proceed?')) { return false }"><span class="glyphicon glyphicon-trash"></span></button></p>
                                                            </form>
                                                    </td>
                                                                  
                                                        
                            
                                            
                                        </tr>
                                     
                                @endforeach
                            </table>
                            {{ $ministries->links() }}
                            @endif
                            </div>
                            <div class="col-md-1"></div>
                            
                        </div>
                    </div>
                </div>
                {{-- <div class="col-md-2"></div> --}}
            </div>
        </section>
    </div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#search').on('keyup',function(){
            //alert('Helllo');
        $value=$(this).val();
        //$select=$('#inputtype').val();
        var token = $("meta[name='csrf-token']").attr("content");
        $.ajax({
        type : 'get',
        url : '{{URL::to('admin/ministries/search')}}',
         
        data:{'search':$value,  "_token": token,},
         
        success:function(data){
         
        $('tbody').html(data);
                          
        }
        });
         
       }); 
       // 
        
    });
</script>
@endsection