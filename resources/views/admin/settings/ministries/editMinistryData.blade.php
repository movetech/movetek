@extends('mainlayout')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class=" content-header text-center">
            <h4><i class="fa fa-asset bg-secondary"></i>
              Edit Information for the {{ $ministry->name }} Ministry
            </h4>
          </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
<div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="box box-primary">
                    <div class="box-body">
                       <div class="card">
                           <div class="card-body">
                             @include('messages.custom')
                               <form action="{{ route('update-existing-ministry', $ministry->id) }}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                   
                                    <div class="form-group row {{ $errors->has('name')? 'has-error':''}}">
                                        <label for="name" class="col-md-4 form-label text-md-right">Name</label>
                                        <div class="col-md-8">
                                            <input type="text" name="name" class="form-control" value="{{ $ministry->name }}">
                                            {{-- <span class="alert alert-danger">{{ $errors->first('name') }}</span> --}}
                                        </div>
                                    </div>
                            
                                    <div class="form-group row">
                                        <div class="col-md-8 col-md-offset-4">
                                            <form action="{{ route('update-existing-ministry', $ministry->id) }}" method="post">
                                                <input type="hidden" name="_method" value="PUT"/>
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="fa fa-arrow-right"></i> Update
                                                </button>
                                            </form>
                                            <a href="{{ route('list-all-ministries') }}" class="btn btn-info" style="float:right"><i class="fa fa-arrow-left"></i> Back</a>
                                        </div>
                                    </div>
                              </form>
                           </div>
                       </div>
                    </div>


                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
        


    </section>



</div>

@endsection