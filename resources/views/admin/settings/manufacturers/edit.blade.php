@extends('mainlayout')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class=" content-header text-center">
            <h4><i class="fa fa-asset bg-secondary"></i>
             Edit Manufacturer
            </h4>
          </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
<div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="box box-primary">
                    <div class="box-body">
                       <div class="card">
                           {{-- <div class="card-header bg-info text-white">
                               Add manufacturers
                           </div> --}}
                           <div class="card-body">
                               @include('admin.includes.logs')
                               <form action="{{ route('manufacturer.update', $manufacturer->id) }}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                   
                                    <div class="form-group row {{ $errors->has('name')? 'has-error':''}}">
                                        <label for="name" class="col-md-4 form-label text-md-right">Name</label>
                                        <div class="col-md-8">
                                            <input type="text" name="name" class="form-control" value="{{ $manufacturer->name }}">
                                            {{-- <span class="alert alert-danger">{{ $errors->first('name') }}</span> --}}
                                        </div>
                                    </div>
                                    <div class="form-group row {{ $errors->has('url') ? 'has-error':''}}">
                                        <label for="url" class="col-md-4 form-label text-md-right">URL</label>
                                        <div class="col-md-8">
                                            <input type="text" name="url" class="form-control" value="{{ $manufacturer->url }}">
                                             {{-- <span class="alert alert-danger">{{ $errors->first('url') }}</span> --}}
                                        </div>
                                    </div>
                                    <div class="form-group row {{ $errors->has('phone') ? 'has-error':''}}">
                                            <label for="phone" class="col-md-4 form-label text-md-right">Phone</label>
                                            <div class="col-md-8">
                                                <input type="text" name="phone" class="form-control" value="{{ $manufacturer->phone }}">
                                                 {{-- <span class="alert alert-danger">{{ $errors->first('phone') }}</span> --}}
                                            </div>
                                    </div>
                                    <div class="form-group row {{ $errors->has('email') ? 'has-error':''}}">
                                            <label for="email" class="col-md-4 form-label text-md-right">Email</label>
                                            <div class="col-md-8">
                                                <input type="email" name="email" class="form-control" value="{{ $manufacturer->email }}"/>
                                                 {{-- <span class="alert alert-danger">{{ $errors->first('email') }}</span> --}}
                                            </div>
                                    </div>
                                    <div class="form-group row">
                                            <label for="image" class="col-md-4 form-label text-md-right">Upload Image</label>
                                            <div class="col-md-8">
                                                <hr>
                                                <img src="/storage/images/manufacturers/{{ $manufacturer->image }}" alt="no-image">
                                                <hr>
                                                <input type="file" name="image">
                                            </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-8 col-md-offset-4">
                                                <form action="{{ route('manufacturer.update',$manufacturer->id ) }}" method="POST">
                                                        <input type="hidden" name="_method" value="PUT">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <input type="submit" class="btn btn-primary btn-sm" value="Update">
                                                </form>
                                                <a href="{{ route('manufacturers.index') }}" class="btn btn-info" style="float:right"><i class="fa fa-arrow-left"></i> Back</a>
                                        </div>
                                    </div>
                              </form>
                           </div>
                       </div>
                    </div>


                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
        


    </section>



</div>

@endsection