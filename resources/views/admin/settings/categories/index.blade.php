@extends('mainlayout')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class=" content-header text-center">
            <h4><i class="fa fa-asset bg-secondary"></i>
            Asset  Categories
            </h4>
          </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
{{-- <div class="col-md-2"></div> --}}
            <div class="col-md-12">
                <div class="box box-default">
                    <div class="box-header">
                        <div class="box-title">
                            <a href="{{ route('categories.create') }}" class="btn btn-info btn-sm">New</a>
                    </div>
                    <div class="box-body">
                        @include('admin.includes.logs')
                        @if(isset($categories))
                            <table class="table table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Type</th>
                                         <th>Action</th>
                                    </tr>
                                </thead>
                                @foreach($categories as $category)
                                    <tbody>
                                        <tr>
                                            <td>{{ $category->id }}</td>
                                            <td>{{ $category->name }}</td>
                                            <td>{{ $category->type }}</td>
                                             <td>
                                                <a href="{{ route('category.show', $category->id) }}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i> View</a>
                                                <a href="{{ route('category.edit', $category->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</a>
                                                {{-- <form action="{{ route('category.delete',$category->id ) }}" method="POST">
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                                                </form> --}}
                                            </td>
                                        </tr>
                                    </tbody>
                                @endforeach
                            </table>
                            {{ $categories->links() }}
                        @endif
                        
                        </table> 
                    </div>


                </div>
            </div>
            {{-- <div class="col-md-2"></div> --}}
        </div>
        


    </section>



</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#categories_table').DataTable({
            "processing":true,
            "serverSide":true,
            "ajax":"{{ route('categories.index') }}",
                    "type":'GET'
            "columns":[
                {data:'id', name:'id'},
                {data:'name', name:'name'},
                {data:'type',name:'type'},
                {data:'action',name:'action'}
            ]
        });
    });
</script>
<script src="{{ asset('assets/datatables/js/lib/data-table/jquery-1.12.4.js') }}"></script>
<script src="{{ asset('assets/datatables/js/main.js') }}"></script>
<script src="{{ asset('assets/datatables/js/lib/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/datatables/js/lib/data-table/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/datatables/js/lib/data-table/buttons.print.min.js') }}"></script>
<script src="{{ asset('assets/datatables/js/lib/data-table/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/datatables/js/lib/data-table/vfs_fonts.min.js') }}"></script>
<script src="{{ asset('assets/datatables/js/lib/data-table/jszip.min.js') }}"></script>
<script src="{{ asset('assets/datatables/js/lib/data-table/buttons.flash.min.js') }}"></script>
<script src="{{ asset('assets/datatables/js/lib/data-table/buttons.bootstrap.min.js') }}"></script>
@endsection