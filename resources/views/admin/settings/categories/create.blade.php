@extends('mainlayout')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class=" content-header text-center">
            <h4><i class="fa fa-asset bg-secondary"></i>
              Create New Asset Category
            </h4>
          </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
<div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="box box-primary">
                    <div class="box-body">
                       <div class="card">
                           {{-- <div class="card-header bg-info text-white">
                               Add Categories
                           </div> --}}
                           <div class="card-body">
                             @include('messages.custom')
                               <form action="{{ route('categories.post') }}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                   
                                    <div class="form-group row{{ $errors->has('name')? 'has-error':''}}">
                                        <label for="name" class="col-md-4 form-label text-md-right">Name</label>
                                        <div class="col-md-8">
                                            <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                                            {{-- <span class="alert alert-danger">{{ $errors->first('name') }}</span> --}}
                                        </div>
                                    </div>
                                    <div class="form-group row {{ $errors->has('type') ? 'has-error':''}}">
                                        <label for="type" class="col-md-4 form-labe text-md-right">Type</label>
                                        <div class="col-md-8">
                                            @if(count($category_types) > 0)
                                                <select name="type" id="type" class="form-control">
                                                    @foreach($category_types as $key=>$value)
                                                            <option value="{{ $value->type }}">{{ $value->type }}</option>
                                                    @endforeach
                                                </select>


                                            @endif
                                            {{-- <input type="text" name="type" class="form-control"> --}}
                                             {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-8 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                <i class="fa fa-arrow-right"></i>
                                                Submit
                                            </button>
                                            <a href="{{ route('assets.create') }}" class="btn btn-info" style="float:right"><i class="fa fa-arrow-left"></i> Back</a>
                                        </div>
                                    </div>
                              </form>
                           </div>
                       </div>
                    </div>


                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
        


    </section>



</div>

@endsection