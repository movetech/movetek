@extends('mainlayout')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class=" content-header text-center">
            <h4><i class="fa fa-asset bg-secondary"></i>
             Edit Category
            </h4>
          </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
<div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="box box-primary">
                    <div class="box-body">
                       <div class="card">
                           {{-- <div class="card-header bg-info text-white">
                               Add Categories
                           </div> --}}
                           <div class="card-body">
                                    @include('admin.includes.logs')
                               <form action="{{ route('category.update', $category->id ) }}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                   
                                    <div class="form-group row{{ $errors->has('name')? 'has-error':''}}">
                                        <label for="name" class="col-md-4 form-label text-md-right">Name</label>
                                        <div class="col-md-8">
                                            <input type="text" name="name" class="form-control" value="{{ $category->name }}">
                                            {{-- <span class="alert alert-danger">{{ $errors->first('name') }}</span> --}}
                                        </div>
                                    </div>
                                    <div class="form-group row {{ $errors->has('type') ? 'has-error':''}}">
                                        <label for="type" class="col-md-4 form-labe text-md-right">Type</label>
                                        <div class="col-md-8">
                                            <input type="text" name="type" class="form-control" value="{{ $category->type }}">
                                             {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-8 col-md-offset-4">
                                                <form action="{{ route('category.update',$category->id ) }}" method="POST">
                                                        <input type="hidden" name="_method" value="PUT">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <input type="submit" class="btn btn-primary" value="Update">
                                                </form>
                                            <a href="{{ route('categories.index') }}" class="btn btn-info" style="float:right"><i class="fa fa-arrow-left"></i> Back</a>
                                        </div>
                                    </div>
                              </form>
                           </div>
                       </div>
                    </div>


                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
        


    </section>



</div>

@endsection