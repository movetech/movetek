@extends('mainlayout')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class=" content-header text-center">
            <h4><i class="fa fa-asset bg-secondary"></i>
              Edit Department Information
            </h4>
          </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
<div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="box box-primary">
                    <div class="box-body">
                       <div class="card">
                           <div class="card-body">
                               @include('admin.includes.logs')
                               <form action="{{ route('department.update', $department->id) }}" method="post">
                                    {{ csrf_field() }}
                                   
                                    <div class="form-group row{{ $errors->has('name')? 'has-error':''}}">
                                        <label for="name" class="col-md-4 form-label text-md-right">Name</label>
                                        <div class="col-md-8">
                                            <input type="text" name="department_code" class="form-control" value="{{ $department->department_code }}">
                                            {{-- <span class="alert alert-danger">{{ $errors->first('name') }}</span> --}}
                                        </div>
                                    </div>
                                    <div class="form-group row {{ $errors->has('manager') ? 'has-error':''}}">
                                        <label for="manager" class="col-md-4 form-label text-md-right"> Manager</label>
                                        <div class="col-md-8">
                                            <input type="text" name="manager" class="form-control" value="{{ $department->manager }}">
                                             {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-8 col-md-offset-4">
                                                <form action="{{ route('department.update',$department->id ) }}" method="POST">
                                                        <input type="hidden" name="_method" value="PUT">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <input type="submit" class="btn btn-primary btn-sm" value="Update">
                                                    </form>
                                            <a href="{{ route('departments.index') }}" class="btn btn-primary" style="float:right"><i class="fa fa-arrow-left"></i> Back</a>
                                                
                                        </div>
                                    </div>
                              </form>
                           </div>
                       </div>
                    </div>


                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
        


    </section>



</div>

@endsection