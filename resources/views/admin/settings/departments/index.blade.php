@extends('mainlayout')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class=" content-header text-center">
            <h4><i class="fa fa-asset bg-secondary"></i>
              Departments
            </h4>
          </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
{{-- <div class="col-md-2"></div> --}}
            <div class="col-md-12">
                <div class="box box-default">
                    <div class="box-header">
                        <div class="box-title">
                        <a href="{{ route('departments.create') }}" class="btn btn-info btn-sm">New</a>
                    </div>
                    <div class="box-body">
                        @include('admin.includes.logs')
                        @if(isset($department))
                            <table class="table table-bordered" style="">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Code</th>
                                        <th>Manager</th>
                                        <th>View</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                @foreach($department as $category)
                                    <tbody>
                                        <tr>
                                            <td>{{ $category->id }}</td>
                                            <td>{{ $category->department_code }}</td>
                                            <td>{{ $category->manager }}</td>
                                            <td class="pt-2-half">

                                                <a href="{{ route('department.show', $category->id) }}" class="btn btn-info btn-xs"><i class="fa fa-edit"></i></a>
                                            </td>
                                            <td class="pt-2-half">
                                                <a href="{{ route('department.edit', $category->id) }}" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></a>
                                            </td>
                                            <td class="pt-2-half">
                                                <form action="{{ route('department.delete',$category->id ) }}" method="POST">
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <button  type="submit" class="btn btn-danger btn-xs" onclick="if (!confirm('Are you sure you want to proceed?')) { return false }"><i class="fa fa-trash"></i></button>
                                                </form> 
                                            </td>
                                        </tr>
                                    </tbody>
                                @endforeach
                            </table> 
                            {{ $department->links() }}
                        @endif
                        </table> 
                    </div>


                </div>
            </div>
            {{-- <div class="col-md-2"></div> --}}
        </div>
        


    </section>



</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#departments_table').DataTable({
            "processing":true,
            "serverSide":true,
            "ajax":"{{ route('departments.index') }}",
                    "type":'GET'
            "columns":[
                {data:'id', name:'id'},
                {data:'name', name:'name'},
                {data:'manager',name:'manager'},
                {data:'action',name:'action', orderable:false, searchable:false}
            ]
        });
    });
</script>
@endsection