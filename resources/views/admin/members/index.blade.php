@extends('mainlayout')
@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/core.js"></script>
<script src="{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}"></script>
<script src="{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}"></script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class=" content-header text-center">
        <h4><i class="fa fa-asset bg-secondary"></i>
          Staff Members
        </h4>
      </section>
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">
        <div class="row">
{{-- <div class="col-md-2"></div> --}}
            <div class="col-md-12">
                <div class="box box-default">
                   
                    <div class="box-body">

                        @include('admin.includes.logs')

                        @if(isset($users))
                       <table class="table table-bordered" id="members">
                               <tr>
                                   <th>Id</th>
                                   <th>Name</th>
                                   <th>Username</th>
                                   <th>Email</th>
                                   <th>Phone</th>
                                   <th>Sub County</th>
                                   <th>Department</th>
                                   <th>Status</th>
                                   <th>Action</th>
                                   <th></th>
                               </tr>
                               @foreach($users as $user)
                                    <tr>
                                        <td>{{ $user->id }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->username }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->phone }}</td>
                                        <td>{{ $user->location }}</td>
                                        <td>{{ $user->department }}</td>
                                        <td>{{ $user->status }}</td>
                                        <td class="pt-2-half" >
                                            <a href="{{ route('member.edit', $user->id) }}" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></a>

                                        </td>
                                        <td class="pt-2-half">
                                            {{-- <a href="{{ route('member.delete', $user->id )}}" data-method="delete" data-token="{{ csrf_token() }}" class="btn btn-danger">Delete</a> --}}
                                            <form action="{{ route('member.delete', $user->id) }}" method="post">
                                                <input type="hidden" name="_method" value="DELETE"/>
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <button type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                                                {{-- <a href="{{ route('member.delete', $user->id) }}" class="btn btn-danger"><i class="fa fa-alt-trash"></i> Delete</a> --}}

                                            </form>
                                        </td>
                                    </tr>

                               @endforeach
                       </table>
                       {{ $users->links() }}
                       @endif
                    </div>


                </div>
            </div>
            {{-- <div class="col-md-2"></div> --}}
        </div>
        
<!-- jQuery 2.2.3 -->
{{-- <script src="{{ asset("/bower_components/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js")}}"></script> --}}
<!-- DataTables -->
<script type="text/javascript">

$(document).ready(function() {
 
 var laravel = {
   initialize: function() {
     this.methodLinks = $('a[data-method]');
     this.token = $('a[data-token]');
     this.registerEvents();
   },
//https://drive.google.com/open?id=1RrYqcOrUrQ4SqJI_JrIcCnck4qNsoVhWhttps://drive.google.com/open?id=1RrYqcOrUrQ4SqJI_JrIcCnck4qNsoVhW
   registerEvents: function() {
     this.methodLinks.on('click', this.handleMethod);
   },

   handleMethod: function(e) {
     var link = $(this);
     var httpMethod = link.data('method').toUpperCase();
     var form;

     // If the data-method attribute is not PUT or DELETE,
     // then we don't know what to do. Just ignore.
     if ( $.inArray(httpMethod, ['PUT', 'DELETE']) === - 1 ) {
       return;
     }

     // Allow user to optionally provide data-confirm="Are you sure?"
     if ( link.data('confirm') ) {
       if ( ! laravel.verifyConfirm(link) ) {
         return false;
       }
     }

     form = laravel.createForm(link);
     form.submit();

     e.preventDefault();
   },

   verifyConfirm: function(link) {
     return confirm(link.data('confirm'));
   },

   createForm: function(link) {
     var form = 
     $('<form>', {
       'method': 'POST',
       'action': link.attr('href')
     });

     var token = 
     $('<input>', {
       'type': 'hidden',
       'name': '_token',
       'value': link.data('token')
       });

     var hiddenInput =
     $('<input>', {
       'name': '_method',
       'type': 'hidden',
       'value': link.data('method')
     });

     return form.append(token, hiddenInput)
                .appendTo('body');
   }
 };

 laravel.initialize();

})();
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#members').DataTable({
        "processing":true,
        "serverSide":true,
        "ajax":[
                "{{ route('members.all') }}",
                "type":"GET"
        ],
        "columns":{
            {data:'id',name:'id'},
            {data:'name', name:'name'},
            {data:'username', name:'username'},
            {data:'email',name:'email'},
            {data:'phone', name:'phone'},
            {data:'location', name:'location'},
            {data:'department',name:'department'},
            {data:'status', name:'status'},
            {data:'action', name:'action', orderable:false, searchable:false}
        }
    });
    });
   
</script>
<script
src="https://code.jquery.com/jquery-3.4.1.min.js"
integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
crossorigin="anonymous"></script>

{{-- 
<script src="https://code.jquery.com/jquery-3.4.1.min.js"
integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
crossorigin="anonymous"></script>
<script src="{{ asset('assets/datatables/js/lib/data-table/dataTables.bootstrap.min.js') }}"></script>
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/core.js"></script> --}}
{{-- <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script> --}}
{{-- <script src="{{ asset('assets/datatables/js/main.js') }}"></script>
<script src="{{ asset('assets/datatables/js/lib/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/datatables/js/lib/data-table/buttons.print.min.js') }}"></script>
<script src="{{ asset('assets/datatables/js/lib/data-table/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/datatables/js/lib/data-table/vfs_fonts.min.js') }}"></script>
<script src="{{ asset('assets/datatables/js/lib/data-table/jszip.min.js') }}"></script>
<script src="{{ asset('assets/datatables/js/lib/data-table/buttons.flash.min.js') }}"></script>
<script src="{{ asset('assets/datatables/js/lib/data-table/buttons.bootstrap.min.js') }}"></script> --}}
<!--================ End DataTableds static files =======================> -->
    </section>
</div>



@endsection