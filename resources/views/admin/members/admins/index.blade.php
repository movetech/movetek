@extends('mainlayout')

@section('content')
    <div class="content-wrapper">

        <section class="content">
                <section class=" content-header text-center">
                        <h4><i class="fa fa-asset bg-secondary"></i>
                          Admins
                        </h4>
                      </section>
            <div class="row">
                {{-- <div class="col-md-2"></div> --}}
                <div class="col-md-12">
                    <div class="box box-default">
                        <div class="box-header">
                            {{-- <center><h3>Admins</h3></center> --}}
                               <a style="float:right" href="{{ route('createadmin') }}" class="btn btn-info btn-sm">New</a>
                        </div>
                        <div class="box-body">
                            @include('admin.includes.logs')
                           @if(isset($admins))
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Role</th>
                                            <th>Status</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>
                                    </thead>
                                    @foreach($admins as $admin)
                                        <tbody>
                                            <tr>
                                                <td>{{ $admin->id }}</td>
                                                <td>{{ $admin->name }}</td>
                                                <td>{{ $admin->email }}</td>
                                                <td>{{ $admin->status }}</td>

                                                <td class="pt-2-half"><a href="{{ route('editadmin', $admin->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</a></td>
                                                <td class="pt-2-half">
                                                <form action="{{ route('deleteadmin', $admin->id) }}" method="post">
                                                        <input type="hidden" name="_method" value="DELETE">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                                                    </form>
                                                </td>
                                                

                                            </tr>
                                        </tbody>
                                    @endforeach
                                </table>
                                {{ $admins->links() }}
                           @endif
                        </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="col-md-2"></div> --}}
            
        </section>
    </div>
@endsection