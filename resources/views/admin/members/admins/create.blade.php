@extends('mainlayout')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
    <!-- Main content -->
    <section class="content">
            <section class=" content-header text-center">
                    <h4><i class="fa fa-asset bg-secondary"></i>
                     Add Admin
                    </h4>
                  </section>
        <div class="row">
{{-- <div class="col-md-2"></div> --}}
            <div class="col-md-12">
                <div class="box box-default">
                    <div class="box-header">
                        {{-- <center><h3>Add New Admin</h3><cemter> --}}
                    </div>
                    <div class="box-body">
                      <div class="row">
                          <div class="col-md-2"></div>
                          <div class="col-md-8">
                                @include('admin.includes.logs')
                                <form action="{{ route('addadmin') }}" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                        <div class="form-group row {{ $errors->has('name') ? 'has-error': '' }}">
                                            <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>
                                            <div class="col-md-8">
                                                <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                                               {{-- <span class="alert alert-danger">{{ $errors->first('name') }}</span>  --}}
                                            </div>
                                        </div>
                                        <div class="form-group row {{ $errors->has('email')? 'has-error':'' }}">
                                            <label for="email" class="col-md-4 col-form-label text-md-right">Email</label>
                                            <div class="col-md-8">
                                                <input type="email" name="email" class="form-control" value="{{ old('email') }}">
                                                {{-- <span class="alert alert-danger">{{ $errors->first('email') }}</span>  --}}

                                            </div>
                                        </div>
                                        <div class="form-group row {{ $errors->has('rolesname')? 'has-error':'' }}">
                                                <label for="rolesname" class="col-md-4 col-form-label text-md-right">Role</label>
                                                <div class="col-md-8">
                                                    @if(isset($roles))
                                                        <select name="rolesname" id="rolesname" class="form-control">
                                                            @foreach($roles as $role)
                                                                <option value="{{ $role->role }}">{{ $role->role }}</option>
                                                            @endforeach
                                                        </select>
                                                    @endif
                                                </div>
                                        </div>
                                       
                                        <div class="form-group row {{ $errors->has('password') ? 'has-error':'' }}">
                                            <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>
                                            <div class="col-md-8">
                                                <input type="password" name="password" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row {{ $errors->has('confirm_password') ? 'has-error':''}}">
                                            <label for="confirm_password" class="col-md-4 col-form-label text-md-right">Confirm Password</label>
                                            <div class="col-md-8">
                                                <input type="password" name="confirm_password" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-6 col-md-offset-4">
                                                <button type="submit" class="btn btn-success">
                                                    <i class="fa fa-point"></i> Submit
                                                </button>
                                                <!--<a href="{{ route('admins') }}" style="float:right" class="btn btn-primary"><i class="fa fa-arrow-left" ></i> Back</a>-->
                                            </div>
                                        </div>
                                   </form>
                          </div>
                          <div class="col-md-2"></div>
                      </div>
                    </div>


                </div>
            </div>
            {{-- <div class="col-md-2"></div> --}}
        </div>
        


    </section>



</div>
@endsection