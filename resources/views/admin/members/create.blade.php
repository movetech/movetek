@extends('mainlayout')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
        <section class=" content-header text-center">
                <h4><i class="fa fa-asset bg-secondary"></i>
                  Add New Staff Member
                </h4>
              </section>
    <!-- Content Header (Page header) -->
    
    <!-- Main content -->
    <section class="content">
        <div class="row">
{{-- <div class="col-md-2"></div> --}}
            <div class="col-md-12">
                <div class="box box-default">
                    <div class="box-header">

                    </div>
                    <div class="box-body">
                      <div class="row">
                          <div class="col-md-2"></div>
                          <div class="col-md-8">
                             @include('admin.includes.logs')
                                <form action="{{ route('members.post') }}" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                        <div class="form-group row {{ $errors->has('name') ? 'has-error': '' }}">
                                            <label for="name" class="col-md-4 col-form-label text-md-right">Full Name</label>
                                            <div class="col-md-8">
                                                <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                                               {{-- <span class="alert alert-danger">{{ $errors->first('name') }}</span> --}}
                                            </div>
                                        </div>
                                        <div class="form-group row {{ $errors->has('username')? 'has-error': ''}}">
                                                <label for="username" class="col-md-4 col-form-label text-md-right">username</label>
                                                <div class="col-md-8">
                                                    <input type="text" name="username" class="form-control" value="{{ old('username') }}">
                                                </div>
                                            </div>
                                         <div class="form-group row {{ $errors->has('phone')? 'has-error':'' }}">
                                            <label for="phone" class="col-md-4 col-form-label text-md-right">Phone</label>
                                            <div class="col-md-8">
                                                <input type="text" name="phone" class="form-control" value="{{ old('phone') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row {{ $errors->has('location') ?'has-error':'' }}">
                                                <label for="location" class="col-md-4 col-form-label text-md-right">Sub County</label>
                                                <div class="col-md-8">
                                                    <select name="location" id="location" class="form-control" value="{{ old('location') }}">
                                                        <option value="">Select...</option>
                                                        @if(isset($location))
                                                            @foreach($location as $l)
                                                                <option value="{{ $l->name }}">{{ $l->name }}</option>
                                                            @endforeach

                                                        @endif  
                                                    </select>
                                                   
                                                </div>
                                        </div>
                                        <div class="form-group row {{ $errors->has('department') ? 'has-error':'' }}">
                                                <label for="department" class="col-md-4 col-form-label text-md-right">Department</label>
                                                <div class="col-md-8">
                                                    <select name="department" id="department" class="form-control" value="{{ old('department') }}">
                                                        <option value="">Select..</option>
                                                        @if(isset($department))
                                                            @foreach($department as $dep)
                                                                <option value="{{ $dep->department_code}}">{{ $dep->department_code}}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row {{ $errors->has('department') ? 'has-error':'' }}">
                                                <label for="department" class="col-md-4 col-form-label text-md-right">Ministry</label>
                                                <div class="col-md-8">
                                                    <select name="ministry" id="ministry" class="form-control">
                                                        <option value="">Select..</option>
                                                        @if(isset($ministries))
                                                            @foreach($ministries as $ministry)
                                                                <option value="{{ $ministry->name}}">{{ $ministry->name}}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        <div class="form-group row {{ $errors->has('email')? 'has-error':'' }}">
                                            <label for="email" class="col-md-4 col-form-label text-md-right">Email</label>
                                            <div class="col-md-8">
                                                <input type="email" name="email" class="form-control" value="{{ old('email') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row {{ $errors->has('password') ? 'has-error':'' }}">
                                            <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>
                                            <div class="col-md-8">
                                                <input type="password" name="password" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row {{ $errors->has('confirm_password') ? 'has-error':''}}">
                                            <label for="confirm_password" class="col-md-4 col-form-label text-md-right">Confirm Password</label>
                                            <div class="col-md-8">
                                                <input type="password" name="confirm_password" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-6 col-md-offset-4">
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="fa fa-add"></i> Submit
                                                </button>
                                            </div>
                                            <a href="{{ route('members.all') }}" class="btn btn-info" style="float:right"><i class="fa fa-arrow-left"></i> Back</a>
                                        </div>
                                   </form>
                          </div>
                          <div class="col-md-2"></div>
                      </div>
                    </div>


                </div>
            </div>
            {{-- <div class="col-md-2"></div> --}}
        </div>
        


    </section>



</div>
@endsection