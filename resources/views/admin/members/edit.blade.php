@extends('mainlayout')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
    <!-- Main content -->
    <section class="content">
        <div class="row">
{{-- <div class="col-md-2"></div> --}}
            <div class="col-md-12">
                <div class="box box-default">
                    <div class="box-header">

                    </div>
                    <div class="box-body">
                      <div class="row">
                          <div class="col-md-2"></div>
                          <div class="col-md-8">
                             @include('admin.includes.logs')
                                <form action="{{ route('member.update', $member->id) }}" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                        <div class="form-group row {{ $errors->has('name') ? 'has-error': '' }}">
                                            <label for="name" class="col-md-4 col-form-label text-md-right">Full Name</label>
                                            <div class="col-md-8">
                                                <input type="text" name="name" class="form-control" value="{{ $member->name }}">
                                               {{-- <span class="alert alert-danger">{{ $errors->first('name') }}</span> --}}
                                            </div>
                                        </div>
                                        <div class="form-group row {{ $errors->has('username')? 'has-error': ''}}">
                                                <label for="username" class="col-md-4 col-form-label text-md-right">username</label>
                                                <div class="col-md-8">
                                                    <input type="text" name="username" class="form-control" value="{{ $member->username }}">
                                                </div>
                                            </div>
                                         <div class="form-group row {{ $errors->has('phone')? 'has-error':'' }}">
                                            <label for="phone" class="col-md-4 col-form-label text-md-right">Phone</label>
                                            <div class="col-md-8">
                                                <input type="text" name="phone" class="form-control" value="{{ $member->phone }}">
                                            </div>
                                        </div>
                                        <div class="form-group row {{ $errors->has('location') ?'has-error':'' }}">
                                                <label for="location" class="col-md-4 col-form-label text-md-right">Location</label>
                                                <div class="col-md-8">
                                                    <select name="location" id="location" class="form-control" value="{{ $member->location }}">
                                                        <option value="">Select...</option>
                                                        @if(isset($location))
                                                            @foreach($location as $l)
                                                                <option value="{{ $l->name }}">{{ $l->name }}</option>
                                                            @endforeach

                                                        @endif  
                                                    </select>
                                                   
                                                </div>
                                        </div>
                                        <div class="form-group row {{ $errors->has('department') ? 'has-error':'' }}">
                                                <label for="department" class="col-md-4 col-form-label text-md-right">Department</label>
                                                <div class="col-md-8">
                                                    <select name="department" id="department" class="form-control" value="{{ $member->department }}">
                                                        <option value="">Select..</option>
                                                        @if(isset($department))
                                                            @foreach($department as $dep)
                                                                <option value="{{ $dep->name }}">{{ $dep->name }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        <div class="form-group row {{ $errors->has('email')? 'has-error':'' }}">
                                            <label for="email" class="col-md-4 col-form-label text-md-right">Email</label>
                                            <div class="col-md-8">
                                                <input type="email" name="email" class="form-control" value="{{ $member->email }}">
                                            </div>
                                        </div>
                                  
                                        <div class="form-group">
                                            <div class="col-md-8 col-md-offset-4">
                                                <form action="{{ route('member.update', $member->id) }}" method="post">
                                                    <input type="hidden" name="_method" value="PUT">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <input type="submit" name="" value="Update" class="btn btn-success">
                                                    <a style= "float:right" href="{{ route('members.all') }}" class="btn btn-info"><i class="fa fa-arrow-left"></i> Back</a>
                                                </form>
                                            </div>
                                        </div>
                                   </form>
                          </div>
                          <div class="col-md-2"></div>
                      </div>
                    </div>


                </div>
            </div>
            {{-- <div class="col-md-2"></div> --}}
        </div>
        


    </section>



</div>
@endsection