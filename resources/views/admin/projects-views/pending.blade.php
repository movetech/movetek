@extends('mainlayout')
@section('content')
<script src="{{asset('https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js')}}"></script>
    <div class="content-wrapper">
            <section class=" content-header text-center">
                    <h4><i class="fa fa-asset bg-secondary"></i>
                 Pending Projects
                    </h4>
                  </section>
                  @include('messages.custom')
        <section class="content">
            <div class="row">
                {{-- <div class="col-md-2"></div> --}}
                <div class="col-md-12">
                    <div class="box box-default">
                        
                        
                        <!-- Search form -->
  <div class="md-form mt-0">
      </div>
                        <div class="box-body">
                            @if(count($pendings) >0)
                            <table class="table table-bordered table-responsive" id="search">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Financial Year</th>
                                        <th>Contructor</th>
                                        <th>Project Cost</th>
                                        <th>Date Awarded</th>
                                        <th>Amount Paid</th>
                                        <th>Amount Remaining</th>
                                         <th>Date Paid</th>
                                         <th>Project Duration</th>
                                         <th>Status</th>
                                        
                                    </tr>
                                    <tr>
                                            <td></td>
                                            <td><input type="text" name="search" id="name" class="form-control"></td>
                                            <td></td>
                                            <td><input type="text" name="search" id="financial_year" class="form-control"></td>
                                            <td><input type="text" name="search" id="contructor" class="form-control"></td>
                                            <td></td>
                                            <td><input type="text" name="search" id="date_awarded" class="form-control"></td>
                                            <td></td>
                                            <td></td>
                                            <td><input type="text" name="search" id="date_paid" class="form-control"></td>
                                            <td></td>
                                            <td><input type="text" name="search" id="status" class="form-control"></td>
                                        </tr>
                                </thead>
                              
                                    {{-- <tbody> --}}
                                        <tr>
                                            <tbody>
                                                @foreach($pendings as $pending)
                                                {{-- <tbody> --}}
                                                    <tr>
                                               
                                                        <td>{{ $pending->id}}</td>
                                                        <td>{{ $pending->name}}</td>
                                                        <td>{{ $pending->description}}</td>
                                                        <td>{{ $pending->financial_year}}</td>
                                                        <td>{{ $pending->contractor}} </td>
                                                        <td>{{ $pending->date_awarded}}</td>
                                                        <td>{{ $pending->amount_paid}}</td>
                                                        <td>{{ $pending->date_paid}}</td>
                                                        <td>{{ $pending->amount_remaining}}</td>
                                                        <td>{{ $pending->date_topay}}</td>
                                                        <td>{{ $pending->duration}}</td>
                                                        <td>{{ $pending->status}}</td>
                                                        <td class="pt-2-half">
                                                            {{$pending->created_at->format('d/m/Y')}}
                                                        </td>
                                                        <td  class="pt-2-half">
                                                            <a href="" style="float:left;" data-placement="top" data-toggle="tooltip" title="view"><button class="btn btn-info btn-xs pull-right " data-title="view" data-toggle="modal" data-target="#view" ><span class="fa fa-eye"></span></button></a>
                                                        </td>
                                                        
                                                                    
                                        
                                                    </tr>
                                                {{-- </tbody> --}}
                                            @endforeach
                                       
                                        {{ $pendings->links() }}
                                        @endif
                                                  
                                            </tbody>
                                        </tr>
                                       
                            </table>
                           
                        </div>
                    </div>
                </div>
                {{-- <div class="col-md-2"></div> --}}
            </div>
        </section>
    </div>

@endsection