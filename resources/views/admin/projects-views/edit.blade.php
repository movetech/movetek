@extends('mainlayout')

@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
     <section class=" content-header text-center">
                    <h4><i class="fa fa-asset bg-secondary"></i>
                  Edit Project
                    </h4>
                  </section>
                  @include('messages.custom')
        <section class="content">
    <!-- Main content -->
    <section class="content">
        <div class="row">
<div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="box box-primary">
                    <div class="box-body">
                       <div class="card">
                           <div class="card-body">
                               @include('messages.custom')
                            <form action="{{ route('update-project', $projects->REF) }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                               
                                <div class="form-group row{{ $errors->has('name')? 'has-error':''}}">
                                    <label for="name" class="col-md-4 form-label text-md-right">Project Name</label>
                                    <div class="col-md-8">
                                        <input type="text" name="name" class="form-control" value="{{ $projects->name }}">
                                        {{-- <span class="alert alert-danger">{{ $errors->first('name') }}</span> --}}
                                    </div>
                                </div>
                                <div class="form-group row{{ $errors->has('description')? 'has-error':''}}">
                                        <label for="description" class="col-md-4 form-label text-md-right">Project Description</label>
                                        <div class="col-md-8">
                                            <textarea name="description" class="form-control">
                                                {{ $projects->description }}
                                            </textarea>
                                            {{-- <span class="alert alert-danger">{{ $errors->first('name') }}</span> --}}
                                        </div>
                                    </div>
                                 <div class="form-group row{{ $errors->has('financial_year')? 'has-error':''}}">
                                    <label for="financial_year" class="col-md-4 form-label text-md-right">Financial Year</label>
                                    <div class="col-md-8">
                                        <input type="text" name="financial_year" class="form-control" value="{{ $projects->financial_year }}">
                                        {{-- <span class="alert alert-danger">{{ $errors->first('name') }}</span> --}}
                                    </div>
                                </div>
                                <div class="form-group row{{ $errors->has('contructor')? 'has-error':''}}">
                                        <label for="contructor" class="col-md-4 form-label text-md-right">Contractor Awarded</label>
                                        <div class="col-md-8">
                                            <input type="text" name="contractor" class="form-control" value="{{ $projects->contractor }}">
                                            {{-- <span class="alert alert-danger">{{ $errors->first('name') }}</span> --}}
                                        </div>
                                    </div>
                                <div class="form-group row {{ $errors->has('total_amount') ? 'has-error':''}}">
                                    <label for="total_amount" class="col-md-4 form-labe text-md-right">Total Project Cost</label>
                                    <div class="col-md-8">
                                        <input type="number" name="total_amount" id="total_amount" class="form-control" value="{{ $projects->amount }}">
                                         {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                    </div>
                                </div>
                                <div class="form-group row {{ $errors->has('award_date') ? 'has-error':''}}">
                                        <label for="award_date" class="col-md-4 form-labe text-md-right">Date Awarded</label>
                                        <div class="col-md-8">
                                            <input type="date" name="award_date" class="form-control" value="{{ $projects->date_awarded }}">
                                             {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                        </div>
                                </div>
                                <div class="form-group row {{ $errors->has('amount_paid') ? 'has-error':''}}">
                                        <label for="amount_paid" class="col-md-4 form-labe text-md-right">Amount Paid</label>
                                        <div class="col-md-8">
                                        <input type="number" name="amount_paid" id="amount_paid" class="form-control" value="{{ $projects->amount_paid }}">
                                             {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                        </div>
                                </div>
                                <div class="form-group row {{ $errors->has('date_paid') ? 'has-error':''}}">
                                    <label for="date_paid" class="col-md-4 form-labe text-md-right">Date Paid</label>
                                    <div class="col-md-8">
                                        <input type="date" name="date_paid" class="form-control" value="{{ $projects->date_paid }}">
                                         {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                    </div>
                            </div>
                                <div class="form-group row {{ $errors->has('amount_remaining') ? 'has-error':''}}">
                                        <label for="amount_remaining" class="col-md-4 form-labe text-md-right">Amount Remaining</label>
                                        <div class="col-md-8">
                                            <input type="number" name="amount_remaining" id="subt" class="form-control" value="{{ $projects->amount_remaining }}">
                                             {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                        </div>
                                </div>
                                <div class="form-group row {{ $errors->has('date_paid') ? 'has-error':''}}">
                                    <label for="date_paid" class="col-md-4 form-labe text-md-right">Date to be Paid</label>
                                    <div class="col-md-8">
                                        <input type="date" name="date_topay" class="form-control" value="{{ $projects->date_topay }}">
                                         {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                    </div>
                            </div>
                                
                                <div class="form-group row {{ $errors->has('next_payment_date') ? 'has-error':''}}" id="payment-phase-div">
                                        <label for="next_payment_date" class="col-md-4 form-label text-md-right">Phase</label>
                                        <div class="col-md-8" style="">
                                            <a href="javascript:void(0);" class="btn btn-primary btn-sm" id="add_btn" title="Add phase">Add</a>
                                            {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                        </div>
                                </div>

                                    <div class="form-group row {{ $errors->has('duration') ? 'has-error':''}}">
                                        <label for="duration" class="col-md-4 form-label text-md-right">Project Duration</label>
                                        <div class="col-md-8">
                                            <input type="text" name="duration" class="form-control" value="{{ $projects->duration }}">
                                             {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                        </div>
                                    </div>
                                    <div class="form-group row {{ $errors->has('ministry') ? 'has-error':''}}">
                                            <label for="ministry" class="col-md-4 form-label text-md-right">Ministry</label>
                                            <div class="col-md-8">
                                                <input type="text" name="ministry" class="form-control" value="{{ $projects->ministry }}">
                                                 {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                            </div>
                                    </div>
                                    <div class="form-group row {{ $errors->has('sub_county') ? 'has-error':''}}">
                                            <label for="subcounty" class="col-md-4 form-label text-md-right">Sub County</label>
                                            <div class="col-md-8">
                                                <input type="text" name="subcounty" class="form-control" value="{{ $projects->subcounty }}">
                                                 {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                            </div>
                                    </div>
                                    <div class="form-group row {{ $errors->has('department') ? 'has-error':''}}">
                                            <label for="department_code" class="col-md-4 form-label text-md-right">Department</label>
                                            <div class="col-md-8">
                                                <select name="department_code" id="department" class="form-control">
                                                    {{ $projects->department }}
                                                    @if(isset($departments))

                                                        @foreach($departments as $department)
                                                            <option value="{{ $department->name }}">{{ $department->name }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                                 {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                            </div>
                                    </div>
                                    <div class="col-md-8 col-md-offset-4">

                                            <form action="{{ route('update-project',$projects->id ) }}" method="POST">
                                                    <input type="hidden" name="_method" value="PUT">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <input type="submit" class="btn btn-primary" value="Update">
                                            </form>   
                                            <a href="{{ route('projects.showall') }}" class="btn btn-primary pull-right"><i class="fas fa-window-close"></i> Cancel</a>                                 
                                    </div>
                           </div>
                       </div>
                    </div>


                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
        


    </section>



</div>
<script type="text/javascript">
    $(document).ready(function(){
        var maxField = 10; //Input fields increment limitation
        var addButton = $('#add_btn'); //Add button selector
        //var phase=1;
        var wrapper = $('#payment-phase-div'); //Input field wrapper
        var fieldHTML = '<div class="col-md-8 col-md-offset-4"><label>Next Phase</label><input type="date" class="form-control" name="next_payment_date[]" value="" required/><br/><div class="col-md-8 col-md-offset-4"></div><input type="text" class="form-control" name="payment[]" value="" placeholder="Amount to be Paid" required/><br/><a href="javascript:void(0);" class="btn btn-danger btn-sm remove_button">Remove</a></div>'; //New input field html

        ; //New input field html 
        var x = 1; //Initial field counter is 1
        //Once add button is clicked
        $(addButton).click(function(){
            //Check maximum number of input fields
            if(x < maxField){ 
                x++; //Increment field counter
                $(wrapper).append(fieldHTML); //Add field html
            }
            //phase=phase++;
        });
        
        //Once remove button is clicked
        $(wrapper).on('click', '.remove_button', function(e){
            e.preventDefault();
            $(this).parent('div').remove(); //Remove field html
            x--; //Decrement field counter
        });
    });
//this calculates values automatically 
sum();
$("#total_amount, #amount_paid").on("keydown keyup", function() {
sum();
});

function sum() {
    var num1 = document.getElementById('amount_paid').value;
    var num2 = document.getElementById('total_amount').value;
    //var result = parseInt(num1) + parseInt(num2);
    var result = parseInt(num2) - parseInt(num1);
    if (!isNaN(result)) {
        if(result<0){
            alert("remaining balance cannot be negative");
            $('#amount_paid').val("");
            $('#total_amount').val("");
            $('#subt').val("");
        }
        else{
            document.getElementById('subt').value = result;
        }
        
    }
}
    </script>
@endsection