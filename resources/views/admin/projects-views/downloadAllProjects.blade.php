<style>
    table {
      width: 100%;
      border: 1px solid #fff;
      text-align: left;
    }
    
    th, td {
    padding: 15px;
    text-align: left;
    }
    
    tr:nth-child(even){background-color: #f2f2f2}
    
    th {
      background-color: #4CAF50;
      color: white;
    }
    #main-body-div{
        border: 2px solid rgba(47,73,100,.9);
        border-radius:2px;
        padding:20px;
    }
    </style>
<div id="main-body-div">
   <div class="content-wrapper ">
    <div style="width:100%; height:100px; text-align:center" class="text-center">
        <img src="{{ public_path('/storage/images/logos/mandera-county.jpg')}}" alt="" style="height:50px;align:center;border-radius:50%" style="img-thumbnail">
        <h3 style="font-style:Sans Serif; font-weight:bold; color:brown">MANDERA COUNTY GOVERNMENT</h3>
        <h5 style="center">Summary of All Projects</h5>
    </div>
        <section class="content ">
            <div class="row">
                {{-- <div class="col-md-2"></div> --}}
                <div class="col-md-12">
                    <div class="box box-default">  
                        <!-- Search form -->
  <div class="md-form mt-0">
      </div>
                            @if(count($projects) >0)
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Financial Year</th>
                                        <th>Ministry</th>
                                        <th>Sub County</th>
                                        <th>Department</th>
                                        <th>Contructor</th>
                                        <th>Initial  Cost</th>
                                        <th>Date Awarded</th>
                                        <th>Amount Paid</th>
                                         <th>Balance</th>
                                         <th>Duration</th>
                                         <th>Supplementary</th>
                                         <th>Final Cost</th>
                                        
                                        
                                    </tr>
                                  
                                </thead>
                              
                                        <tr>
                                            <tbody class="table-responsive">
                                                @foreach($projects as $project)
                                                    <tr>
                                                     {{-- <p class="hidden">
                                                            {{$phases=App\Phases::where('REF',$project->REF)->get()}}
                                                            </p>  --}}
                                                        <td>{{ $project->id}}</td>
                                                        <td>{{ $project->name}}</td>
                                                        <td>{{ $project->financial_year}}</td>
                                                        <td>{{ $project->ministry }}</td>
                                                        <td>{{ $project->subcounty }}</td>
                                                        <td>{{ $project->department_code }}</td>
                                                        <td>{{ $project->contractor}} </td>
                                                        <td>{{ $project->amount }}</td>
                                                        <td>{{ $project->date_awarded }}</td>
                                                        <td>{{ $project->amount_paid }}</td>
                                                      
                                                        <td>{{ $project->amount_remaining }}</td>
                                                       
                                                        <td>{{ $project->duration }}</td>
                                                        <td>{{ $project->supplementary_budget }}</td>
                                                        <td>{{ $project->total_budget }}</td>
                                                       {{--  
                                                        @if(count($phases)>0)
                                                        <td>{{count($phases)+1}}</td>
                                                        @else
                                                        <td>1</td>
                                                        @endif  --}}
                                                       
                                        
                                                    </tr>

                                                {{-- </tbody> --}}
                                            @endforeach
                                       
                                      
                                        @else
                                       
                                              @endif        
                                        </tr>
                                   
                            </table>
                        </div>  
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
</div>