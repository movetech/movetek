@extends('mainlayout')
@section('content')
<link rel="stylesheet" href="{{asset('//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css')}}">
<script src="{{asset('https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js')}}"></script> 
<style>
#btnSearch,
#btnClear{
    display: inline-block;
    vertical-align: top;
    float:left;
    margin:5px;
}
</style>
   <div class="content-wrapper ">
            <section class=" content-header text-center">
                    <h4><i class="fa fa-asset bg-secondary"></i>
                        All Projects
                      
                        <a style="float:right" href="{{route('projects.create')}}" class="btn btn-info btn-sm"><i class="fa fa-plus">New Project</i></a>
                    </h4>
                    <div class="row">
                            <div class="col-sm-12 text-center">
                                <button id="btnSearch" class="btn btn-sm btn-primary btn-md center-block"  OnClick="btnSearch_Click" >EXCEL</button>
                                 <a href="{{ route('downAllProjectsInPdf') }}"><button id="btnClear" class="btn btn-sm btn-primary btn-md center-block"   OnClick="btnClear_Click" >PDF</button></a>
                                 <a href="{{route('budget.show')}}"><button id="btnClear" class="btn btn-sm btn-info btn-md center-block"   OnClick="btnClear_Click" >BUDGET INFO</button></a>
                             </div>
                            </div>
                   
                  </section>
                  @include('messages.custom')
        <section class="content ">
            <div class="row">
                {{-- <div class="col-md-2"></div> --}}
                <div class="col-md-12">
                    <div class="box box-default">  
                        <!-- Search form -->
  <div class="md-form mt-0">
      </div>
                        <div class="container-fluid box-body">
                            @if(count($projects) >0)
                            <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Financial Year</th>
                                        <th>Ministry</th>
                                        <th>Sub County</th>
                                        <th>Department</th>
                                        <th>Contructor</th>
                                        <th>Initial  Cost</th>
                                        <th>Date Awarded</th>
                                        <th>Amount Paid</th>
                                         <th>Balance</th>
                                         <th>Duration</th>
                                         <th>Supplementary</th>
                                         <th>Final Cost</th>
                                        <th>Phases</th>
                                         <th>Status</th>
                                         <th>View</th>
                                         <th>Edit</th>
                                         <th>Delete</th>
                                        
                                    </tr>
                                    <tr>
                                            <td></td>
                                        
                                            <td><input type="text" placeholder="Search1" aria-label="Search"  name="search" id="project_name" class="form-control"></td>
                                            <td></td>
                                            <td><input type="text" placeholder="Search2" aria-label="Search"  name="search" id="ministry" class="form-control"></td>
                                            <td><input type="text" placeholder="Search3" aria-label="Search"  name="search" id="subcounty" class="form-control"></td>
                                            <td><input type="text" placeholder="Search4" aria-label="Search"  name="search" id="department" class="form-control"></td>
                                            <td><input type="text" placeholder="Search5" aria-label="Search"  name="search" id="contractor" class="form-control"></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td><input type="text" name="search" placeholder="status" id="status" class="form-control"></td>
                                            <td></td>
                                            <td></td>
                                            
                                            <td></td>
                                            
                                           
                                            
                                            
                                        </tr>
                                </thead>
                              
                                    {{-- <tbody> --}}
                                        <tr>
                                            <tbody class="table-responsive">
                                                @foreach($projects as $project)
                                                {{-- <tbody> --}}
                                                    <tr>
                                               {{-- <tbody> --}}
                                                    <p class="hidden">
                                                            {{$phases=App\Phases::where('REF',$project->REF)->get()}}
                                                            </p>
                                                        <td>{{ $project->id}}</td>
                                                        <td>{{ $project->name}}</td>
                                                        <td>{{ $project->financial_year}}</td>
                                                        <td>{{ $project->ministry }}</td>
                                                        <td>{{ $project->subcounty }}</td>
                                                        <td>{{ $project->department_code }}</td>
                                                        <td>{{ $project->contractor}} </td>
                                                        <td>{{ $project->amount }}</td>
                                                        <td>{{ $project->date_awarded }}</td>
                                                        <td>{{ $project->amount_paid }}</td>
                                                      
                                                        <td>{{ $project->amount_remaining }}</td>
                                                       
                                                        <td>{{ $project->duration }}</td>
                                                        <td>{{ $project->supplementary_budget }}</td>
                                                        <td>{{ $project->total_budget }}</td>
                                                        
                                                        @if(count($phases)>0)
                                                        <td>{{count($phases)+1}}</td>
                                                        @else
                                                        <td>1</td>
                                                        @endif
                                                        <td  class="dropdown">
                                                              
                                                                        {{ $project->status }}
                                                                            @if($project->status == 'initiated')
                                                                                    <button class="btn btn-primary btn-xs dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                        <span class="caret"></span>
                                                                                    </button>
                                                                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton" role="menu">
                                                                                    <li class="dropdown-item bg-info">
                                                                                                        <input type="checkbox" name="status" value="completed" autocomplete="off"> Mark as completed<br>
                                                                                                        <a style="width:25px;height:25px;" href="{{route('projects.mark',$project->REF)}}" onclick="if (!confirm('By clicking this button means the phase is or project is complete?')) { return false }" title="save" class="btn btn-success btn-sm">
                                                                                                                <i class="fa fa-save"></i></a>
                                                                                                
                                                            

                                                                                    </li>


                                                                                    </ul>
                                                                        @endif
                                                                      
                                                        </td>
                                                        <td  class="pt-2-half">
                                                            <a href="{{route('projects.show',$project->id)}}" style="float:left;" data-placement="top" data-toggle="tooltip" title="view"><button class="btn btn-success btn-xs pull-right " data-title="view" data-toggle="modal" data-target="#view" ><span class="fa fa-eye"></span></button></a>
                                                        </td>
                                                        <td class="pt-2-half">
                                                            <a href="{{route('edit-project',$project->REF)}}" style="float:left;" data-placement="top" data-toggle="tooltip" title="view"><button class="btn btn-info btn-xs pull-right " data-title="view" data-toggle="modal" data-target="#view" ><span class="fa fa-edit"></span></button></a>
                                                        </td>
                                                        <td class="pt-2-half">
                                                            <form action="{{ route('delete-project', $project->REF) }}" method="post" type="hidden">
                                                                <input type="hidden" name="_method" value="DELETE">
                                                                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                                                <button  type="submit" class="btn btn-danger btn-xs pull-right" onclick="if (!confirm('Are you sure you want to proceed?')) { return false }"><span class="fa fa-trash"></span></button>
                                                            </form>
                                                        </td>
                                                        
                                                                    
                                        
                                                    </tr>

                                                {{-- </tbody> --}}
                                            @endforeach
                                       
                                        {{ $projects->links() }}
                                      
                                        @else
                                        <div class="panel panel-success">
                                                <div class="panel-heading"><h4>No Projects Running...</h4></div>
                                              </div>
                                              @endif        
                                            {{-- </tbody> --}}
                                        </tr>
                                       
                            </table>
                        </div>  
                        </div>
                    </div>
                </div>
                {{-- <div class="col-md-2"></div> --}}
            </div>
        </section>
    </div>
    <script type="text/javascript">
        // $(document).ready(function(){
            $('#project_name, #ministry, #subcounty, #department, #contractor,#status').on('keyup', function(){
                $value = $(this).val();
                var token = $("meta[name='csrf-token']").attr("content");
                $.ajax({
                    type:"GET",
                    url:'{{ URL::to('admin/projects/search') }}',
                    data:{
                        'search':$value,'_token':token,
                    },
                        success:function(data){
                            $('tbody').html(data);
                        }
                    
                });
            });
        // });
    </script>

@endsection