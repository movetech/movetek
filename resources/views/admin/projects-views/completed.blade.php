@extends('mainlayout')
@section('content')
<script src="{{asset('https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js')}}"></script>
    <div class="content-wrapper">
            <section class=" content-header text-center">
                    <h4><i class="fa fa-asset bg-secondary"></i>
                       Completed  Projects
                    </h4>
                  </section>
                  @include('messages.custom')
        <section class="content">
            <div class="row">
                {{-- <div class="col-md-2"></div> --}}
                <div class="col-md-12">
                        <a href="{{ route('downloadAllCompletedProjects') }}" class="btn btn-danger btn-sm"><i class="fa fa-arrow-down"></i> Export to pdf</a>
                    <div class="box box-default">
                        
                        
                        <!-- Search form -->
  <div class="md-form mt-0">
      </div>
                        <div class="box-body">
                            @if(count($completes) >0)
                            <table class="table table-bordered table-responsive" id="search">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Financial Year</th>
                                        <th>Contructor</th>
                                        <th>Project Cost</th>
                                        <th>Date Awarded</th>
                                        <th>Amount Paid</th>
                                        <th>Amount Remaining</th>
                                        <th>Date Paid</th>
                                        <th>Project Duration</th>
                                        <th>Status</th>
                                        <th>View</th>
                                        
                                    </tr>
                                   
                                </thead>
                              
                                    {{-- <tbody> --}}
                                        <tr>
                                            <tbody>
                                                @foreach($completes as $complete)
                                                {{-- <tbody> --}}
                                                    <tr>
                                               
                                                        <td>{{ $complete->id}}</td>
                                                        <td>{{ $complete->name}}</td>
                                                        <td>{{ $complete->description}}</td>
                                                        <td>{{ $complete->financial_year}}</td>
                                                        <td>{{ $complete->contractor}} </td>
                                                        <td>{{ $complete->date_awarded}}</td>
                                                        <td>{{ $complete->amount_paid}}</td>
                                                        <td>{{ $complete->date_paid}}</td>
                                                        <td>{{ $complete->amount_remaining}}</td>
                                                        <td>{{ $complete->date_topay}}</td>
                                                        <td>{{ $complete->duration}}</td>
                                                        <td>{{ $complete->status}}</td>
                                                        <td  class="pt-2-half">
                                                            {{$complete->created_at->format('d/m/Y')}}
                                                        </td>
                                                        <td  class="pt-2-half">
                                                            <a href="" style="float:left;" data-placement="top" data-toggle="tooltip" title="view"><button class="btn btn-info btn-xs pull-right " data-title="view" data-toggle="modal" data-target="#view" ><span class="fa fa-eye"></span></button></a>
                                                        </td>
                                                        
                                                                    
                                        
                                                    </tr>
                                                {{-- </tbody> --}}
                                            @endforeach
                                       
                                        {{ $completes->links() }}
                                        @endif
                                                  
                                            </tbody>
                                        </tr>
                                       
                            </table>
                           
                        </div>
                    </div>
                </div>
                {{-- <div class="col-md-2"></div> --}}
            </div>
        </section>
    </div>

@endsection