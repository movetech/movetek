<style>
        table {
          width: 100%;
          border: 1px solid #fff;
          text-align: left;
        }
        
        th, td {
        padding: 15px;
        text-align: left;
        }
        
        tr:nth-child(even){background-color: #f2f2f2}
        
        th {
          background-color: #4CAF50;
          color: white;
        }
        #main-body-div{
            border: 2px solid rgba(47,73,100,.9);
            border-radius:2px;
            padding:20px;
        }
        </style>
    <div id="main-body-div">
       <div class="content-wrapper ">
        <div style="width:100%; height:100px; text-align:center" class="text-center">
            <img src="{{ public_path('/storage/images/logos/mandera-county.jpg')}}" alt="" style="height:50px;align:center;border-radius:50%" style="img-thumbnail">
            <h3 style="font-style:Sans Serif; font-weight:bold; color:brown">MANDERA COUNTY GOVERNMENT</h3>
            <h5 style="center">Summary of All Completed Projects</h5>
        </div>
        <section class="content">
            <div class="row">
                {{-- <div class="col-md-2"></div> --}}
                <div class="col-md-12">
                    <div class="box box-default">
                        
                        
                        <!-- Search form -->
  <div class="md-form mt-0">
      </div>
                        <div class="box-body">
                            @if(count($completes) >0)
                            <table class="table table-bordered table-responsive" id="search">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Financial Year</th>
                                        <th>Contructor</th>
                                        <th>Project Cost</th>
                                        <th>Date Awarded</th>
                                        <th>Amount Paid</th>
                                        <th>Amount Remaining</th>
                                        <th>Date Paid</th>
                                        <th>Project Duration</th>
                                        
                                    </tr>
                                   
                                </thead>
                              
                                    {{-- <tbody> --}}
                                        <tr>
                                            <tbody>
                                                @foreach($completes as $complete)
                                                {{-- <tbody> --}}
                                                    <tr>
                                               
                                                        <td>{{ $complete->id}}</td>
                                                        <td>{{ $complete->name}}</td>
                                                        <td>{{ $complete->description}}</td>
                                                        <td>{{ $complete->financial_year}}</td>
                                                        <td>{{ $complete->contractor}} </td>
                                                        <td>{{ $complete->date_awarded}}</td>
                                                        <td>{{ $complete->amount_paid}}</td>
                                                        <td>{{ $complete->date_paid}}</td>
                                                        <td>{{ $complete->amount_remaining}}</td>
                                                        <td>{{ $complete->date_topay}}</td>
                                                        <td>{{ $complete->duration}}</td>
                                                        
                                                        
                                                                    
                                        
                                                    </tr>
                                                {{-- </tbody> --}}
                                            @endforeach
                                       
                                        @endif
                                                  
                                            </tbody>
                                        </tr>
                                       
                            </table>
                           
                        </div>
                    </div>
                </div>
                {{-- <div class="col-md-2"></div> --}}
            </div>
        </section>
    </div>

