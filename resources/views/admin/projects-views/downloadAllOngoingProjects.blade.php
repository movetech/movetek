<style>
        table {
          width: 100%;
          border: 1px solid #fff;
          text-align: left;
        }
        
        th, td {
        padding: 15px;
        text-align: left;
        }
        
        tr:nth-child(even){background-color: #f2f2f2}
        
        th {
          background-color: #4CAF50;
          color: white;
        }
        #main-body-div{
            border: 2px solid rgba(47,73,100,.9);
            border-radius:2px;
            padding:20px;
        }
        </style>
    <div id="main-body-div">
       <div class="content-wrapper ">
        <div style="width:100%; height:100px; text-align:center" class="text-center">
            <img src="{{ public_path('/storage/images/logos/mandera-county.jpg')}}" alt="" style="height:50px;align:center;border-radius:50%" style="img-thumbnail">
            <h3 style="font-style:Sans Serif; font-weight:bold; color:brown">MANDERA COUNTY GOVERNMENT</h3>
            <h5 style="center">Summary of All Ongoing Projects</h5>
        </div>
        <section class="content">
            <div class="row">
                {{-- <div class="col-md-2"></div> --}}
                <div class="col-md-12">
                    <div class="box box-default">
                        
                        
                        <!-- Search form -->
  <div class="md-form mt-0">
      </div>
                        <div class="box-body">
                            @if(count($ongoings) >0)
                            <table class="table table-bordered table-responsive" id="search">
                                <thead>
                                    <tr>
                                        <th>SR/NO</th>
                                        <th>Project</th>
                                        <th>Description</th>
                                        <th>Financial Year</th>
                                        <th>Project Cost</th>
                                        <th>Contractor</th>
                                        <th>Date Awarded</th>
                                        <th>Amount Paid</th>
                                        <th>Date Paid</th>
                                        <th>Amount Remaining</th>
                                        <th>Date to pay</th>
                                         <th>Project Duration</th>
                                         <th>Status</th>
                                         <th>Date</th>
                                         
                                        
                                    </tr>
                                    
                                </thead>
                              
                                    {{-- <tbody> --}}
                                        <tr>
                                            <tbody>
                                                @foreach($ongoings as $ongoing)
                                                {{-- <tbody> --}}
                                                    <tr>
                                               
                                                        <td>{{ $ongoing->id}}</td>
                                                        <td>{{ $ongoing->name}}</td>
                                                        <td>{{ $ongoing->description}}</td>
                                                        <td>{{ $ongoing->financial_year}}</td>
                                                        <td>{{ $ongoing->amount}} </td>
                                                        <td>{{ $ongoing->contractor}} </td>
                                                        <td>{{ $ongoing->date_awarded}}</td>
                                                        <td>{{ $ongoing->amount_paid}}</td>
                                                        <td>{{ $ongoing->date_paid}}</td>
                                                        <td>{{ $ongoing->amount_remaining}}</td>
                                                        <td>{{ $ongoing->date_topay}}</td>
                                                        <td>{{ $ongoing->duration}}</td>
                                                        <td>{{ $ongoing->status}}</td>
                                                        <td  class="pt-2-half">
                                                            {{$ongoing->created_at->format('d/m/Y')}}
                                                        </td>
                                                       
                                                                    
                                        
                                                    </tr>
                                                {{-- </tbody> --}}
                                            @endforeach
                                       
                                        @endif
                                                  
                                            </tbody>
                                        </tr>
                                       
                            </table>
                           
                        </div>
                    </div>
                </div>
                {{-- <div class="col-md-2"></div> --}}
            </div>
        </section>
    </div>

