@extends('mainlayout')
@section('content')
<script src="{{asset('https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js')}}"></script>
    <div class="content-wrapper">
            <section class=" content-header text-center">
                    <h4><i class="fa fa-asset bg-secondary"></i>
                        Ongoing Projects
                    </h4>
                  </section>
                  @include('messages.custom')
        <section class="content">
            <div class="row">

                {{-- <div class="col-md-2"></div> --}}
                <div class="col-md-12">
                    <a href="{{ route('downloadAllOngoingProjects') }}" class="btn btn-danger btn-sm"><i class="fa fa-arrow-down"></i> Export to pdf</a>
                    <div class="box box-default">
                        
                        
                        <!-- Search form -->
  <div class="md-form mt-0">
      </div>
                        <div class="box-body">
                            @if(count($ongoings) >0)
                            <table class="table table-bordered table-responsive" id="search">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Project</th>
                                        <th>Description</th>
                                        <th>Financial Year</th>
                                        <th>Project Cost</th>
                                        <th>Contractor</th>
                                        <th>Date Awarded</th>
                                        <th>Amount Paid</th>
                                        <th>Date Paid</th>
                                        <th>Amount Remaining</th>
                                        <th>Date to pay</th>
                                         <th>Project Duration</th>
                                         <th>Status</th>
                                         <th>Date</th>
                                        
                                    </tr>
                                    
                                </thead>
                              
                                    {{-- <tbody> --}}
                                        <tr>
                                            <tbody>
                                                @foreach($ongoings as $ongoing)
                                                {{-- <tbody> --}}
                                                    <tr>
                                               
                                                        <td>{{ $ongoing->id}}</td>
                                                        <td>{{ $ongoing->name}}</td>
                                                        <td>{{ $ongoing->description}}</td>
                                                        <td>{{ $ongoing->financial_year}}</td>
                                                        <td>{{ $ongoing->amount}} </td>
                                                        <td>{{ $ongoing->contractor}} </td>
                                                        <td>{{ $ongoing->date_awarded}}</td>
                                                        <td>{{ $ongoing->amount_paid}}</td>
                                                        <td>{{ $ongoing->date_paid}}</td>
                                                        <td>{{ $ongoing->amount_remaining}}</td>
                                                        <td>{{ $ongoing->date_topay}}</td>
                                                        <td>{{ $ongoing->duration}}</td>
                                                        <td>{{ $ongoing->status}}</td>
                                                        <td  class="pt-2-half">
                                                            {{$ongoing->created_at->format('d/m/Y')}}
                                                        </td>
                                                       
                                                                    
                                        
                                                    </tr>
                                                {{-- </tbody> --}}
                                            @endforeach
                                       
                                        {{$ongoings->links() }}
                                        @endif
                                                  
                                            </tbody>
                                        </tr>
                                       
                            </table>
                           
                        </div>
                    </div>
                </div>
                {{-- <div class="col-md-2"></div> --}}
            </div>
        </section>
    </div>

@endsection