@extends('mainlayout')
@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
     <section class=" content-header text-center">
                    <h4><i class="fa fa-asset bg-secondary"></i>
                        Supplementary Budget for <strong>{{ $project->REF }}</strong>
                    </h4>
    </section>
        <section class="content">
    <!-- Main content -->
    <section class="content">
        <div class="row">
<div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="box box-primary">
                    <div class="box-body">
                       <div class="card">
                           <div class="card-body">
                               @include('messages.custom')
                               <form action="{{ route('save-new-supplementary-budget', $project->REF) }}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                   <div class="form-group row">
                                       <label for="project" id="project" class="col-md-4 form-label text-md-right">Project Name</label>
                                       <div class="col-md-8">
                                           <input type="text" name="project" value="{{ $project->name }}" class="form-control">
                                       </div>
                                   </div>
                                   <div class="form-group row">
                                       <label for="phase" id="phase" class="col-md-4 form-label text-md-right">Phase</label>
                                       <div class="col-md-8 {{ $errors->has('name') ? 'has-error':''}}">
                                            <select name="phase" id="phase" class="form-control">
                                                <option value="{{ $project->name.'Phase:I'}}">{{ $project->name.'Phase:I' }}</option>
                                                @if(isset($project_phases))
                                                    @foreach($project_phases as $phase)
                                                        <option value="{{ $phase->name }}">{{ $phase->name }}</option>
                                            <small class="alert alert-danger">{{ $errors->first('name') }}</small>

                                                    @endforeach
                                                @endif
                                            </select>
                                       </div>
                                   </div>
                                   <div class="form-group row{{ $errors->has('amount') ? 'has-error':''}}">
                                       <label for="amount" name="amount" class="col-md-4 form-label text-md-right">Amount</label>
                                       <div class="col-md-8">
                                           <input type="number" name="amount" class="form-control">
                                           {{-- <span class="alert alert-danger">{{ $errors->first('amount') }}</span> --}}
                                       </div>
                                   </div>
                                   <div class="form-group row{{ $errors->has('amount') ? 'has-error':''}}">
                                    <label for="amount" name="amount" class="col-md-4 form-label text-md-right">Phase Count</label>
                                    <div class="col-md-8">
                                        <input type="text" name="count" value={{$count}} class="form-control">
                                        {{-- <span class="alert alert-danger">{{ $errors->first('amount') }}</span> --}}
                                    </div>
                                </div>
                                   <div class="col-md-8 col-md-offset-4">
                                       <button type="submit" class="btn btn-info"><i class="fa fa-arrow-right"></i> Submit</button>
                                       <a href="{{ route('projects.showall') }}" class="btn btn-danger pull-right"><i class="fa fa-arrow-left"></i> Cancel</a>
                                   </div>
                                   
                            </div>
                    </div>


                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
        


    </section>
       
</div>

@endsection