@extends('mainlayout')
@section('content')
<script src="{{asset('https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js')}}"></script>
    <div class="content-wrapper">
            <section class=" content-header text-center">
                    <h4><i class="fa fa-asset bg-secondary"></i>
                        Delayed  Projects
                    </h4>
                  </section>
                  @include('messages.custom')
        <section class="content">
            <div class="row">
                {{-- <div class="col-md-2"></div> --}}
                <div class="col-md-12">
                    <div class="box box-default">
                        
                        
                        <!-- Search form -->
  <div class="md-form mt-0">
      </div>
                        <div class="box-body">
                            
                            <table class="table table-bordered table-responsive" id="search">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Financial Year</th>
                                        <th>Contructor</th>
                                        <th>Project Cost</th>
                                        <th>Date Awarded</th>
                                        <th>Amount Paid</th>
                                        <th>Amount Remaining</th>
                                        <th>Date Paid</th>
                                        <th>Project Duration</th>
                                        <th>Status</th>
                                        <th>View</th>
                                        
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><input type="text" name="search" id="name" class="form-control"></td>
                                        <td></td>
                                        <td><input type="text" name="search" id="financial_year" class="form-control"></td>
                                        <td></td>
                                        <td></td>
                                        <td><input type="text" name="search" id="date_awarded" class="form-control"></td>
                                        <td></td>
                                        <td></td>
                                        <td><input type="text" name="search" id="date_paid" class="form-control"></td>
                                        <td></td>
                                        <td><input type="text" name="search" id="status" class="form-control"></td>
                                    </tr>
                                </thead>
                              
                                    {{-- <tbody> --}}
                                        <tr>
                                            <tbody>
                                          <!-- Loop data from database -->
                                                
                                            </tbody>
                                        </tr>
                                       
                            </table>
                           
                        </div>
                    </div>
                </div>
                {{-- <div class="col-md-2"></div> --}}
            </div>
        </section>
    </div>

@endsection