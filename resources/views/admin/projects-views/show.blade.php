@extends('mainlayout')
@section('content')
<script src="{{asset('https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js')}}"></script>
    <div class="content-wrapper">
            <section class=" content-header text-center">
                    <h4><i class="fa fa-asset bg-secondary"></i>
                        Project Details
                    </h4>
                  </section>
                 
        <section class="content">
            <div class="row">
                {{-- <div class="col-md-2"></div> --}}
                <div class="col-md-12">
                    <div class="box box-default">
      <section class="container-fluid alert alert-success" role="alert">
        <div class="card">
        <p class="card-header text-center font-weight-bold text-uppercase py-3"><strong>{{$specific_project->name}}</strong><span class="badge"></span></p>
        <a href="{{ route('supplementary-budget.create', [$specific_project->REF,$phases->count()+1])}}" class="btn btn-info pull-right"><i class="fa fa-add"></i> Add Supplementraty Budget</a>
                <div class="card">
          <h5 class="card-header h4"><strong>Project Ref:{{$specific_project->REF}}</strong></h4>
          <div class="card-body">
            <h5 class="card-title"><strong>General Details</strong></h5>
          <ul>
            <li>Contractor:{{$specific_project->contractor}}</li>
            <li>Ministry{{$specific_project->ministry}}</li>
            <li>Department:{{$specific_project->department}}</li>
            <li>Subcounty:{{$specific_project->subcounty}}</li>
            <li>Timeline:{{$specific_project->duration}}</li>
            <li>Project Cost:{{$specific_project->amount}}</li>
            <li>Date Awarded:{{$specific_project->date_awarded}}</li>
            <li>Status:{{$specific_project->status}}</li>
            <li>Amount Remaining:{{$specific_project->amount_remaining}}</li>
          </ul>
          <div class="card-header">
          <h5 class="card-title"><strong>Description</strong></h5>
          </div>
          
          <div class="card-body">
            <blockquote class="blockquote mb-0">
              <p>{{$specific_project->description}}</p>
              <footer class="blockquote-footer">Project Phases <cite title="Source Title">{{$phases->count()+1}}</cite></footer>
            </blockquote>
          </div>
           
            <div class="panel panel-success">
                <div class="panel-heading"><h4>Project Phases</h4></div>
              </div>
              @include('messages.custom')
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-info">
                        <div class="panel-heading">{{$specific_project->name."Phase I"}}</div>
                    <div style="color:black;" class="panel-body"> 
                        <h5>Project Ref:{{$specific_project->REF}}</h5>
                          <p class="card-text">Initial Budget:{{$specific_project->amount_paid}}</p>
                          @if($specific_project->status=="partially complete")
                          <p class="card-text">Status:complete</p>
                          @else
                          <p class="card-text">Status:{{$specific_project->status}}</p>
                          @endif
                          
                          <p class="card-text">Supplemented Budget:{{$specific_project->supplementary_budget}}</p>
                          <p class="card-text">Final Phase Budget:{{$specific_project->total_budget}}</p>
                          @if($specific_project->status=="initiated")
                          <a href="{{route('projects.mark',$specific_project->REF)}}" onclick="if (!confirm('By clicking this button means the phase is complete?')) { return false }"class="btn btn-info pull-right">Mark as Complete</a>
                          @endif
                        </div>
                  </div>
                  </div>
                  @if($phases->count()>0)
                @foreach($phases as $phase)
            <div class="col-md-6">
            <div class="panel panel-info">
                <div class="panel-heading">{{$phase->name}}</div>
            <div style="color:black;" class="panel-body"> 
                  <h5 class="card-title">{{$phase->REF}}</h5>
                  <p class="card-text">Budget:{{$phase->amount}}</p>
                  <p class="card-text">Date To be paid:{{$phase->date_to_be_paid}}</p>
                  <p class="card-text">Supplemented Cost:{{$phase->supplementary_budget}}</p>
                  <p class="card-text">Phase Cost:{{$phase->phase_cost}}</p>
                  <p class="card-text">Status:{{$phase->status}}</p>
                  @if($phase->status=="pending")
                  <a href="{{route('phase.activate',[$phase->id,$phase->REF])}}" onclick="if (!confirm('Are you sure you want to activate this phase, by doing so means the payment has been disbursed?')) { return false }"class="btn btn-primary">Activate Payment</a>
                  @endif
                  @if($phase->status=="initiated")
                  <a href="{{route('phase.mark',[$phase->id,$phase->REF])}}" onclick="if (!confirm('By clicking this button means the phase is complete?')) { return false }"class="btn btn-info pull-right">Mark as Complete</a>
                  @endif
                </div>
          </div>
          </div>
          @endforeach
        </div>
        
           
            @endif
          </div>
        </div>
          </div>
        </div>
                    </div>
                    <a href="javascript:history.back()" class="btn btn-default pull-right">Back</a>
                </div>
            </section>
           
    </div>

@endsection