@extends('mainlayout')
@section('content')
<script src="{{asset('https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js')}}"></script>
<script src="{{asset('html.js')}}"></script>
    <div  class="content-wrapper">
            <section class=" content-header text-center">
                    <h4><i class="fa fa-asset bg-secondary"></i>
                       Budget Information
                    </h4>
                    <button class="btn btn-xs btn-primary"onclick="generatePDF()">Download as PDF</button>
                  </section>
                 
                  @include('messages.custom')

        <section class="content">
               
            <div id="invoice" class="row">
                    <h4 class="text-center"><i class="fa fa-asset"></i>
                        Project Budget Information 
                     </h4>
                {{-- <div class="col-md-2"></div> --}}
                <div class="col-md-12">
                    <div class="box box-default">
                        
                        
                        <!-- Search form -->
  <div class="md-form mt-0">
                        <div   class="box-body">
                            @if(count($budgets) >0)
                            <table  class="table table-bordered table-responsive" id="search">
                                <thead>
                                    <tr>
                                        <th>Project</th>
                                        <th>Ministry</th>
                                        <th>Department</th>
                                        <th>Financial Year</th>
                                        <th>No. Of Phases</th>
                                        <th>Phases Complete</th>
                                        <th>Amount Used</th>
                                        <th>Supplementary Budget</th>
                                        <th>Amount Remaining</th>
                                        <th>Final Project Cost</th>
                                       
                                        
                                        
                                    </tr>
                                   
                                </thead>
                              
                                    {{-- <tbody> --}}
                                        <tr>
                                            <tbody>
                                                @foreach($budgets as $budget)
                                                {{-- <tbody> --}}
                                                    <tr>
                                               
                                                        <p class="hidden">
                                                            {{$phases=App\Phases::where('REF',$budget->REF)->get()}}
                                                            {{$completed=App\Phases::where('REF',$budget->REF)->where('status','complete')->get()}}
                                                            {{$phase_sum=App\Phases::where('REF',$budget->REF)->where('status','complete')->sum('phase_cost')}}
                                                            {{$supplemented=App\Projects\SupplementaryBudgetsModel::where('REF',$budget->REF)->sum('amount')}}
                                                            </p>
                                                        <td>{{$budget->name}}</td>
                                                        <td>{{$budget->ministry}}</td>
                                                        <td>{{$budget->department_code}}</td>
                                                        <td>{{$budget->financial_year}} </td>
                                                        @if(count($phases)>0)
                                                        <td>{{count($phases)+1}}</td>
                                                        @else
                                                        <td>1</td>
                                                        @endif
                                                        @if($completed)
                                                        <td>{{count($completed)+1}}</td>
                                                        @else
                                                        @if(!($budget->status=="initiated"))
                                                        <td>1</td>
                                                        @else
                                                        <td>1</td>
                                                        @endif
                                                        @endif
                                                        <td>{{$budget->amount_paid+$phase_sum}}</td>
                                                        @if($supplemented)
                                                        <td>{{$supplemented}}</td>
                                                        @else
                                                        <td>0</td>
                                                        @endif
                                                        <td>{{$budget->amount_remaining}}</td>
                                                        <td>{{$budget->total_budget+$phase_sum}}</td>
                                                       
                                                       
                                                        
                                                       
                                                                    
                                        
                                                    </tr>
                                                {{-- </tbody> --}}
                                            @endforeach
                                       
                                      
                                        @endif
                                                  
                                            </tbody>
                                        </tr>
                                       
                                       
                            </table>
                      
                           
                        </div>
                    </div>
                </div>
                {{-- <div class="col-md-2"></div> --}}
            </div>
        </div>
            {{ $budgets->links() }}
            <div>
                    <a href="javascript:history.back()" class="btn btn-success pull-right"><i class="fa fa-arrow-left">Back</i></a>
                </div>
        </section>
        
    </div>
    
    <script>
      function generatePDF() {
  // Choose the element that our invoice is rendered in.
  const element = document.getElementById("invoice");
var opt = {
  margin:       1,
  filename:     'budget.pdf',
  image:        { type: 'jpeg', quality: 0.98 },
  html2canvas:  { scale: 8},
  jsPDF:        { unit: 'in', format: 'letter', orientation: 'landscape' }
};


// New Promise-based usage:
html2pdf().set(opt).from(element).save();

// Old monolithic-style usage:
html2pdf(element, opt);
      }
      </script>
@endsection