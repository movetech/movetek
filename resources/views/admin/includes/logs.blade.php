@foreach (['danger', 'warning', 'success', 'info'] as $msg)
@if(Session::has('alert-'. $msg))
    <div class="alert  alert-success alert-warning alert-danger alert-info" role="alert">

        <strong> {{ Session::get('alert-' . $msg) }}</strong> 


        <button class="close" type="button" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
    </div>
@endif
@endforeach