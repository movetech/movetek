@extends('mainlayout')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class=" content-header text-center">
            <h4><i class="fa fa-asset bg-secondary"></i>
               Add New Asset
            </h4>
          </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
<div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="box box-primary">
                    <div class="box-body">
                       <div class="card">
                           <div class="card-body">
                            @include('messages.custom')
                               <form action="{{ route('assets.post') }}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                   
                                    <div class="form-group row{{ $errors->has('name')? 'has-error':''}}">
                                        <label for="name" class="col-md-4 form-label text-md-right">Asset Name</label>
                                        <div class="col-md-8">
                                            <input type="text" name="name" class="form-control">
                                            {{-- <span class="alert alert-danger">{{ $errors->first('name') }}</span> --}}
                                        </div>
                                    </div>
                                    <div class="form-group row{{ $errors->has('category_id')? 'has-error':''}}">
                                            <label for="category_id" class="col-md-4 form-label text-md-right">Category</label>
                                            <div class="col-md-8">
                                                <select name="category_id" id="category_id" class="col-md-10 form-control">
                                                    @if(isset($categories))
                                                        @foreach($categories as $category)
                                                            <option class="form-control" value="{{ $category->id }}">{{ $category->name }}</option>
                                                        @endforeach

                                                    @endif
                                                </select>
                                                <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#exampleModalCenter">
                                                        <i class="fa fa-plus"></i>
                                                </button>
                                                {{-- <span class="alert alert-danger">{{ $errors->first('name') }}</span> --}}
                                            </div>
                                        </div>
                                    <div class="form-group row {{ $errors->has('available_balance') ? 'has-error':''}}">
                                        <label for="available_balance" class="col-md-4 form-labe text-md-right">Available Balance</label>
                                        <div class="col-md-8">
                                            <input type="number" name="available_balance" class="form-control">
                                             {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                        </div>
                                    </div>
                                    <div class="form-group row {{ $errors->has('given_out') ? 'has-error':''}}">
                                            <label for="given_out" class="col-md-4 form-labe text-md-right">Given Out</label>
                                            <div class="col-md-8">
                                                <input type="number" name="given_out" class="form-control">
                                                 {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                            </div>
                                    </div>
                                    <div class="form-group row {{ $errors->has('total') ? 'has-error':''}}">
                                            <label for="total" class="col-md-4 form-labe text-md-right">Total</label>
                                            <div class="col-md-8">
                                                <input type="text" name="total" class="form-control">
                                                 {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                            </div>
                                        </div>
                                    <div class="form-group row">
                                        <div class="col-md-8 col-md-offset-4">
                                            <button type="submit" class="btn btn-success">
                                                <i class="fa fa-arrow-right"></i>
                                                Submit
                                            </button>
                                            <a href="{{ route('assets.list') }}" class="btn btn-info" style="float:right">
                                                <i class="fa fa-arrow-left"></i> Back
                                            </a>
                                        </div>
                                    </div>
                              </form>
                           </div>
                       </div>
                    </div>


                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
      <!-- Modal -->
      <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Add New Category</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                    @include('messages.custom')
                    <form action="{{ route('categories.post') }}" method="post" enctype="multipart/form-data" id="btnsubmit">
                         {{ csrf_field() }}
                        
                         <div class="form-group row{{ $errors->has('name')? 'has-error':''}}">
                             <label for="name" class="col-md-4 form-label text-md-right">Name</label>
                             <div class="col-md-8">
                                 <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                                 {{-- <span class="alert alert-danger">{{ $errors->first('name') }}</span> --}}
                             </div>
                         </div>
                         <div class="form-group row {{ $errors->has('type') ? 'has-error':''}}">
                             <label for="type" class="col-md-4 form-labe text-md-right">Type</label>
                             <div class="col-md-8">
                                 @if(count($types) >0)

                                        <select name="type" id="type" class="form-control">
                                            <option value="">Select Category type</option>
                                            @foreach($types as $key=>$value)
                                                <option value="{{ $value->type }}">{{ $value->type }}</option>
                                            @endforeach
                                        </select>
                                 @endif
                             </div>
                         </div>
                         <div class="form-group row">
                             <div class="col-md-8 col-md-offset-4">
                                 <button type="submit" class="btn btn-primary" id="btnsubmitjj">
                                     <i class="fa fa-arrow-right"></i>
                                     Submit
                                 </button>
                             </div>
                         </div>
                   </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
    </section>



</div>
<script>
    $.ajaxSetup(){

    }
    $("#btnsubmit").click(function(event) {
    event.preventDefault();

    $.ajax({
        type: "post",
        url: "{{ url('categories.post') }}",
        contentType: "application/json",
        data: $('#btnsubmit').serialize(),
        success: function(data){
              alert("Data Save: " + data);
        },
        error: function(data){
             alert("Error")
        }
    });
});
</script>

@endsection