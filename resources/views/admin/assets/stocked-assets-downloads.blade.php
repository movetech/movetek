<style>
        table {
          width: 100%;
          border: 1px solid #fff;
          text-align: left;
        }
        
        th, td {
        padding: 15px;
        text-align: left;
        }
        
        tr:nth-child(even){background-color: #f2f2f2}
        
        th {
          background-color: #4CAF50;
          color: white;
        }
        #main-body-div{
            border: 2px solid rgba(47,73,100,.9);
            border-radius:2px;
            padding:20px;
        }
        </style>
    <div id="main-body-div">
       <div class="content-wrapper ">
        <div style="width:100%; height:100px; text-align:center" class="text-center">
            <img src="{{ public_path('/storage/images/logos/mandera-county.jpg')}}" alt="" style="height:50px;align:center;border-radius:50%" style="img-thumbnail">
            <h3 style="font-style:Sans Serif; font-weight:bold; color:brown">MANDERA COUNTY GOVERNMENT</h3>
            <h5 style="center">Summary of All Stocked Assets</h5>
        </div>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-default">
                        <div class="box-header">
                          
                        </div>
                        <div class="box-body">
                            @include('messages.custom')
                            @if(count($assets) >0)
                            <table class="table table-bordered table-responsive">
                                <thead>
                                    <tr>
                                        <th>SR/NO:</th>
                                        <th>Category</th>
                                        <th>Available Balance</th>
                                        <th>Given Out</th>
                                        <th>Total</th>
                                        
                                    </tr>
                                </thead>
                                @foreach($assets as $s)
                                        <tr>
                                            <td>{{ $s->id }}</td>
                                            <td>{{ $s->category->name }}</td>
                                            <td>{{ $s->available_balance}} </td>
                                            <td>{{ $s->given_out }}</td>
                                            <td>{{ $s->total }}</td>
                                           
                                           
                                          
                                        </tr>
                                @endforeach
                            </table>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script>
 
