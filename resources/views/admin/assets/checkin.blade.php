@extends('mainlayout')

@section('content')
<link rel="stylesheet" href="{{asset('//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css')}}">
<script src="{{asset('https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js')}}"></script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class=" content-header text-center">
        <h4><i class="fa fa-asset bg-secondary"></i>
          Asset Issuance(Checkin)
        </h4>
      </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
<div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="box box-primary">
                    <div class="box-body">
                       <div class="card">
                           <div class="card-body">
                                @include('messages.custom')
                               <form action="{{ route('assets.Issue') }}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="form-group row{{ $errors->has('name')? 'has-error':''}}">
                                        <label for="category_id" class="col-md-4 form-label text-md-right">Name</label>
                                        <div class="col-md-8">
                                            <select name="name" id="name" class="form-control">
                                                @if(isset($users))
                                                    @foreach($users as $user)
                                                        <option class="form-control" value="{{ $user->id}}">{{ $user->name }}</option>
                                                    @endforeach

                                                @endif
                                            </select>
                                            {{-- <span class="alert alert-danger">{{ $errors->first('name') }}</span> --}}
                                        </div>
                                    </div>
                                    <div class="form-group row{{ $errors->has('category_id')? 'has-error':''}}">
                                            <label for="category_id" class="col-md-4 form-label text-md-right">Category</label>
                                            <div class="col-md-8">
                                                <select name="category" id="category_id" class="form-control">
                                                    @if(isset($categories))
                                                        @foreach($categories as $category)
                                                            <option class="form-control" value="{{ $category->id }}">{{ $category->name }}</option>
                                                        @endforeach

                                                    @endif
                                                </select>
                                                {{-- <span class="alert alert-danger">{{ $errors->first('name') }}</span> --}}
                                            </div>
                                        </div>
                                        <div class="form-group row{{ $errors->has('asset_id')? 'has-error':''}}">
                                            <label for="asset_id" class="col-md-4 form-label text-md-right">Asset Name</label>
                                            <div class="col-md-8">
                                                <select name="asset_name" id="asset_id" class="form-control">
                                                    @if(isset($assets))
                                                        @foreach($assets as $asset)
                                                            <option class="form-control" value="{{ $asset->id}}">{{ $asset->name}}</option>
                                                        @endforeach

                                                    @endif
                                                </select>
                                                {{-- <span class="alert alert-danger">{{ $errors->first('name') }}</span> --}}
                                            </div>
                                        </div>
                                        <div class="form-group row{{ $errors->has('location_id')? 'has-error':''}}">
                                            <label for="location_id" class="col-md-4 form-label text-md-right">Sub County</label>
                                            <div class="col-md-8">
                                                <select name="location" id="location_id" class="form-control">
                                                    <option value="">Select...</option>
                                                    @if(isset($locations))
                                                        @foreach($locations as $location)
                                                            <option class="form-control" value="{{ $location->name }}">{{ $location->name }}</option>
                                                        @endforeach

                                                    @endif
                                                </select>
                                                {{-- <span class="alert alert-danger">{{ $errors->first('name') }}</span> --}}
                                            </div>
                                        </div>
                                        <div class="form-group row {{ $errors->has('total') ? 'has-error':''}}">
                                            <label for="total" class="col-md-4 form-labe text-md-right">Model Number</label>
                                            <div class="col-md-8">
                                                <input type="text" id="modelnumber" name="modelnumber" class="form-control">
                                                 {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                            </div>
                                        </div>
                                        <div class="form-group row {{ $errors->has('total') ? 'has-error':''}}">
                                            <label for="total" class="col-md-4 form-labe text-md-right">Asset Tag</label>
                                            <div class="col-md-8">
                                                <input type="text" id="tag" name="tag" class="form-control">
                                                 {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                            </div>
                                        </div>
                                    <div class="form-group row {{ $errors->has('total') ? 'has-error':''}}">
                                            <label for="total" class="col-md-4 form-labe text-md-right">Serial Number</label>
                                            <div class="col-md-8">
                                                <input type="text" name="serialnumber" class="form-control">
                                                 {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                            </div>
                                        </div>
                                       
                                    <div class="form-group row">
                                        <div class="col-md-8 col-md-offset-4">
                                            <button type="submit" class="btn btn-sm btn-success">
                                                <i class="fa fa-arrow-right"></i>
                                                Issue
                                            </button>
                                            <a href="{{ route('assets.show.issued') }}" class="btn btn-sm btn-success pull-right"><i class="fa fa-eye"></i> View Issued</a>
                                            {{-- <button type="submit" class="btn btn-sm btn-success pull-right">
                                                <i class="fa fa-eye"></i>
                                                View Issued
                                            </button> --}}
                                        <br/> <br/>
                                            <a href="{{ route('assets.list') }}" class="btn btn-sm btn-info" style="float:right">
                                                <i class="fa fa-arrow-left"></i> Back
                                            </a>
                                        </div>
                                    </div>
                              </form>
                           </div>
                       </div>
                    </div>
                   


                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
        


    </section>



</div>
<script type="text/javascript">
     $(document).ready(function(){
        $('#tag').on('keyup', function(){
            var test=$('#tag').val();
           console.log(test);
        });
     });
</script>
@endsection