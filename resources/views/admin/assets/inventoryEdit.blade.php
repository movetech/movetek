@extends('mainlayout')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class=" content-header text-center">
        <h4><i class="fa fa-asset bg-secondary"></i>
       Edit Inventory Data
        </h4>
      </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
<div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="box box-primary">
                    <div class="box-body">
                       <div class="card">
                           <div class="card-body">
                            @include('messages.custom')
                               <form action="{{ route('inventory.update', $inventory->id) }}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                   
                                    <div class="form-group row{{ $errors->has('name')? 'has-error':''}}">
                                        <label for="name" class="col-md-4 form-label text-md-right">Manufacturer</label>
                                        <div class="col-md-8">
                                            <select name="manufacturer" id="manufacturer" class="form-control">
                                                <option value="{{ $inventory->manufacturer }}">{{ $inventory->manufacturer }}</option>
                                                @if(count( $manufacturers ) > 0)
                                                    @foreach($manufacturers as $key=>$value)
                                                        <option value="{{ $value->name }}">{{ $value->name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>

                                            {{-- <span class="alert alert-danger">{{ $errors->first('name') }}</span> --}}
                                        </div>
                                    </div>
                                    <div class="form-group row{{ $errors->has('category_id')? 'has-error':''}}">
                                            <label for="category_id" class="col-md-4 form-label text-md-right">Supplier</label>
                                            <div class="col-md-8">
                                                <select name="supplier" id="supplier" class="form-control"  required>
                                                    {{ $inventory->supplier }}
                                                    @if(isset($suppliers))
                                                        @foreach($suppliers as $supplier)
                                                            <option class="form-control" value="{{ $supplier->name }}">{{$supplier->name }}</option>
                                                        @endforeach

                                                    @endif
                                                </select>
                                                {{-- <span class="alert alert-danger">{{ $errors->first('name') }}</span> --}}
                                            </div>
                                        </div>
                                    <div class="form-group row {{ $errors->has('available_balance') ? 'has-error':''}}">
                                        <label for="available_balance" class="col-md-4 form-labe text-md-right">Asset</label>
                                        <div class="col-md-8">
                                            <select name="asset" id="asset" class="form-control"  required>
                                               <option value="{{ $inventory->asset_name }}">{{ $inventory->asset_name }}</option> 
                                                @if(isset($assets))
                                                    @foreach($assets as $asset)
                                                        <option class="form-control" value="{{$asset->name}}">{{$asset->name}}</option>
                                                    @endforeach

                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row {{ $errors->has('available_balance') ? 'has-error':''}}">
                                        <label for="tag" class="col-md-4 form-labe text-md-right">Asset Tag</label>
                                        <div class="col-md-8">
                                            <input type="text" name="tag" class="form-control"  required value="{{ $inventory->tag }}">
                                             {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="category" class="col-md-4 form-label text-md-right">Category</label>
                                        <div class="col-md-8">
                                            <select name="category" id="category" class="form-control">
                                                <option value="{{ $inventory->category->category_id }}">{{ $inventory->category->name }}</option>
                                                @if(count($categories) > 0)
                                                    @foreach($categories as $key=>$value )
                                                        <option value="{{ $value->name }}">{{ $value->name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row {{ $errors->has('available_balance') ? 'has-error':''}}">
                                        <label for="available_balance" class="col-md-4 form-labe text-md-right">Cost</label>
                                        <div class="col-md-8">
                                            <input type="text" name="cost" class="form-control"  required value="{{ $inventory->cost }}">
                                             {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                        </div>
                                    </div>
                                    <div class="form-group row {{ $errors->has('available_balance') ? 'has-error':''}}">
                                            <label for="model_number" class="col-md-4 form-label text-md-right">Model Number</label>
                                            <div class="col-md-8">
                                                <input type="text" name="model_number" class="form-control"  required value="{{ $inventory->modelnumber }}">
                                                 {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                            </div>
                                        </div>
                                        <div class="form-group row {{ $errors->has('serial_number') ? 'has-error':''}}">
                                            <label for="serial_number" class="col-md-4 form-labe text-md-right">Serial Number</label>
                                            <div class="col-md-8">
                                                <input type="text" name="serial_number" class="form-control"  required value="{{ $inventory->serialnumber }}">
                                                 {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                            </div>
                                        </div>
                                    <div class="form-group row {{ $errors->has('given_out') ? 'has-error':''}}">
                                            <label for="given_out" class="col-md-4 form-labe text-md-right">Purchase Date</label>
                                            <div class="col-md-8">
                                                <input type="date" name="purchase_date" class="form-control"  required value="{{ $inventory->purchase_date }}">
                                                 {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                            </div>
                                    </div>
                                    <div class="form-group row {{ $errors->has('total') ? 'has-error':''}}">
                                            <label for="total" class="col-md-4 form-labe text-md-right">Warranty</label>
                                            <div class="col-md-8">
                                                <input type="text" name="warrant" class="form-control" value="{{ $inventory->warrant }}">
                                                 {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                            </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="financial_year" class="col-md-4 form-label text-md-right">Financial Year</label>
                                        <div class="col-md-8">
                                            <select name="financial_year" id="financial_year" class="form-control">
                                                <option value="">Year Format example 2019/20</option>
                                            </select>
                                        </div>
                                    </div>
                                        <div class="form-group row {{ $errors->has('total') ? 'has-error':''}}">
                                            <label for="total" class="col-md-4 form-labe text-md-right" >Delivery Date</label>
                                            <div class="col-md-8">
                                                <input type="date" name="delivery_date" class="form-control"  required value="{{ $inventory->delivery_date }}">
                                                 {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                            </div>
                                        </div>
                                    <div class="form-group row">
                                        <div class="col-md-8 col-md-offset-4">
                                            {{-- <button type="submit" class="btn btn-success">
                                                <i class="fa fa-arrow-right"></i>
                                                Submit
                                            </button> --}}
                                            <form action="{{ route('inventory.update', $inventory->id) }}" method="post" enctype="multipart/form-data">
                                                <input type="hidden" name="_method" value="PUT"/>
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                                <button type="submit" class="btn btn-success">
                                                    <i class="fa fa-arrow-right"></i> Update
                                                </button>
                                            </form>
                                            <a href="{{ route('inventory.view') }}" class="btn btn-info pull-right"><i class="fa fa-arrow-left"></i>  Back</a>
                                            </a>
                                        </div>
                                    </div>
                              </form>
                           </div>
                       </div>
                    </div>


                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
        


    </section>



</div>

@endsection