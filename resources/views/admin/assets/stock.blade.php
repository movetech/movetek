@extends('mainlayout')
@section('content')
<link rel="stylesheet" href="{{asset('//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css')}}">
<script src="{{asset('https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js')}}"></script> 
    <div class="content-wrapper">
            <section class=" content-header text-center">
                    <h4><i class="fa fa-asset bg-secondary"></i>
                   All Assets
                    </h4>
                  </section>
                  @include('messages.custom')
        <section class="content">
                <a href="{{ route('download-all-assets') }}" class="btn btn-sm btn-danger puul-right"><i class="fa fa-arrow-down"></i> Export to pdf</a>

            <div class="row">
                {{-- <div class="col-md-2"></div> --}}
                <div class="col-md-12">
                    <div class="box box-default">
                               {{-- <a style="float:right" href="{{route('assets.assign')}}" class="btn btn-info btn-sm">New</a> --}}
                        
                        <!-- Search form -->
  <div class="md-form mt-0">
        <input class="form-control" id="search" type="text" placeholder="Search&hellip;" aria-label="Search">
      </div>
                        <div class="box-body">
                            @if(count($stocks) >0)
                            <div class="table-responsive">
                            <table class="table table-bordered table-responsive" id="search">
                                <thead>
                                    <tr>
                                     
                                        <th>Manufacturer</th>
                                        <th>Supplier</th>
                                        <th>Cost</th>
                                        <th>Asset Tag</th>
                                        <th>Asset</th>
                                        <th>Assset Type</th>
                                        <th>Category</th>
                                        <th>serialnumber</th>
                                        <th>modelnumber</th>
                                        <th>Purchase Date</th>
                                        <th>Warrant(Months)</th>
                                        <th>Status</th>
                                        <th>State</th>
                                        <th>Lifespan(Months)</th>
                                        <th>Delivered Date</th>
                                        <th>Financial Year</th>
                                        <th>View</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                        
                                    </tr>
                                </thead>
                                @foreach($stocks as $stock)
                                    {{-- <tbody> --}}
                                        <tr>
                                          
                                           
                                            <td>{{ $stock->manufacturer}}</td>
                                            <td>{{ $stock->supplier}}</td>
                                            <td>{{ $stock->cost}} </td>
                                            <td>{{ $stock->tag}}</td>
                                            <td>{{ $stock->asset_name}}</td>
                                            <td>{{ $stock->asset_type}}</td>
                                            <td>{{ $stock->category->name}}</td>
                                            <td>{{ $stock->serialnumber}}</td>
                                            <td>{{ $stock->modelnumber}}</td>
                                            <td>{{ $stock->purchase_date}}</td>
                                            <td>{{ $stock->warrant }}</td>
                                            <td>{{ $stock->status}}</td>
                                            <td>{{ $stock->state}}</td>
                                            <td>{{ $stock->lifespan}}</td>
                                            <td>{{ $stock->delivery_date}}</td>
                                            <td>{{ $stock->financial_year}}</td>
                                           
                                                    <td  class="pt-2-half">
                                                            <a href="{{ route('inventory.detail', $stock->id) }}" style="float:left;" data-placement="top" data-toggle="tooltip" title="view"><button class="btn btn-info btn-xs pull-right " data-title="view" data-toggle="modal" data-target="#view" ><span class="fa fa-eye"></span></button></a>
                                                            </td>
                                                            <td  class="pt-2-half">
                                                                    <a href="{{ route('inventory.edit', $stock->id) }}" style="float:left;" data-placement="top" data-toggle="tooltip" title="view"><button class="btn btn-info btn-xs pull-right " data-title="view" data-toggle="modal" data-target="#view" ><span class="fa fa-edit"></span></button></a>
                                                                    </td>
                                                                    <td  class="pt-2-half">
                                                                        <form action="{{ route('inventory.delete', $stock->id) }}" method="post">
                                                                            <input type="hidden" name="_method" value="DELETE"/>
                                                                            <input type="hidden" name="_token" value="{{  csrf_token() }}"/>
                                                                            <p style="float:left;" data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-danger btn-xs pull-right" data-title="Delete" data-toggle="modal" data-target="#delete" onclick="if (!confirm('Are you sure you want to proceed?')) { return false }"><span class="glyphicon glyphicon-trash"></span></button></p>
                                                                        </form>
                                                                    </td>
                                 
                                            
                                        </tr>
                                    {{-- </tbody> --}}
                                @endforeach
                            </table>
                            </div>
                            {{ $stocks->links() }}
                            @endif
                        </div>
                    </div>
                </div>
                {{-- <div class="col-md-2"></div> --}}
            </div>
        </section>
    </div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#search').on('keyup',function(){
        $value=$(this).val();
        //$select=$('#inputtype').val();
        var token = $("meta[name='csrf-token']").attr("content");
        $.ajax({
        type : 'get',
        url : '{{URL::to('admin/assets/searchall')}}',
         
        data:{'search':$value,  "_token": token,},
         
        success:function(data){
         
        $('tbody').html(data);
                          }
         
        });
         
       }); 
       // 
        
    });
</script>
@endsection