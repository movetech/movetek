@extends('mainlayout')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class=" content-header text-center">
        <h4><i class="fa fa-asset bg-secondary"></i>
       Add Asset Stock
        </h4>
      </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
<div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="box box-primary">
                    <div class="box-body">
                       <div class="card">
                           <div class="card-body">
                            @include('messages.custom')
                               <form action="{{ route('inventory.post') }}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                   
                                    <div class="form-group row{{ $errors->has('name')? 'has-error':''}}">
                                        <label for="manufacturer" class="col-md-4 form-label text-md-right">Manufacturer
                                         <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#exampleModalCenter" style="position:relative">
                                                 New
                                        </button>
                                    </label>
                                        <div class="col-md-8">
                                                <input list="manufacturer" name="manufacturer" placeholder="manufacturer name" class="form-control" required>
                                            <datalist name="manufacturer" id="manufacturer">
                                                @if(count($manufacturers) > 0)
                                                    @foreach($manufacturers as $key=>$value)
                                                            <option value="{{ $value->name }}">{{ $value->name }}</option>
                                                    @endforeach
                                                @endif
                                            </datalist>
                                            
                                            {{-- <span class="alert alert-danger">{{ $errors->first('name') }}</span> --}}
                                        </div>
                                    </div>
                                    <div class="form-group row{{ $errors->has('category_id')? 'has-error':''}}">
                                            <label for="category_id" class="col-md-4 form-label text-md-right">Suppliers 
                                                <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#exampleModalCenter2">
                                                     New
                                                </button>
                                        </label>
                                            <div class="col-md-8">
                                                    <input list="supplier" name="supplier" placeholder="supplier name" class="form-control" required>
                                                <datalist name="supplier" id="supplier">
                                                   
                                                    <option value="">Select Supplier...</option>
                                                    @if(isset($suppliers))
                                                        @foreach($suppliers as $supplier)
                                                            <option class="form-control" value="{{ $supplier->name }}">{{$supplier->name }}</option>
                                                        @endforeach

                                                    @endif
                                                    </datalist>
                                                
                                                {{-- <span class="alert alert-danger">{{ $errors->first('name') }}</span> --}}
                                            </div>
                                        </div>
                                         <div class="form-group row">
                                            <label for="category" class="col-md-4 form-label text-md-right">Category
                                                    <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#exampleModalCenter4">
                                                            New
                                                    </button> 
                                            </label>
                                            <div class="col-md-8">
                                                    <input list="category" name="category" placeholder="category name" class="form-control"> 
                                                <datalist name="category" id="category">
                                                    <option value="">Select Asset category</option>
                                                    @if(count($categories) > 0)
                                                            @foreach($categories as $category)
                                                                    <option  value="{{$category->id}}">{{ $category->name}}</option>
                                                            @endforeach

                                                    @endif
                                                </datalist>
                                            </div>
                                        </div>
                                    <div class="form-group row {{ $errors->has('available_balance') ? 'has-error':''}}">
                                        <label for="available_balance" class="col-md-4 form-labe text-md-right">Asset Name
                                                <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#exampleModalCenter3" required>
                                                        New
                                                </button>    
                                        </label>
                                        <div class="col-md-8">
                                                <input list="asset" name="asset" placeholder="asset name" class="form-control">
                                            <datalist name="asset" id="asset">
                                                
                                                <option value="">Select Asset...</option>
                                                @if(isset($assets))
                                                    @foreach($assets as $asset)
                                                        <option class="form-control" value="{{$asset->name}}">{{$asset->name}}</option>
                                                    @endforeach

                                                @endif
                                            </datalist>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="asset_type" class="col-md-4 form-label text-md-right">Asset Type
                                        <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#assets_type">
                                            New
                                    </button>
                                </label>
                                        <div class="col-md-8">
                                            <input list="asset_type" name="asset_type" placeholder="asset type" class="form-control">
                                            <datalist name="asset_type" id="asset_type">
                                               
                                                <option value="">Asset Type</option>
                                                @if(isset($types))
                                                    @foreach($types as $type)
                                                        <option class="form-control" value="{{$type->type}}">{{$type->type}}</option>
                                                    @endforeach

                                                @endif
                                            </datalist>
                                        </div>
                                    </div>
                                    <div class="form-group row {{ $errors->has('available_balance') ? 'has-error':''}}">
                                            <label for="available_balance" class="col-md-4 form-labe text-md-right">Status   
                                            </label>
                                            <div class="col-md-8">
                                                    <input list="status" name="status" placeholder="status" class="form-control" required>
                                                <datalist name="status" id="status">
                                                    {{ old('asset') }}
                                                    <option value="">Asset Status...</option>
                                                    @if(isset($statuses))
                                                        @foreach($statuses as $status)
                                                            <option class="form-control" value="{{$status->status}}">{{$status->status}}</option>
                                                        @endforeach
    
                                                    @endif
                                                </datalist>
                                            </div>
                                        </div>
                                    
                                    <div class="form-group row {{ $errors->has('available_balance') ? 'has-error':''}}">
                                        <label for="available_balance" class="col-md-4 form-labe text-md-right">Asset Tag</label>
                                        <div class="col-md-8">
                                            <input type="text" name="tag" class="form-control">
                                             {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                        </div>
                                    </div>
                                    <div class="form-group row {{ $errors->has('available_balance') ? 'has-error':''}}">
                                        <label for="model_number" class="col-md-4 form-label text-md-right">Model Number</label>
                                        <div class="col-md-8">
                                            <input type="text" name="model_number" class="form-control">
                                             {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                        </div>
                                    </div>
                                    <div class="form-group row {{ $errors->has('serial_number') ? 'has-error':''}}">
                                        <label for="serial_number" class="col-md-4 form-labe text-md-right">Serial Number</label>
                                        <div class="col-md-8">
                                            <input type="text" name="serial_number" class="form-control">
                                             {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                        </div>
                                    </div> 
                                    <div class="form-group row {{ $errors->has('available_balance') ? 'has-error':''}}">
                                        <label for="available_balance" class="col-md-4 form-labe text-md-right">Cost</label>
                                        <div class="col-md-8">
                                            <input type="text" name="cost" class="form-control"  required >
                                             {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                        </div>
                                    </div>
                                    <div class="form-group row {{ $errors->has('given_out') ? 'has-error':''}}">
                                            <label for="given_out" class="col-md-4 form-labe text-md-right">Purchase Date</label>
                                            <div class="col-md-8">
                                                <input type="date" name="purchase_date" class="form-control"  required >
                                                 {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                            </div>
                                    </div>
                                    <div class="form-group row {{ $errors->has('total') ? 'has-error':''}}">
                                            <label for="total" class="col-md-4 form-labe text-md-right">Warranty(Months)</label>
                                            <div class="col-md-8">
                                                <input type="number" name="warrant" class="form-control" max="24" min="2" value="{{ old('warrant') }}">
                                                 {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                            </div>
                                    </div>
                                    <div class="form-group row {{ $errors->has('available_balance') ? 'has-error':''}}">
                                            <label for="lifespan" class="col-md-4 form-labe text-md-right">Lifespan</label>
                                            <div class="col-md-8">
                                                <input type="text" name="lifespan" class="form-control" placeholder="Months">
                                                 {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                            </div>
                                        </div>
                                    <div class="form-group row">
                                        <label for="financial_year" class="col-md-4 form-label text-md-right">Financial Year</label>
                                        <div class="col-md-8">
                                            <select name="financial_year" id="financial_year" class="form-control">
                               
                                                                <option value="{{ $years->year}}">{{ $years->year}}</option>
                
                                                </select>
                                        </div>
                                    </div>
                                       
                                        <div class="form-group row {{ $errors->has('total') ? 'has-error':''}}">
                                            <label for="total" class="col-md-4 form-labe text-md-right" >Delivery Date</label>
                                            <div class="col-md-8">
                                                <input type="date" name="delivery_date" class="form-control"  required value="{{ old('delivery_date') }}">
                                                 {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                            </div>
                                        </div>
                                    <div class="form-group row">
                                        <div class="col-md-8 col-md-offset-4">
                                            <button type="submit" class="btn btn-success">
                                                <i class="fa fa-arrow-right"></i>
                                                Submit
                                            </button>
                                            <a href="javascript:history.back()" class="btn btn-info pull-right"><i class="fa fa-arrow-left"></i>  Back</a>
                                            </a>
                                        </div>
                                    </div>
                              </form>
                           </div>
                       </div>
                    </div>


                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
        

 <!--Manufacturer Modal -->
 <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header bg-primary text-white text-center text-uppercase">
              <h5 class="modal-title" id="exampleModalLongTitle">Add New Manufacturer</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                    @include('messages.custom')
                    <form action="{{ route('manufacturers.post') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                       
                        <div class="form-group row {{ $errors->has('name')? 'has-error':''}}">
                            <label for="name" class="col-md-4 form-label text-md-right">Name</label>
                            <div class="col-md-8">
                                <input type="text" name="name" class="form-control">
                                
                                {{-- <span class="alert alert-danger">{{ $errors->first('name') }}</span> --}}
                            </div>
                        </div>
                        <div class="form-group row {{ $errors->has('url') ? 'has-error':''}}">
                            <label for="url" class="col-md-4 form-label text-md-right">URL</label>
                            <div class="col-md-8">
                                <input type="text" name="url" class="form-control">
                                 {{-- <span class="alert alert-danger">{{ $errors->first('url') }}</span> --}}
                            </div>
                        </div>
                        <div class="form-group row {{ $errors->has('phone') ? 'has-error':''}}">
                                <label for="phone" class="col-md-4 form-label text-md-right">Phone</label>
                                <div class="col-md-8">
                                    <input type="text" name="phone" class="form-control">
                                     {{-- <span class="alert alert-danger">{{ $errors->first('phone') }}</span> --}}
                                </div>
                        </div>
                        <div class="form-group row {{ $errors->has('email') ? 'has-error':''}}">
                                <label for="email" class="col-md-4 form-label text-md-right">Email</label>
                                <div class="col-md-8">
                                    <input type="email" name="email" class="form-control"/>
                                     {{-- <span class="alert alert-danger">{{ $errors->first('email') }}</span> --}}
                                </div>
                        </div>
                        <div class="form-group row">
                                <label for="image" class="col-md-4 form-label text-md-right">Upload Image</label>
                                <div class="col-md-8">
                                    <input type="file" name="image">
                                </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-arrow-right"></i>
                                    Submit
                                </button>
                            </div>
                        </div>
                  </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
      <!-- End Manufacturer Modal -->
      
 <!--Supplier Modal -->
 <div class="modal fade" id="exampleModalCenter2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header bg-primary text-white text-center text-uppercase">
              <h5 class="modal-title" id="exampleModalLongTitle">Add New Supplier</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                    @include('messages.custom')
                    <form action="{{ route('suppliers.post') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                       
                        <div class="form-group row {{ $errors->has('name')? 'has-error':''}}">
                            <label for="name" class="col-md-4 form-label text-md-right">Name</label>
                            <div class="col-md-8">
                                <input type="text" name="name" class="form-control">
                                {{-- <span class="alert alert-danger">{{ $errors->first('name') }}</span> --}}
                            </div>
                        </div>
                        <div class="form-group row {{ $errors->has('contact_name')? 'has-error':''}}">
                            <label for="contact_name" class="col-md-4 form-label text-md-right">Contact Name</label>
                            <div class="col-md-8">
                                <input type="text" name="contact_name" class="form-control">
                                {{-- <span class="alert alert-danger">{{ $errors->first('name') }}</span> --}}
                            </div>
                        </div>
                        <div class="form-group row {{ $errors->has('phone') ? 'has-error':''}}">
                            <label for="phone" class="col-md-4 form-label text-md-right">Phone</label>
                            <div class="col-md-8">
                                <input type="text" name="phone" class="form-control">
                                 {{-- <span class="alert alert-danger">{{ $errors->first('phone') }}</span> --}}
                            </div>
                    </div>
                    <div class="form-group row {{ $errors->has('email') ? 'has-error':''}}">
                        <label for="email" class="col-md-4 form-label text-md-right">Email</label>
                        <div class="col-md-8">
                            <input type="email" name="email" class="form-control"/>
                             {{-- <span class="alert alert-danger">{{ $errors->first('email') }}</span> --}}
                        </div>
                    </div>
                        <div class="form-group row {{ $errors->has('url') ? 'has-error':''}}">
                            <label for="url" class="col-md-4 form-label text-md-right">URL</label>
                            <div class="col-md-8">
                                <input type="text" name="url" class="form-control">
                                 {{-- <span class="alert alert-danger">{{ $errors->first('url') }}</span> --}}
                            </div>
                        </div>
                       
                        <div class="form-group row {{ $errors->has('notes') ? 'has-error':''}}">
                            <label for="notes" class="col-md-4 form-label text-md-right">Notes</label>
                            <div class="col-md-8">
                               <textarea name="notes" id="notes" cols="30" rows="10" class="form-control">

                               </textarea>
                                 {{-- <span class="alert alert-danger">{{ $errors->first('email') }}</span> --}}
                            </div>
                    </div>
                        <div class="form-group row">
                                <label for="image" class="col-md-4 form-label text-md-right">Upload Image</label>
                                <div class="col-md-8">
                                    <input type="file" name="image">
                                </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-arrow-right"></i>
                                    Submit
                                </button>
                                {{-- <a href="{{ route('suppliers.index') }}" class="btn btn-danger" style="float:right"><i class="fa fa-window-close"></i> Cancel</a> --}}
                            </div>
                        </div>
                  </form>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
      <!-- End Modal Supplier Modal -->
<!-- Asset modal -->

<div class="modal fade" id="exampleModalCenter3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header bg-primary text-white text-center text-uppercase">
              <h5 class="modal-title" id="exampleModalLongTitle">Add New Asset</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                    @include('messages.custom')
                    <form action="{{ route('assets.post') }}" method="post" enctype="multipart/form-data">
                         {{ csrf_field() }}
                        
                         <div class="form-group row{{ $errors->has('name')? 'has-error':''}}">
                             <label for="name" class="col-md-4 form-label text-md-right">Asset Name</label>
                             <div class="col-md-8">
                                 <input type="text" name="name" class="form-control">
                                 {{-- <span class="alert alert-danger">{{ $errors->first('name') }}</span> --}}
                             </div>
                         </div>
                         <div class="form-group row{{ $errors->has('category_id')? 'has-error':''}}">
                                 <label for="category_id" class="col-md-4 form-label text-md-right">Category
                                   {{--  <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#exampleModalCenter4">
                                        New
                                   </button>  --}}
                                 </label>
                                 <div class="col-md-8">
                                     <select name="category_id" id="category_id" class="col-md-10 form-control">
                                      <div class="hidden"> {{ $categories = App\Settings\Categories::all() }}</div>  
                                      <option value="">Select Category...</option>
                                         @foreach($categories as $key=>$value)
                                                     <option value="{{ $value->id }}">{{ $value->name }}</option>
                                         @endforeach
                                     </select>
                                     
                                     {{-- <span class="alert alert-danger">{{ $errors->first('name') }}</span> --}}
                                 </div>
                             </div>
                         
                         <div class="form-group row">
                             <div class="col-md-8 col-md-offset-4">
                                 <button type="submit" class="btn btn-success">
                                     <i class="fa fa-arrow-right"></i>
                                     Submit
                                 </button>
                                 
                             </div>
                         </div>
                   </form>
                
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
      <!-- End Asset Modal -->
      <!-- Asset type modal -->

<div class="modal fade" id="assets_type" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header bg-primary text-white text-center text-uppercase">
          <h5 class="modal-title" id="exampleModalLongTitle">Add New Asset Type</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
                @include('messages.custom')
                <form action="{{ route('new-asset-type') }}" method="post" enctype="multipart/form-data">
                     {{ csrf_field() }}
                    
                     <div class="form-group row{{ $errors->has('name')? 'has-error':''}}">
                         <label for="name" class="col-md-4 form-label text-md-right">Type</label>
                         <div class="col-md-8">
                             <input type="text" name="asset_type" class="form-control">
                             {{-- <span class="alert alert-danger">{{ $errors->first('name') }}</span> --}}
                         </div>
                     </div>
                     
                     <div class="form-group row">
                         <div class="col-md-8 col-md-offset-4">
                             <button type="submit" class="btn btn-success">
                                 <i class="fa fa-arrow-right"></i>
                                 Submit
                             </button>
                             
                         </div>
                     </div>
               </form>
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <!-- End Asset Modal -->
<!-- Categories modal -->

<div class="modal fade" id="exampleModalCenter4" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header bg-primary text-white text-center text-uppercase">
          <h5 class="modal-title" id="exampleModalLongTitle">Add New Asset Category</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            @include('messages.custom')
            <form action="{{ route('categories.post') }}" method="post" enctype="multipart/form-data">
                 {{ csrf_field() }}
                
                 <div class="form-group row{{ $errors->has('name')? 'has-error':''}}">
                     <label for="name" class="col-md-4 form-label text-md-right">Name</label>
                     <div class="col-md-8">
                         <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                         {{-- <span class="alert alert-danger">{{ $errors->first('name') }}</span> --}}
                     </div>
                 </div>
                  <div class="form-group row {{ $errors->has('type') ? 'has-error':''}}">
                     <label for="type" class="col-md-4 form-labe text-md-right">Type</label>
                     <div class="col-md-8 col-md-offset-4">
                         {{-- <div class="hidden">{{ $category_types = App\Settings\Categories\CategoryTypeModel::all()}}</div> --}}
                        {{--  @if(count($category_types) > 0)
                             <select name="type" id="type" class="form-control">
                                <option value="">Select Category type...</option>
                                 @foreach($category_types as $key=>$value)
                                         <option value="{{ $value->type }}">{{ $value->type }}</option>
                                 @endforeach
                             </select>


                         @endif
                          --}}
                             <button type="submit" class="btn btn-primary"><i class="fa fa-send"></i>Submit</button>
                     </div> 
                     <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      </div>
                 </div>
                 <!--end category -->
               

    </section>



</div>

@endsection