@extends('mainlayout')

@section('content')
    <div class="content-wrapper">
            <section class=" content-header text-center">
                    <h4><i class="fa fa-asset bg-secondary"></i>
                      Asset Stock
                    </h4>
                  </section>
        <section class="content">
            <div class="row">
                {{-- <div class="col-md-2"></div> --}}
                <div class="col-md-12">
                    <a href="{{ route('download-all-stocked-assets') }}" class="btn btn-danger btn-sm"><i class="fa fa-arrow-down"></i> Export to pdf</a>
                    <div class="box box-default">
                        <div class="box-header">
                            <a  href="{{ route('assets.create') }}" class="btn btn-primary">New</a>
                           
                        </div>
                        <div class="box-body">
                            @include('messages.custom')
                            @if(count($asset) >0)
                            <table class="table table-bordered table-responsive">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                      
                                        <th>Category</th>
                                        <th>Available Balance</th>
                                        <th>Given Out</th>
                                        <th>Total</th>
                                        <th>View</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                @foreach($asset as $s)
                                    {{-- <tbody> --}}
                                        <tr>
                                            <td>{{ $s->id }}</td>
                                         
                                            <td>{{ $s->category->name }}</td>
                                            <td>{{ $s->available_balance}} </td>
                                            <td>{{ $s->given_out }}</td>
                                            <td>{{ $s->total }}</td>
                                            <td class="pt-2-half">
                                                <a href="{{ route('asset.show', $s->id) }}" class="btn btn-success btn-sm"><i class="fa fa-eye"></i> View</a>
                                            </td>
                                            <td class="pt-2-half">
                                                <a href="{{ route('asset.edit', $s->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</a>
                                            </td>
                                           <td class="pt-2-half">
                                                <form action="{{ route('asset.delete', $s->id) }}" method="post" type="hidden">
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                                    <button  type="submit" class="btn btn-danger" onclick="if (!confirm('Are you sure you want to proceed?')) { return false }"><span>Delete</span></button>
                                                </form>
                                            </td>
                                        </tr>
                                    {{-- </tbody> --}}
                                @endforeach
                            </table>
                            {{ $asset->links() }}
                            @endif
                        </div>
                    </div>
                </div>
                {{-- <div class="col-md-2"></div> --}}
            </div>
        </section>
    </div>
    <script>
        $('.delete').on("click", function (e) {
    e.preventDefault();

    var choice = confirm($(this).attr('data-confirm'));

    if (choice) {
        window.location.href = $(this).attr('href');
    }
});
    </script>
@endsection