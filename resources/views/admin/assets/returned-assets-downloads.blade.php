
    <div class="content-wrapper">
            <style>
                    table {
                      width: 100%;
                      border: 1px solid #000;
                      text-align: left;
                    }
                    
                    th, td {
                    padding: 15px;
                    text-align: left;
                    }
                    
                    tr:nth-child(even){background-color: #f2f2f2}
                    
                    th {
                      background-color: #4CAF50;
                      color: white;
                    }
                    </style>
                <div style="width:100%; height:100px; text-align:center" class="text-center">
                    <img src="{{ public_path('/storage/images/logos/mandera-county.jpg')}}" alt="" style="height:50px;align:center;border-radius:50%" style="img-thumbnail">
                    <h2 style="font-style:Sans Serif; font-weight:bold; color:brown">MANDERA COUNTY GOVERNMENT</h2>
                    <h3 style="center">Summary of All Returned Assets</h3>
                </div>
                
        <section class="content">
            <div class="row">
                {{-- <div class="col-md-2"></div> --}}
                <div class="col-md-12">

                    <div class="box box-default">

                        <!-- Search form -->
  <div class="md-form mt-0">
      </div>
                        <div class="box-body">
                            @if(count($returned_assets) >0)
                            <table class="table table-bordered table-responsive" id="search">
                                <thead>
                                    <tr>
                                        <th>SR/NO</th>
                                        <th>Name</th>
                                        <th>Category</th>
                                        <th>Serial Number</th>
                                        <th>Model Number</th>
                                        <th>Sub County</th>
                                        <th>Date Returned</th>
                                        <th>View</th>
                                    </tr>
                                </thead>
                                @foreach($returned_assets as $returned_asset)
                                    {{-- <tbody> --}}
                                        <tr>
                                         
                                            <td>{{ $returned_asset->id }}</td>
                                            <td>{{ $returned_asset->asset}}</td>
                                            <td>{{ $returned_asset->category->name}}</td>
                                            <td>{{ $returned_asset->serialnumber}} </td>
                                            <td>{{ $returned_asset->modelnumber}}</td>
                                            <td>{{ $returned_asset->location}}</td>
                                            <td  class="pt-2-half">
                                                {{$returned_asset->created_at->format('d/m/Y')}}
                                            </td>
                                            
                                            
                                                        
                            
                                        </tr>
                                @endforeach
                            </table>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    
    
