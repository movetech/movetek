
    <div class="content-wrapper">
           
    <div class="content-wrapper">
            <style>
                    table {
                      width: 100%;
                      border: 1px solid #000;
                      text-align: left;
                    }
                    
                    th, td {
                    padding: 15px;
                    text-align: left;
                    }
                    
                    tr:nth-child(even){background-color: #f2f2f2}
                    
                    th {
                      background-color: #4CAF50;
                      color: white;
                    }
                    </style>
                <div style="width:100%; height:100px; text-align:center" class="text-center">
                    <img src="{{ public_path('/storage/images/logos/mandera-county.jpg')}}" alt="" style="height:50px;align:center;border-radius:50%" style="img-thumbnail">
                    <h2 style="font-style:Sans Serif; font-weight:bold; color:brown">MANDERA COUNTY GOVERNMENT</h2>
                    <h3 style="center">Summary of All Issued Assets</h3>
                </div>
        <section class="content">
            <div class="row">
                {{-- <div class="col-md-2"></div> --}}
                <div class="col-md-12">
                    <div class="box box-default">
                        
                        
  <div class="md-form mt-0">
      </div>
                        <div class="box-body">
                            @if(count($issued) >0)
                            <table class="table table-bordered table-responsive " id="search">
                                <thead class="bg-success">
                                    <tr>
                                        <th>SR/NO</th>
                                        <th>Asset Name</th>
                                        <th>Category</th>
                                        <th>Serial Number</th>
                                        <th>Model Number</th>
                                        <th>Asset TagS</th>
                                        <th>Person Issued</th>
                                        <th>Department</th>
                                        <th>Sub County</th>
                                        
                                    </tr>
                                </thead>
                                @foreach($issued as $issue)
                                        <tr>
                                            <td>{{ $issue->id }}</td>
                                            <td>{{ $issue->asset }}</td>
                                            <td>{{ $issue->category->name}}</td>
                                            
                                            <td>{{ $issue->serialnumber}} </td>
                                            <td>{{ $issue->modelnumber}}</td>
                                            <td>{{ $issue->tag}}</td>
                                            <td>{{ $issue->user->name }}</td>
                                            <td>{{ $issue->department}}</td>
                                            <td>{{ $issue->location}}</td>
                                           
                            
                                        </tr>
                                @endforeach
                            </table>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

