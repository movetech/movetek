@extends('mainlayout')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
    <!-- Main content -->
    <section class="content">
        <div class="row">
<div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="box box-primary">
                    <div class="box-body">
                       <div class="card">
                           <div class="card-body">
                                @include('messages.custom')
                               <form action="#" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                   
                                    <div class="form-group row{{ $errors->has('name')? 'has-error':''}}">
                                        <label for="name" class="col-md-4 form-label text-md-right">Name</label>
                                        <div class="col-md-8">
                                            <input type="text" id="name" name="name" class="form-control" value="{{$eachissuence->user->name }}" disabled>
                                            {{-- <span class="alert alert-danger">{{ $errors->first('name') }}</span> --}}
                                        </div>
                                    </div>
                                    <div class="form-group row {{ $errors->has('available_balance') ? 'has-error':''}}">
                                            <label for="available_balance" class="col-md-4 form-labe text-md-right">Category</label>
                                            <div class="col-md-8">
                                                <input type="text" name="available_balance" class="form-control" value="{{$eachissuence->category}}" disabled>
                                                 {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                            </div>
                                        </div>
                                    <div class="form-group row {{ $errors->has('available_balance') ? 'has-error':''}}">
                                        <label for="available_balance" class="col-md-4 form-labe text-md-right">SerialNmber</label>
                                        <div class="col-md-8">
                                            <input type="text" name="available_balance" class="form-control" value="{{$eachissuence->serialnumber}}" disabled>
                                             {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                        </div>
                                    </div>
                                    <div class="form-group row {{ $errors->has('given_out') ? 'has-error':''}}">
                                            <label for="given_out" class="col-md-4 form-labe text-md-right">Modelnumber</label>
                                            <div class="col-md-8">
                                                <input type="text" id="modelnumber" name="modelnumber" class="form-control" value="{{$eachissuence->modelnumber }}" disabled>
                                                 {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                            </div>
                                    </div>
                                    <div class="form-group row {{ $errors->has('total') ? 'has-error':''}}">
                                            <label for="total" class="col-md-4 form-labe text-md-right">Location</label>
                                            <div class="col-md-8">
                                                <input type="text" name="total" class="form-control" value="{{$eachissuence->location}}" disabled>
                                                 {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                            </div>
                                        </div>
                                    <div class="form-group row">
                                        <div class="col-md-8 col-md-offset-4">
                                            <a href="{{ route('assets.show.issued') }}" class="btn btn-info" style="float:right">
                                                <i class="fa fa-arrow-left"></i> Back
                                            </a>
                                           
                                        </div>
                                    </div>
                              </form>
                             
                           </div>
                       </div>
                    </div>


                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
        


    </section>



</div>


@endsection