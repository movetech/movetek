@extends('mainlayout')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
    <!-- Main content -->
    <section class="content">
        <div class="row">
<div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="box box-primary">
                    <div class="box-body">
                       <div class="card">
                           <div class="card-body">
                                @include('messages.custom')
                                <form action="{{ route('asset.update', $asset->id) }}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                   
                                    <div class="form-group row{{ $errors->has('name')? 'has-error':''}}">
                                        <label for="name" class="col-md-4 form-label text-md-right">Name</label>
                                        <div class="col-md-8">
                                            <input type="text" name="name" class="form-control" value="{{ $asset->name }}">
                                            {{-- <span class="alert alert-danger">{{ $errors->first('name') }}</span> --}}
                                        </div>
                                    </div>
                                    <div class="form-group row{{ $errors->has('category_id')? 'has-error':''}}">
                                            <label for="category_id" class="col-md-4 form-label text-md-right">Category</label>
                                            <div class="col-md-8">
                                                <select name="category_id" id="category_id" class="form-control" >
                                                  {{-- <option value=""> {{ $category_name }}  </option> --}}
                                                  @if(isset($category))      
                                                        @foreach($category as $q)
                                                                <option value="{{ $q->id}}">{{ $q->name}}</option>
                                                        @endforeach
                                                  @endif
                                                </select>
                                            </div>
                                        </div>
                                    <div class="form-group row {{ $errors->has('available_balance') ? 'has-error':''}}">
                                        <label for="available_balance" class="col-md-4 form-labe text-md-right">Available Balance</label>
                                        <div class="col-md-8">
                                            <input type="number" name="available_balance" class="form-control" value="{{ $asset->available_balance }}">
                                             {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                        </div>
                                    </div>
                                    <div class="form-group row {{ $errors->has('given_out') ? 'has-error':''}}">
                                            <label for="given_out" class="col-md-4 form-labe text-md-right">Given Out</label>
                                            <div class="col-md-8">
                                                <input type="number" name="given_out" class="form-control" value="{{ $asset->given_out }}">
                                                 {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                            </div>
                                    </div>
                                    <div class="form-group row {{ $errors->has('total') ? 'has-error':''}}">
                                            <label for="total" class="col-md-4 form-labe text-md-right">Total</label>
                                            <div class="col-md-8">
                                                <input type="text" name="total" class="form-control" value="{{ $asset->total }}">
                                                 {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                            </div>
                                        </div>
                                    <div class="form-group row">
                                        <div class="col-md-8 col-md-offset-4">
                                                <form action="{{ route('asset.update',$asset->id) }}" method="POST">
                                                        <input type="hidden" name="_method" value="PUT">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <input type="submit" class="btn btn-primary" value="Update">
                                                    </form>
                                            <a href="{{ route('assets.list') }}" class="btn btn-info" style="float:right">
                                                <i class="fa fa-arrow-left"></i> Back
                                            </a>
                                        </div>
                                    </div>
                              </form>
                           </div>
                       </div>
                    </div>


                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
        


    </section>



</div>

@endsection