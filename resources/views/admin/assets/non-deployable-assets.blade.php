@extends('mainlayout')

@section('content')
<link rel="stylesheet" href="{{asset('//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css')}}">
<script src="{{asset('https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js')}}"></script>
    <div class="content-wrapper">
            <section class=" content-header text-center">
                    <h4><i class="fa fa-asset bg-secondary"></i>
                   Non Deployable Assets
                    </h4>
                   
                  </section>
                
        <section class="content">
            <div class="row">
                {{-- <div class="col-md-2"></div> --}}
                <div class="col-md-12">

                    <div class="box box-default">

                        <!-- Search form -->
  <div class="md-form mt-0">
      </div>
                        <div class="box-body">
                            @if(count($non_deployable_assets) >0)
                            <table class="table table-bordered table-responsive" id="search">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Category</th>
                                        <th>Serial Number</th>
                                        <th>Model Number</th>
                                        <th>Sub County</th>
                                        <th>Date Returned</th>
                                        <th>View</th>
                                    </tr>
                                </thead>
                                @foreach($non_deployable_assets as $asset)
                                    {{-- <tbody> --}}
                                        <tr>
                                         
                                            <td>{{ $asset->id }}</td>
                                            <td>{{ $asset->asset}}</td>
                                            <td>{{ $asset->category->name}}</td>
                                            <td>{{ $asset->serialnumber}} </td>
                                            <td>{{ $asset->modelnumber}}</td>
                                            <td>{{ $asset->location}}</td>
                                            <td  class="pt-2-half">
                                                {{$asset->created_at->format('d/m/Y')}}
                                            </td>
                                            <td  class="pt-2-half">
                                                <a href="{{ route('assets.each', $returned_asset->id) }}" style="float:left;" data-placement="top" data-toggle="tooltip" title="view"><button class="btn btn-info btn-xs pull-right " data-title="view" data-toggle="modal" data-target="#view" ><span class="fa fa-eye"></span></button></a>
                                            </td>
                                            
                                                        
                            
                                        </tr>
                                    {{-- </tbody> --}}
                                @endforeach
                            </table>
                            {{ $returned_assets->links() }}
                            @endif
                        </div>
                    </div>
                </div>
                {{-- <div class="col-md-2"></div> --}}
            </div>
        </section>
    </div>
    <script type="text/javascript">
        $('#returned').on('keyup', function(){
            $value = $(this).val();
            var token = $("meta[name='csrf-token']").attr("content");
           $.ajax({
               
               type:'GET',
               url:'{{URL::to('admin/searchassets')}}',
               data:{
                   'search':$value, "_token":token,
               },
               success:function(data){
                   $('tbody').html(data);
               } 
           });
        });
       
      
    
    </script>
    
@endsection