@extends('mainlayout')

@section('content')
<link rel="stylesheet" href="{{asset('//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css')}}">
<script src="{{asset('https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js')}}"></script> 
    <div class="content-wrapper">
            <section class=" content-header text-center">
                    <h4><i class="fa fa-asset bg-secondary"></i>
                   Returned Assets
                    </h4>
                   
                  </section>
                
        <section class="content">
            <div class="row">
                {{-- <div class="col-md-2"></div> --}}
                <div class="col-md-12">
                        <a href="{{ route('download-all-returned-assets' ) }}" class="btn btn-sm btn-danger"><i class="fa fa-arrow-down"></i> Export to pdf</a>

                    <div class="box box-default">

                        <!-- Search form -->
  <div class="md-form mt-0">
        <input class="form-control" type="text" placeholder="Search" aria-label="Search" id="returned" name="search">
      </div>
                        <div class="box-body">
                            @if(count($returned_assets) >0)
                            <table class="table table-bordered table-responsive" id="search">
                                <thead>
                                    <tr>
                                        
                                        <th>User</th>
                                        <th>Asset</th>
                                        <th>Category</th>
                                        <th>Tag</th>
                                        <th>Serial Number</th>
                                        <th>Model Number</th>
                                        <th>Department</th>
                                        <th>Sub County</th>
                                        <th>Date Returned</th>
                                      
                                    </tr>
                                </thead>
                                @foreach($returned_assets as $returned_asset)
                                    {{-- <tbody> --}}
                                        <tr>
                                         
                                            
                                            <td>{{ $returned_asset->returnasset->name}}</td>
                                            <td>{{ $returned_asset->asset}}</td>
                                            <td>{{ $returned_asset->category->name}}</td>
                                            <td>{{ $returned_asset->tag}}</td>
                                            <td>{{ $returned_asset->serialnumber}} </td>
                                            <td>{{ $returned_asset->modelnumber}}</td>
                                            <td>{{ $returned_asset->department_code}}</td>
                                            <td>{{ $returned_asset->location}}</td>
                                            <td  class="pt-2-half">
                                                {{$returned_asset->created_at->format('d/m/Y')}}
                                            </td>
                                          
                                            
                                                        
                            
                                        </tr>
                                    {{-- </tbody> --}}
                                @endforeach
                            </table>
                            {{ $returned_assets->links() }}
                            @endif
                        </div>
                    </div>
                </div>
                {{-- <div class="col-md-2"></div> --}}
            </div>
        </section>
    </div>
    <script type="text/javascript">
        $('#returned').on('keyup', function(){
            $value = $(this).val();
            var token = $("meta[name='csrf-token']").attr("content");
           $.ajax({
               
               type:'GET',
               url:'{{URL::to('admin/searchassets')}}',
               data:{
                   'search':$value, "_token":token,
               },
               success:function(data){
                   $('tbody').html(data);
               } 
           });
        });
       
      
    
    </script>
    
@endsection