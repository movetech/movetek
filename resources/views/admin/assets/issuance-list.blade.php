@extends('mainlayout')
@section('content')
<link rel="stylesheet" href="{{asset('//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css')}}">
<script src="{{asset('https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js')}}"></script> 
    <div class="content-wrapper">
            <section class=" content-header text-center">
                    <h4><i class="fa fa-asset bg-secondary"></i>
                   Issued Assets Lists
                    </h4>
                  </section>
                  @include('messages.custom')
        <section class="content">
            <div class="row">
                {{-- <div class="col-md-2"></div> --}}
                <div class="col-md-12">
                    <div class="box box-default">
                            <a href="{{ route('download-all-issued-assets') }}" class="btn btnsm btn-danger pull-left"><i class="fa fa-arrow-down"></i> Export to pdf</a>

                               <a style="float:right" href="{{route('assets.assign')}}" class="btn btn-info btn-sm">New</a>
                        
                        <!-- Search form -->
  <div class="md-form mt-0">
        <input class="form-control" id="search" type="text" placeholder="Search&hellip;" aria-label="Search">
      </div>
                        <div class="box-body">
                            @if(count($issued) >0)
                            <table class="table table-bordered table-responsive " id="search">
                                <thead class="bg-success">
                                    <tr>
                                        <th>Id</th>
                                        <th>Asset Name</th>
                                        <th>Category</th>
                                        <th>Serial Number</th>
                                        <th>Model Number</th>
                                        <th>Asset TagS</th>
                                        <th>Person Issued</th>
                                        <th>Department</th>
                                        <th>Ministry</th>
                                        <th>Sub County</th>
                                        <th>View</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                        <th>Checkin(Returned)</th>
                                    </tr>
                                </thead>
                                @foreach($issued as $issue)
                                    {{-- <tbody> --}}
                                        <tr>
                                            {{-- <tbody> --}}
                                            <td>{{ $issue->id }}</td>
                                            <td>{{ $issue->asset }}</td>
                                            <td>{{ $issue->category->name}}</td>
                                            
                                            <td>{{ $issue->serialnumber}} </td>
                                            <td>{{ $issue->modelnumber}}</td>
                                            <td>{{ $issue->tag}}</td>
                                            <td>{{ $issue->user->name }}</td>
                                            <td>{{ $issue->department}}</td>
                                            <td>{{ $issue->ministry }}</td>
                                            <td>{{ $issue->location}}</td>
                                           
                                                    <td  class="pt-2-half">
                                                            <a href="{{ route('assets.each', $issue->id) }}" style="float:left;" data-placement="top" data-toggle="tooltip" title="view"><button class="btn btn-info btn-xs pull-right " data-title="view" data-toggle="modal" data-target="#view" ><span class="fa fa-eye"></span></button></a>
                                                            </td>
                                                            <td  class="pt-2-half">
                                                                    <a href="{{ route('asset.show', $issue->id) }}" style="float:left;" data-placement="top" data-toggle="tooltip" title="view"><button class="btn btn-info btn-xs pull-right " data-title="view" data-toggle="modal" data-target="#view" ><span class="fa fa-edit"></span></button></a>
                                                                    </td>
                                                                    <td  class="pt-2-half">
                                                                            <form action="{{ route('delete-issued-asset',$issue->id ) }}" method="POST">
                                                                                    <input type="hidden" name="_method" value="DELETE">
                                                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                                    <button  type="submit" class="btn btn-danger btn-xs" onclick="if (!confirm('Are you sure you want to delete this record?')) { return false }"><i class="fa fa-trash"></i></button>
                                                                                </form>
                                                                        {{-- <p style="float:left;" data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-danger btn-xs pull-right" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span></button></p> --}}
                                                                    </td>
                                                                    <td  class="pt-2-half">
                                                                            <form action="{{ route('assets.checkout') }}" method="post" enctype="multipart/form-data">
                                                                                {{ csrf_field() }}
                                                                            <input type="hidden" name="tag" value="{{$issue->tag}}">
                                                                            <button type="submit" onclick="if (!confirm('Are you sure you want to proceed?')) { return false }" class="btn btn-sm btn-info btn-xs pull-right " data-title="checkout" data-toggle="modal" data-target="#view" >Checkin(Returned)</button>
                                                                            </form>
                                                                        </td>
                                                        
                            
                                            {{-- </tbody> --}}
                                        </tr>
                                    {{-- </tbody> --}}
                                @endforeach
                            </table>
                            {{ $issued->links() }}
                            @endif
                        </div>
                    </div>
                </div>
                {{-- <div class="col-md-2"></div> --}}
            </div>
        </section>
    </div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#search').on('keyup',function(){
        $value=$(this).val();
        //$select=$('#inputtype').val();
        var token = $("meta[name='csrf-token']").attr("content");
        $.ajax({
        type : 'get',
        url : '{{URL::to('admin/assets/search')}}',
         
        data:{'search':$value, "_token": token,},
         
        success:function(data){
         //console.log(data);
        $('tbody').html(data);
                          }
         
        });
         
       }); 
       // 
        
    });
</script>
@endsection