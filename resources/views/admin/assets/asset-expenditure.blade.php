@extends('mainlayout')

@section('content')
<link rel="stylesheet" href="{{asset('//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css')}}">
<script src="{{asset('https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js')}}"></script>
<script src="{{asset('html.js')}}"></script>
    <div class="content-wrapper">
            <section class=" content-header text-center">
                    <h4><i class="fa fa-asset bg-secondary"></i>
                     Expenditure
                    </h4>
                    <button class="btn btn-xs btn-primary"onclick="generatePDF()">Download as PDF</button>
                   
                   
                  </section>
               
                 
                  <div class="dropdown  pull-right">
                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Filter Financial Years
                    <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        @foreach($financial_years as $financial_year)
                    <li><a href="{{route('year',$financial_year->id)}}">{{$financial_year->year}}</a></li>
                      @endforeach
                      
                    </ul>
                  </div>
                 
        <section id="expenditure" class="content">
            <div class="row">
                {{-- <div class="col-md-2"></div> --}}
                <div class="col-md-12">
                    <div class="box box-default">
                        <div class="box-header">
                        </div>
                        <div class="box-body">
                                <div class="panel panel-primary">
                                        <div class="panel-heading"><h4>Total Expenditure: </h4></div>
                                        <h4><strong>Financial Year {{$years->year}}: </strong></h4>
                                      </div>
                            @if($allassets)
                            
                                @foreach($allassets as $s)
                                    {{-- <tbody> --}}
                                            <div class="col-md-6">
                                                    <div class="panel panel-info">
                                                        <div class="panel-heading">{{$s->name}}</div>
                                                    <div style="color:black;" class="panel-body"> 
                                                          <h5 class="card-title">Number of Assets:{{$s->totals}}</h5>
                                                          <p class="card-text">Financial year:{{$s->financial_year}}</p>
                                                          <p class="card-text bg-primary">Total Cost:{{$s->cost}}</p>
                                                         
                                         
                                                         
                                                        </div>
                                                  </div>
                                                  </div>
                                @endforeach
                           
                            @endif
                           
                            <div class="md-form mt-0">
                                    <div   class="box-body">
                                        @if(count($budgets) >0)
                                       
                                        <table  class="table table-bordered table-responsive" id="table">
                                            <thead>
                                                <tr>
                                                    <th>Project</th>
                                                    <th>Ministry</th>
                                                    <th>Department</th>
                                                    <th>Financial Year</th>
                                                    <th>No. Of Phases</th>
                                                    <th>Phases Complete</th>
                                                    <th>Amount Used</th>
                                                    <th>Supplementary Budget</th>
                                                    <th>Total Expenditure</th>
                                                   
                                                     <span id="val"></span>
                                                    
                                                    
                                                </tr>
                                               
                                            </thead>
                                          
                                                {{-- <tbody> --}}
                                                    <tr>
                                                        <tbody>
                                                            @foreach($budgets as $budget)
                                                            {{-- <tbody> --}}
                                                                <tr>
                                                           
                                                                    <p class="hidden">
                                                                        {{$phases=App\Phases::where('REF',$budget->REF)->get()}}
                                                                        {{$completed=App\Phases::where('REF',$budget->REF)->where('status','complete')->get()}}
                                                                        {{$phase_sum=App\Phases::where('REF',$budget->REF)->where('status','complete')->sum('phase_cost')}}
                                                                        {{$supplemented=App\Projects\SupplementaryBudgetsModel::where('REF',$budget->REF)->sum('amount')}}
                                                                        </p>
                                                                    <td>{{$budget->name}}</td>
                                                                    <td>{{$budget->ministry}}</td>
                                                                    <td>{{$budget->department_code}}</td>
                                                                    <td>{{$budget->financial_year}} </td>
                                                                    @if(count($phases)>0)
                                                                    <td>{{count($phases)+1}}</td>
                                                                    @else
                                                                    <td>1</td>
                                                                    @endif
                                                                    @if($completed)
                                                                    <td>{{count($completed)+1}}</td>
                                                                    @else
                                                                    @if(!($budget->status=="initiated"))
                                                                    <td>1</td>
                                                                    @else
                                                                    <td>1</td>
                                                                    @endif
                                                                    @endif
                                                                    <td>{{$budget->amount_paid+$phase_sum}}</td>
                                                                    @if($supplemented)
                                                                    <td>{{$supplemented}}</td>
                                                                    @else
                                                                    <td>0</td>
                                                                    @endif
                                                                    <td>{{$budget->amount_paid+$supplemented+$phase_sum}}</td>
                                                                   
                                                                            
                                                    
                                                                </tr>
                                                               
                                                            {{-- </tbody> --}}
                                                        @endforeach
                                                        <tr class="bg-primary">
                                                                <td></td>
                                                                <td><strong></strong><h3>Total Expense on Projects</h3></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                
                                                                
                                                                <td ><strong></strong><h3 id="sum1"></h3></td>
                                                              </tr>
                                                   
                                                 
                                                    @endif
                                                              
                                                        </tbody>
                                                    </tr>
                                                   
                                        </table>
                                  
                                       
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="col-md-2"></div> --}}
            </div>
        </section>
    </div>
    <script type="text/javascript">
var sum1 = 0;
//var sum2 = 0;
$("#table tr").not(':first').not(':last').each(function() {
  sum1 +=  getnum($(this).find("td:eq(8)").text());
  //sum2 +=  getnum($(this).find("td:eq(9)").text());
  function getnum(t){
  	if(isNumeric(t)){
    	return parseInt(t,10);
    }
    return 0;
	 	function isNumeric(n) {
  		return !isNaN(parseFloat(n)) && isFinite(n);
		}
  }
});
$("#sum1").text(sum1);
//$("#sum2").text(sum2);

      function generatePDF() {
  // Choose the element that our invoice is rendered in.
  const element = document.getElementById("expenditure");
var opt = {
  margin:       1,
  filename:     'budget.pdf',
  image:        { type: 'jpeg', quality: 0.98 },
  html2canvas:  { scale: 8},
  jsPDF:        { unit: 'in', format: 'letter', orientation: 'landscape' }
};


// New Promise-based usage:
html2pdf().set(opt).from(element).save();

// Old monolithic-style usage:
html2pdf(element, opt);
      }
    
    </script>
@endsection