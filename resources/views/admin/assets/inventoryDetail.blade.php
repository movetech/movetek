@extends('mainlayout')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class=" content-header text-center">
        <h4><i class="fa fa-asset bg-secondary"></i>
                Inventory Details
        </h4>
      </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
<div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="box box-primary">
                    <div class="box-body">
                       <div class="card">
                           <div class="card-body">
                            @include('messages.custom')
                               <form action="" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                   
                                    <div class="form-group row{{ $errors->has('name')? 'has-error':''}}">
                                        <label for="name" class="col-md-4 form-label text-md-right">Manufacturer</label>
                                        <div class="col-md-8">
                                            <input type="text" name="manufacturer" class="form-control" disabled value="{{ $inventory->manufacturer }}">
                                            {{-- <span class="alert alert-danger">{{ $errors->first('name') }}</span> --}}
                                        </div>
                                    </div>
                                    <div class="form-group row{{ $errors->has('category_id')? 'has-error':''}}">
                                            <label for="category_id" class="col-md-4 form-label text-md-right">Suppliers</label>
                                            <div class="col-md-8">
                                                <input type="text" name="" value="{{ $inventory->supplier }}" class="form-control" disabled>
                                          
                                                {{-- <span class="alert alert-danger">{{ $errors->first('name') }}</span> --}}
                                            </div>
                                        </div>
                                    <div class="form-group row {{ $errors->has('available_balance') ? 'has-error':''}}">
                                        <label for="available_balance" class="col-md-4 form-labe text-md-right">Asset</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" value="{{ $inventory->asset_name }}" disabled>
                                            
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="modelnumber" class="col-md-4 form-label text-md-right">Model Number</label>
                                        <div class="col-md-8">
                                            <input type="text"  class="form-control"  name="modelnumber" value="{{ $inventory->modelnumber }}" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="serialnumber" class="col-md-4 form-label text-md-right">Serial Number</label>
                                        <div class="col-md-8">
                                            <input type="text"  class="form-control" name="serialnumber" value="{{ $inventory->serialnumber }}" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="category" class="col-md-4 form-label text-md-right" id="category">Category</label>
                                        <div class="col-md-8">
                                            <input type="text" name="category" class="form-control" value="{{ $inventory->category->name }}" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group row {{ $errors->has('available_balance') ? 'has-error':''}}">
                                        <label for="tag" class="col-md-4 form-labe text-md-right">Asset Tag</label>
                                        <div class="col-md-8">
                                            <input type="text" name="tag" class="form-control" disabled value="{{ $inventory->tag }}">
                                             {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                        </div>
                                    </div>
                                    <div class="form-group row {{ $errors->has('available_balance') ? 'has-error':''}}">
                                        <label for="available_balance" class="col-md-4 form-labe text-md-right">Cost</label>
                                        <div class="col-md-8">
                                            <input type="text" name="cost" class="form-control"  disabled value="{{ $inventory->cost }}">
                                             {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                        </div>
                                    </div>
                                    <div class="form-group row {{ $errors->has('available_balance') ? 'has-error':''}}">
                                            <label for="available_balance" class="col-md-4 form-labe text-md-right">Cost</label>
                                            <div class="col-md-8">
                                                <input type="text" name="cost" class="form-control"  disabled value="{{ $inventory->cost }}">
                                                 {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                            </div>
                                        </div>
                                    <div class="form-group row {{ $errors->has('given_out') ? 'has-error':''}}">
                                            <label for="given_out" class="col-md-4 form-labe text-md-right">Purchase Date</label>
                                            <div class="col-md-8">
                                                <input type="date" name="purchase_date" class="form-control"  disabled value="{{ $inventory->purchase_date }}">
                                                 {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                            </div>
                                    </div>
                                    <div class="form-group row {{ $errors->has('total') ? 'has-error':''}}">
                                            <label for="total" class="col-md-4 form-labe text-md-right">Warranty</label>
                                            <div class="col-md-8">
                                                <input type="text" name="warrant" class="form-control"  disabled value="{{ $inventory->warrant }}">
                                                 {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                            </div>
                                        </div>
                                    <div class="form-group row">
                                        <label for="financial_year" class="col-md-4 form-label text-md-right">Financial Year</label>
                                        <div class="col-md-8">
                                            <input type="text" name="financial_year" class="form-control" value="{{ $inventory->financialyear }}" disabled>
                                        </div>
                                    </div>
                                        <div class="form-group row {{ $errors->has('total') ? 'has-error':''}}">
                                            <label for="total" class="col-md-4 form-labe text-md-right" >Delivery Date</label>
                                            <div class="col-md-8">
                                                <input type="date" name="delivery_date" class="form-control"  disabled value="{{ $inventory->delivery_date }}">
                                                 {{-- <span class="alert alert-danger">{{ $errors->first('type') }}</span> --}}
                                            </div>
                                        </div>
                                    <div class="form-group row">
                                        <div class="col-md-8 col-md-offset-4">
                                           
                                            <a href="javascript:history.back()" class="btn btn-info pull-right"><i class="fa fa-arrow-left"></i>  Back</a>
                                            
                                        </div>
                                    </div>
                              </form>
                           </div>
                       </div>
                    </div>


                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
        


    </section>



</div>

@endsection