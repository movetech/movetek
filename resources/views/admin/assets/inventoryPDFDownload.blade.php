
    <div class="content-wrapper">
            <style>
                    table {
                      width: 100%;
                      border: 1px solid #000;
                      text-align: left;
                    }
                    
                    th, td {
                    padding: 15px;
                    text-align: left;
                    }
                    
                    tr:nth-child(even){background-color: #f2f2f2}
                    
                    th {
                      background-color: #4CAF50;
                      color: white;
                    }
                    </style>
                <div style="width:100%; height:100px; text-align:center" class="text-center">
                    <img src="{{public_path('/assets/images/bg1.jpg')}}" alt="" style="height:50px;align:center;border-radius:50%" style="img-thumbnail">
                    <h2 style="font-style:Sans Serif; font-weight:bold; color:brown">MANDERA COUNTY GOVERNMENT</h2>
                    <h3 style="center">Summary of All Assets</h3>
                </div>
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-default">
                        
  <div class="md-form mt-0">
      </div>
                        <div class="box-body">
                            @if(count($stocks) >0)
                            <table class="table table-bordered table-responsive" id="search">
                                <thead>
                                    <tr>
                                        <th>Manufacturer</th>
                                        <th>Supplier</th>
                                        <th>Cost</th>
                                        <th>Asset Tag</th>
                                        <th>Asset</th>
                                        <th>Assset Type</th>
                                        <th>Category</th>
                                        <th>serialnumber</th>
                                        <th>modelnumber</th>
                                        <th>Purchase Date</th>
                                        <th>Warrant(Months)</th>
                                        <th>Status</th>
                                        <th>State</th>
                                        <th>Lifespan(Months)</th>
                                        <th>Delivered Date</th>
                                        <th>Financial Year</th>
                                        
                                    </tr>
                                </thead>
                                @foreach($stocks as $stock)
                                        <tr>
                                            <td>{{ $stock->manufacturer}}</td>
                                            <td>{{ $stock->supplier}}</td>
                                            <td>{{ $stock->cost}} </td>
                                            <td>{{ $stock->tag}}</td>
                                            <td>{{ $stock->asset_name}}</td>
                                            <td>{{ $stock->asset_type}}</td>
                                            <td>{{ $stock->category->name}}</td>
                                            <td>{{ $stock->serialnumber}}</td>
                                            <td>{{ $stock->modelnumber}}</td>
                                            <td>{{ $stock->purchase_date}}</td>
                                            <td>{{ $stock->warrant }}</td>
                                            <td>{{ $stock->status}}</td>
                                            <td>{{ $stock->state}}</td>
                                            <td>{{ $stock->lifespan}}</td>
                                            <td>{{ $stock->delivery_date}}</td>
                                            <td>{{ $stock->financial_year}}</td>
                                            </tbody>
                                        </tr>
                                @endforeach
                            </table>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

