@extends('mainlayout')

@section('content')
{{-- {!! Charts::assets() !!} --}}


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        @include('messages.custom')
        <section class="content-header">
            <h1>
                Admin Dashboard

            </h1>
            <ol class="breadcrumb">
                <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
                <li ><a href=""><i class="fa fa-dashboard"></i>Dashboard </a></li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <!-- Info boxes -->
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>

                        <div class="info-box-content">
                            <div class="hidden">{{ $projects = count(App\Projects\Projects::all()) }}</div>
                            <span class="info-box-text">All Projects</span>
                            <span class="info-box-number">{{ $projects }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-red"><i class="fa fa-list"></i></span>

                        <div class="info-box-content">
                            <div class="hidden">
                                {{ $assets = count(App\Assets\Inventory::all()) }}
                            </div>
                          <span class="info-box-text">All Assets</span>
                            <span class="info-box-number">{{ $assets }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->

                <!-- fix for small devices only -->
                <div class="clearfix visible-sm-block"></div>

                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-green"><i class="fa fa-list"></i></span>

                        <div class="info-box-content">
                            <div class="hidden">
                                {{ $stock = count(App\Assets\AssetsModel::all())}}
                            </div>
                            <span class="info-box-text">Asset Stock</span>
                            <span class="info-box-number">{{ $stock }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

                        <div class="info-box-content">
                         <p class="hidden">{{ $members = count(App\Members\Members::all()) }} </p>
                            <span class="info-box-text">Staff  Members</span>
                            <span class="info-box-number">{{ $members }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
            </div>
          
            <div class="row">
                    <div class="col-md-4">
                        <div class="box">
                            <div class="box-header bg-info text-white text-uppercase">Departments</div>
                            <div class="box-body">
                                 @if(count($departments) > 0)
                                <table class="table table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>SR/NO:</th>
                                            <th>Name</th>
                                            <th>Manager</th>
                                            
                                        </tr>
                                    </thead>
                                    @foreach($department as $key=>$value)
                                        <tbody>
                                            <tr>
                                                <td>{{ $value->id }}</td>
                                                <td>{{ $value->department_code }}</td>
                                                <td>{{ $value->manager }}</td>
                                                
                                            </tr>
                                        </tbody>
                                    @endforeach
                                </table>
                                 @endif
                            </div>
                        </div>
                    </div>
                <div class="col-md-8">
                    <div class="box box-primary">
                        <div class="box-header text-white text-uppercase bg-info">Projects by Subcounty</div>
                        <div class="box-body">
                        @if($allprojects)
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        
                                        <th>Subcounty</th>
                                        <th>Total Projects</th>
                                       
                                    </tr>
                                </thead>
                                    @foreach($allprojects as $value)
                                        <tbody>
                                            <tr>
                                                    <td>{{ $value->subcounty}}</td>
                                                    <td>{{ $value->totals}}</td>
                                                   
                                            </tr>
                                        </tbody>
                                    @endforeach

                               
                            </table>
                            @endif
                        </div>
                        <div class="box-footer bg-dark text-white">
                           
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header bg-success text-white text-uppercase">Assets By Departments</div>
                        <div class="box-body">
                             
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Department</th>
                                            <th>Number Of Assets</th>
                                           
                                            <th></th>
                                        </tr>
                                    </thead>
                                    @foreach($departments as $value) 
                                        <tbody>
                                            <tr>
                                                 <td>{{$value->department }}</td>
                                                <td>{{$value->total }}</td>
                                               
                                            </tr>
                                        </tbody>
                                     @endforeach 
                                </table>
                            
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </section>



    </div>
   
@endsection
{{-- @section('javascript')
{!! $chart1->renderChartJsLibrary() !!}
{!! $chart1->renderJs() !!}
@endsection --}}
