@extends('mainlayout')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">
        <div class="row">
{{-- <div class="col-md-2"></div> --}}
            <div class="col-md-12">
                <div class="box box-default">
                    <div class="box-header text-center" >
                        <h3>Edit Role</h3>
                    </div>
                    <div class="box-body">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                                              @include('admin.includes.logs')
                            <form action="{{ route('role.update', $role->id) }}" method="post">
                                         {{ csrf_field() }}
                                <div class="form-group row">
                                    <label for="role" class="col-md-4 col-form-label text-md-right">Role</label>
                                    <div class="col-md-6">
                                        <input type="text" name="role" class="form-control" value="{{ $role->role}}">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                        <div class="col-md-8 col-md-offset-4">
                                                <form action="{{ route('role.update',$role->id ) }}" method="POST">
                                                        <input type="hidden" name="_method" value="PUT">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <input type="submit" class="btn btn-primary" value="Update">
                                                </form>
                                        </div>
                                    </div>
                            </form>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>  
            </div>
        </div>
    </section>
</div>



@endsection