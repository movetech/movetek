@extends('mainlayout')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
        <section class=" content-header text-center">
                <h4><i class="fa fa-asset bg-secondary"></i>
                  Roles
                </h4>
              </section>
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">
           
        <div class="row">
{{-- <div class="col-md-2"></div> --}}
            <div class="col-md-12">
                <div class="box box-default">
                    <div class="box-header">
                    </div>
                    <div class="box-body">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                                @include('admin.includes.logs')
                            @if(isset($roles))
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Role</th>
                                            <th>Task</th>
                                            <th>Action</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    @foreach($roles as $role)
                                        <tbody>
                                            <tr>
                                                <td>{{ $role->id }}</td>
                                                <td>{{ $role->role }}</td>
                                                <td>{{ $role->task }}</td>
                                                <td class="pt-2-half">
                                                    <a href="{{ route('role.edit', $role->id) }}" class="btn btn-info"><i class="fa fa-edit"></i> Edit</a>
                                                </td>
                                                <td class="pt-2-half">
                                                    <form action="{{ route('role.delete',$role->id) }}" method="post">
                                                        <input type="hidden" name="_method" value="DELETE">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <input type="submit" name="" class="btn btn-danger" value="Delete">
                                                    </form>
                                                </td>
                                                
                                            </tr>
                                        </tbody>
                                    @endforeach
                                </table>
                                {{ $roles->links() }}
                            @endif
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>  
            </div>
        </div>
    </section>
</div>


@endsection