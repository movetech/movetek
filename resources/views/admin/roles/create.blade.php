@extends('mainlayout')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
        <section class=" content-header text-center">
                <h4><i class="fa fa-asset bg-secondary"></i>
                 Add Role
                </h4>
              </section>
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">
        <div class="row">
{{-- <div class="col-md-2"></div> --}}
            <div class="col-md-12">
                <div class="box box-default">
                    <div class="box-header">
                    </div>
                    <div class="box-body">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            @include('admin.includes.logs')
                            <form action="{{ route('roles.post') }}" method="post">
                                         {{ csrf_field() }}
                                <div class="form-group row">
                                    <label for="role" class="col-md-4 col-form-label text-md-right">Role</label>
                                    <div class="col-md-6">
                                        <input type="text" name="role" class="form-control" value="{{ old('role') }}">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                        <div class="col-md-8 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                <i class="fa fa-add"></i> Submit
                                            </button>
                                            <a href="{{ route('roles.all') }}" class="btn btn-info pull-right"><i class="fa fa-arrow-left"></i> Back</a>

                                        </div>

                                </div>
                            </form>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>  
            </div>
        </div>
    </section>
</div>
@endsection