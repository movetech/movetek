@extends('mainlayout')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
        <section class=" content-header text-center">
                <h4><i class="fa fa-asset bg-secondary"></i>
                  Assign Tasks
                </h4>
        </section>
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">
            
        <div class="row">
{{-- <div class="col-md-2"></div> --}}
            <div class="col-md-12">
                <div class="box box-default">
            
                    <div class="box-body">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            @include('admin.includes.logs')
                            <form action="{{ route('submittaskrole') }}" method="post">
                                         {{ csrf_field() }}
                                <div class="form-group row">
                                    <label for="role" class="col-md-4 col-form-label text-md-right">Role</label>
                                    <div class="col-md-5">
                                        <select name="role" class="form-control" >
                                        @foreach($tasks as $task)
                                        <option value="{{$task->role}}">{{$task->role}}</option>
                                        @endforeach

                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="role" class="col-md-4 col-form-label text-md-right">Tasks</label>
                                    <div class="col-md-5">
                                        <div class="row">
                                            <div class="col-md-6">
                                                            <div class="form-group row  {{ $errors->has('user_settings') ? ' has-error' : '' }}">
                                                            <label class="col-md-9 col-form-label" for="text-input">Stock Asset </label>
                                                            <div class="col-md-3">

                                                                <input type="checkbox" value="stockassets"  id="stockassets" name="task[]">
                                                                                
                                                            
                                                            
                                                            </div>
                                                            
                                                            </div>

                                            </div>
                                            <div class="col-md-6">
                                                            <div class="form-group row  {{ $errors->has('user_settings') ? ' has-error' : '' }}">
                                                            <label class="col-md-9 col-form-label" for="text-input"> Asset Inventory</label>
                                                            <div class="col-md-3">

                                                                <input type="checkbox" value="assetinventory"  id="assetinventory" name="task[]">
                                                                                  
                                                            
                                                            
                                                            </div>
                                                            
                                                            </div>

                                            </div>

                                        </div>

                                        
                                        <div class="row">
                                            <div class="col-md-6">
                                                            <div class="form-group row  {{ $errors->has('user_settings') ? ' has-error' : '' }}">
                                                            <label class="col-md-9 col-form-label" for="text-input">Check-IN/Issuance </label>
                                                            <div class="col-md-3">

                                                                <input type="checkbox" value="assetisssuance"  id="assetisssuance" name="task[]">
                                                                
                                                            </div>
                                                            
                                                            </div>

                                            </div>
                                            <div class="col-md-6">
                                                            <div class="form-group row  {{ $errors->has('user_settings') ? ' has-error' : '' }}">
                                                            <label class="col-md-9 col-form-label" for="text-input"> User Setting </label>
                                                            <div class="col-md-3">

                                                                <input type="checkbox" value="usersetting"  id="usersetting" name="task[]">
                                                                                  
                                                            
                                                            
                                                            </div>
                                                            
                                                            </div>

                                            </div>

                                        </div>
                                    
                                       
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                        <div class="col-md-8 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                <i class="fa fa-add"></i> Submit
                                            </button>
                                        </div>
                                    </div>
                            </form>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>  
            </div>
        </div>
    </section>
</div>



@endsection