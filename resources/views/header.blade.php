<!-- Main Header -->
<head>
<link rel="stylesheet" href="{{asset("/custom.css")}}">
<link rel="stylesheet" href="{{ asset('css/custom.css') }}">

</header>
<header  class="main-header">
    <!-- Logo -->
    <a href="{{ route('admin.home') }}" class="logo"><b>System </b>Portal</a>

    <!-- Header Navbar -->
    <nav  class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="{{ route('admin.home') }}" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        {{-- <form class="form-inline my-2 my-lg-0 pull-left" style="margin-top:10px; margin-left:30%">
                <input class="form-control mr-sm-2" type="search" placeholder="search asset" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit"><i class="fa fa-search" aria-hidden="true"></i>
                </button>
        </form>  --}}
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
                
            <ul class="nav navbar-nav">
                                   <!-- User Account Menu -->
                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!-- The user image in the navbar-->
                        <img src="/storage/images/admins/{{Auth::user()->avartar}}" alt="admin-image" class="user-image img-thumbnail" style="border-radius:50%;">

                        {{-- <img sr class="user-image" alt="User Image"/> --}}
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-xs"> {{ Auth::user()->name }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- The user image in the menu -->
                        <li class="user-header">
                                <img src="/storage/images/admins/{{Auth::user()->avartar }}" alt="admin-image" class="img-thumbnail" style="border-radius:50%;">
                                <p>
                                {{ Auth::user()->name }}
                                <small>System Administrator</small>
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <li class="user-body">
                            <div class="col-xs-4 text-center">
                                <!--   <a href="#">Followers</a> -->
                            </div>
                            <div class="col-xs-4 text-center">
                                <!--   <a href="#">Sales</a>-->
                            </div>
                            <div class="col-xs-4 text-center">
                                <!--  <a href="#">Friends</a> -->
                            </div>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{ route('admin.profile') }}" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{ route('admin_logout') }}" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>