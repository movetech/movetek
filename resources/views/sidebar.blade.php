<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <img src="{{asset('assets/images/bg1.jpg')}}" alt="county-logo" class="img-thumbnail" style="width:90%;height:150px;border-radius:50%;position:center; margin:10px 0">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                {{-- {{ Auth::user()->avartar }} --}}
                <img src="/storage/images/admins/{{Auth::user()->avartar}}" alt="admin-image" class="img-thumbnail" style="border-radius:50%;">
                {{-- <img src="{{ asset("/img/prof.jpg") }}" class="img-circle" alt="User Image" /> --}}
            </div>
            <div class="pull-left info">
                {{ Auth::user()->name }}<br>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>


        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="active"><i class="fa fa-dashboard"></i> <a href="{{ route('admin.home') }}"><span>Home</span></a></li>
            
               
            </li>
                <li class="treeview">
                    <a href="#"> <i class="fa fa-users"></i> <span>Staff</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('members.create') }}"><i class="fa fa-circle-o"></i> Add Staff</a></li>
                        <li><a href="{{ route('members.all') }}"><i class="fa fa-circle-o"></i> View Staff</a></li>
    
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"> <i class="fa fa-lock"></i> <span>Roles</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('roles.create') }}"><i class="fa fa-circle-o"></i> Create New</a></li>
                        <li><a href="{{ route('roles.all') }}"><i class="fa fa-circle-o"></i> View Roles</a></li>
                        <li><a href="{{ route('assigntask') }}"><i class="fa fa-circle-o"></i> Assign Roles</a></li>

                    <li><a href="{{ route('createadmin') }}"><i class="fa fa-circle-o"></i> Add Admin</a></li>
                    <li><a href="{{ route('admins') }}"><i class="fa fa-circle-o"></i> View Admin</a></li>

                    </ul>
                </li>
    
           
            <li class="treeview">
                    <a href="#"><i class="fa fa-list-alt"></i><span>Assets Management</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                            <li><a href="{{ route('assets.create') }}"><i class="fa fa-plus"></i> Create New Asset</a></li>
                            <li><a href="{{route('inventory.view')}}"><i class="fa fa-circle-o"></i> Asset List</a></li>
    
                            <li><a href="{{route('assets.inventory')}}"><i class="fa fa-circle-o"></i> Create Stock(Inventory)</a></li>
                            <li><a href="{{ route('assets.list') }}"><i class="fa fa-list-alt"></i> Asset Stock</a></li>
                            <li><a href="{{route('assets.assign')}}"><i class="fa fa-circle-o"></i> Issuance(Check-in)</a></li>
    
                            <li><a href="{{route('assets.show.issued')}}"><i class="fa fa-circle-o"></i> All Deployed</a></li> 
                            <li><a href="{{route('assets.returned')}}"><i class="fa fa-circle-o"></i> All Returned</a></li>                           
                           <li><a href="{{ route('list-all-non-deployable-assets') }}"><i class="fa fa-circle-o"></i> All un-deployable</a></li>
                    </ul>
                </li>
              
            <li class="treeview">
                    <a href="#"> <i class="fa fa-book"></i> <span>Project Management</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('projects.create') }}"><i class="fa fa-circle-o"></i> Create New</a></li>
                        <li><a href="{{ route('projects.showall') }}"><i class="fa fa-circle-o"></i> All Projects</a></li>
                        <li><a href="{{ route('projects.completed') }}"><i class="fa fa-circle-o"></i> Completed Projects</a></li>
                        <li><a href="{{ route('projects.ongoing') }}"><i class="fa fa-circle-o"></i> Ongoing Projects</a></li>
                    </ul>
                </li>
                <li><a href="{{route('total-expense')}}"> <i class="fa fa-money"></i> <span>Expenditure</span></a></li>           
            <li class="treeview">
                <a href="#"> <i class="fa fa-wrench"></i> <span>Settings</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('categories.index') }}"><i class="fa fa-circle-o"></i> Categories</a></li>
                    <li class="treenview">
                            <a href=""><i class="fa fa-circle-o"></i> Asset Status</a>
                        <ul class="treeview-menu">
                            <li><a href="{{ route('show-asset-status-form') }}"> <i class="fa fa-plus"></i> New Status</a></li>
                            <li><a href="{{ route('get-assest-statuses') }}"><i class="fa fa-list"></i> List All Statuses</a></li>
                            <li></li>
                        </ul>
                        </li>
                    <li><a href="{{ route('manufacturers.index') }}"><i class="fa fa-circle-o"></i> Manufacturers</a></li>
                    <li><a href="{{ route('suppliers.index') }}"><i class="fa fa-circle-o"></i> Suppliers</a></li>
                    <li><a href="{{ route('departments.index') }}"><i class="fa fa-circle-o"></i> Departments</a></li>
                    <li class="treenview">
                        <a href=""><i class="fa fa-circle-o"></i> Ministries</a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('ministry-creation-form') }}"> <i class="fa fa-plus"></i> Create New</a></li>
                        <li><a href="{{ route('list-all-ministries') }}"><i class="fa fa-list"></i> List All</a></li>
                        <li></li>
                    </ul>
                    </li>
                    <li><a href="{{ route('locations.index') }}"><i class="fa fa-circle-o"></i> Sub Counties</a></li>
                    <li><a href="{{ route('create-new-category') }}"><i class="fa fa-plus"></i> New Asset Type</a></li>
                    <li class="treenview">
                            <a href=""><i class="fa fa-calendar"></i> Financial Year</a>
                        <ul class="treeview-menu">
                            <li><a href="{{ route('show-financial-year-creation-form') }}"> <i class="fa fa-plus"></i> Create New</a></li>
                            <li><a href="{{ route('financial-years-list') }}"><i class="fa fa-list"></i> List All</a></li>
                            <li></li>
                        </ul>
                        </li>
                </ul>
            </li>
            
              
            @foreach(explode(',', Auth::user()->task) as $task)
                <?php
                $array=array($task);
                $tasks = array_flip( $array );
                ?>

                @if (isset($tasks['view_report']))

                    <li class="treeview">

                        <a href="#">
                            <i class="fa fa-book"></i> <span>Settings</span>

                            <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                        </span>

                        </a>

                        <ul class="treeview-menu">
                            <li class="active"><a href=""><i class="fa fa-circle-o"></i> Assign Roles</a></li>
                            <li><a href=""><i class="fa fa-circle-o"></i>  Mail Settings</a></li>
                            <li><a href=""><i class="fa fa-circle-o"></i>  Sms Setting</a></li>
                        </ul>
                    </li>
                @else

                @endif

            @endforeach

        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
