@extends('usermainlayout1')

@section('content')
    <div class="content-wrapper">
            <section class=" content-header text-center">
                    <h4><i class="fa fa-asset bg-secondary"></i>
                    Issued Assets
                    </h4>
                  </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-default">
                        <div class="box-header">
                        </div>
                        <div class="box-body">
                            @include('messages.custom')
                            @if(isset($issued_assets))
                            <table class="table table-bordered table-responsive">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Category</th>
                                        <th>Serial N0.</th>
                                        <th>Model No</th>
                                        <th>Asset tag</th>
                                        <th>Sub County</th>
                                        <th>Date issued</th>
                                    </tr>
                                </thead>
                                @foreach($issued_assets as $s)
                                        <tr>
                                            <td>{{ $s->id }}</td>
                                             <td>{{ $s->category->name}} </td>
                                            <td>{{ $s->asset }}</td>
                                            <td>{{ $s->serialnumber}}</td>
                                            <td>{{ $s->modelnumber}}</td>
                                            <td>{{ $s->tag}}</td>
                                            <td>{{ $s->location}}</td>
                                            <td>{{ $s->created_at}}</td>   
                                        </tr>
                                @endforeach
                            </table>
                         @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection