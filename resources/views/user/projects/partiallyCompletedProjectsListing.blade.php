@extends('usermainlayout1')
@section('content')
<link rel="stylesheet" href="{{asset('//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css')}}">
<script src="{{asset('https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js')}}"></script> 
<style>
#btnSearch,
#btnClear{
    display: inline-block;
    vertical-align: top;
    float:left;
    margin:5px;
}
</style>
   <div class="content-wrapper ">
            <section class=" content-header text-center">
                    <h4><i class="fa fa-asset bg-secondary"></i>
                       Ongoing Projects
                      
                    </h4>
                    <div class="row">
                            <div class="col-sm-12 text-center">
                               <a href="{{ route('download-pdf-view-of-partially-completed-projects') }}" class="btn btn-sx btn-danger"><i class="fa fa-arrow-down"></i> Export to pdf</a>
                             </div>
                            </div>
                   
                  </section>
                  @include('messages.custom')
        <section class="content ">
            <div class="row">
                {{-- <div class="col-md-2"></div> --}}
                <div class="col-md-12">
                    <div class="box box-default">  
                        <!-- Search form -->
  <div class="md-form mt-0">
      </div>
                        <div class="container-fluid box-body">
                            @if(count($partially_completed_projects) >0)
                            <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Financial Year</th>
                                        <th>Ministry</th>
                                        <th>Sub County</th>
                                        <th>Department</th>
                                        <th>Contructor</th>
                                        <th>Initial  Cost</th>
                                        <th>Date Awarded</th>
                                        <th>Amount Paid</th>
                                        <th>Balance</th>
                                        <th>Duration</th>
                                        <th>Final Cost</th>
                                        <th>Phases</th>
                                       
                                        
                                    </tr>
                                     </thead>
                              
                                    {{-- <tbody> --}}
                                        <tr>
                                            <tbody class="table-responsive">
                                                @foreach($partially_completed_projects as $project)
                                                {{-- <tbody> --}}
                                                    <tr>
                                               {{-- <tbody> --}}
                                                    <p class="hidden">
                                                            {{$phases=App\Phases::where('REF',$project->REF)->get()}}
                                                            </p>
                                                        <td>{{ $project->id}}</td>
                                                        <td>{{ $project->name}}</td>
                                                        <td>{{ $project->financial_year}}</td>
                                                        <td>{{ $project->ministry }}</td>
                                                        <td>{{ $project->subcounty }}</td>
                                                        <td>{{ $project->department_code }}</td>
                                                        <td>{{ $project->contractor}} </td>
                                                        <td>{{ $project->amount }}</td>
                                                        <td>{{ $project->date_awarded }}</td>
                                                        <td>{{ $project->amount_paid }}</td>
                                                      
                                                        <td>{{ $project->amount_remaining }}</td>
                                                       
                                                        <td>{{ $project->duration }}</td>
                                                        <td>{{ $project->total_budget }}</td>
                                                        
                                                        @if(count($phases)>0)
                                                        <td>{{count($phases)+1}}</td>
                                                        @else
                                                        <td>1</td>
                                                        @endif
                                                       
                                                    </tr>

                                                {{-- </tbody> --}}
                                            @endforeach
                                       
                                        {{-- {{ $completed_projects->links() }} --}}
                                      
                                        @else
                                        <div class="panel panel-success">
                                                <div class="panel-heading"><h4>No Projects Running...</h4></div>
                                              </div>
                                              @endif        
                                            {{-- </tbody> --}}
                                        </tr>
                                       
                            </table>
                        </div>
                    </div>
                </div>
                {{-- <div class="col-md-2"></div> --}}
            </div>
        </section>
    </div>
    <script type="text/javascript">
        // $(document).ready(function(){
            $('#project_name, #ministry, #subcounty, #department, #contractor,#status').on('keyup', function(){
                $value = $(this).val();
                var token = $("meta[name='csrf-token']").attr("content");
                $.ajax({
                    type:"GET",
                    url:'{{ URL::to('admin/projects/search') }}',
                    data:{
                        'search':$value,'_token':token,
                    },
                        success:function(data){
                            $('tbody').html(data);
                        }
                    
                });
            });
        // });
    </script>

@endsection