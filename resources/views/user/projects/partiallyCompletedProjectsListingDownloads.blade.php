<style>
    table {
      width: 100%;
      border: 1px solid #000;
      text-align: left;
    }
    
    th, td {
    padding: 15px;
    text-align: left;
    }
    
    tr:nth-child(even){background-color: #f2f2f2}
    
    th {
      background-color: #4CAF50;
      color: white;
    }
    </style>
<div style="width:100%; height:100px; text-align:center" class="text-center">
    <img src="{{ public_path('/storage/images/logos/mandera-county.jpg')}}" alt="" style="height:50px;align:center;border-radius:50%" style="img-thumbnail">
    <h2 style="font-style:Sans Serif; font-weight:bold; color:brown">MANDERA COUNTY GOVERNMENT</h2>
    <h3 style="center">Summary of All Partially Completed Projects</h3>
</div>
<div style="float:left">
    <h5>Financial Year:{{ $financial_year->year }}</h5>
</div>
                    <table class="table table-bordered" style="width:100%">
                        <thead>      
                            <tr>
                                <th>SR/NO</th>
                                <th>Name</th>
                                <th>Financial Year</th>
                                <th>Ministry</th>
                                <th>Sub County</th>
                                <th>Department</th>
                                <th>Contractor</th>
                                <th>Initial  Cost</th>
                                <th>Date Awarded</th>
                                <th>Amount Paid</th>
                                <th>Balance</th>
                                <th>Duration</th>
                                <th>Final Cost</th>
                            </tr>
                             </thead>
                                <tr>
                                    <tbody class="table-responsive">
                                        @foreach($projects as $project)
                                            <tr>
                                                <td>{{ $project->id}}</td>
                                                <td>{{ $project->name}}</td>
                                                <td>{{ $project->financial_year}}</td>
                                                <td>{{ $project->ministry }}</td>
                                                <td>{{ $project->subcounty }}</td>
                                                <td>{{ $project->department_code }}</td>
                                                <td>{{ $project->contractor}} </td>
                                                <td>{{ $project->amount }}</td>
                                                <td>{{ $project->date_awarded }}</td>
                                                <td>{{ $project->amount_paid }}</td>
                                                <td>{{ $project->amount_remaining }}</td>
                                                <td>{{ $project->duration }}</td>
                                                <td>{{ $project->total_budget }}</td>
                                            </tr>
                                    @endforeach
                                </tr>
                        </table>
                 

