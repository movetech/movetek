@extends('mainlayout')
@section('content')
<link rel="stylesheet" href="{{asset('//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css')}}">
<script src="{{asset('https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js')}}"></script> 

   <div class="content-wrapper ">
            <section class=" content-header text-center">
                    <h4><i class="fa fa-asset bg-secondary"></i>
                        Admin Profile
                      
                    </h4>
                    
            </section>
        <section class="content ">
            <div class="row">
                 <div class="col-md-2"></div>
                <div class="col-md-8">
                        @include('messages.custom')
                    <div class="box box-default row">  
                       <div class="col-md-6">
                           <img src="/storage/images/admins/{{Auth::user()->avartar}}" alt="admin-image" class="img-thumbnail" style="border-radius:50%;">
                       </div>
                       <div class="col-md-6 row">
                            <form action="{{ route('update-profile', $user->id)}}" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="form-group row">
                                        <label for="name" class="col-md-4 form-label text-md-right">Name</label>
                                        <div class="col-md-8">
                                            <input type="text" name="name" value="{{ $user->name }}" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="email" class="col-md-4 form-label text-md-right">Email</label>
                                        <div class="col-md-8">
                                            <input type="email" name="email" class="form-control" value="{{ $user->email }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="photo" class="col-md-4 form-label text-md-right">Upload Photo</label>
                                        <div class="col-md-8">
                                            <input type="file" name="photo" class="" placeholder="Upload">
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-md-offset-4">
                                        <form action="{{ route('update-profile', $user->id) }}" method="POST">
                                            <input type="hidden" name="_method" value="PUT">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="submit" value="Update" class="btn btn-xs btn-info">
                                            <a href="{{ route('admin.home') }}" class="btn btn-danger btn-xs pull-right"><i class="fa fa-arrow-left"></i> Back</a>
                                        </form>
                                    </div>
                                </form>
                        <div class="col-md-2">

                        </div>
                           
                       </div>
                    </div>
                </div>
  
        </section>
    </div>
   

@endsection