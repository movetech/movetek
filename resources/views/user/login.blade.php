@extends('loginlayout')
@section('content')

    <div class="log-w3">
        <div class="w3layouts-main">
            <h2>Sign In Now</h2>

            <div class="flash-message"><!--'danger', 'warning', 'success'-->
                @foreach (['success'] as $msg)
                    @if(Session::has('alert-' . $msg))
                    <div class="alert alert-{{ $msg }}" role="alert">
                        <!-- <p class="alert alert-{{ $msg }}"> -->
                        {{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <!-- </p> -->
                    </div>
                    @endif
                @endforeach
            </div> <!-- end .flash-message -->

            <!-- <div class="alert alert-info" role="alert">
					<strong>Heads up!</strong> This alert needs your attention, but it's not super important.
				</div> -->

                <form method="POST" action="{{ route('submit_user_login') }}">
                        <img src="{{ asset('assets/images/bg1.jpg') }}" alt="Mandera-county" class="" style="width:80%; height:5%; border-radius:50%; margin:0px 20px 0px 30px">
                {{ csrf_field() }}
                    <input type="name" class="ggg" name="name" placeholder="Username" required="">
                    <input type="password" class="ggg" name="password" placeholder="PASSWORD" required="">
                    <span><input type="checkbox" /> Remember Me</span>
                    <h6><a href="#"><font style='font-size:15px'>Forgot Password?</font></a></h6>
                        <div class="clearfix"></div>
                        <input type="submit" value="Sign In" name="login">
                </form>
                <p>Don't Have an Account ?<a href="{{ route('userregister') }}">Create an account</a></p>
        </div>
    </div>




@endsection
