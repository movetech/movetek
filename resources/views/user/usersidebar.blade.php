<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <img src="{!! asset('assets/images/bg1.jpg') !!}" alt="county-logo" class="img-thumbnail" style="width:100%;height:200px;border-radius:50%;position:center; margin:10px 0">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
             {{-- {{ Auth::user()->avartar }}  --}}
                <img src="/storage/images/users/{{Auth::user()->avartar}}" alt="user-image" class="img-thumbnail" style="border-radius:50%;">
                {{-- <img src="{{ asset("/img/prof.jpg") }}" class="img-circle" alt="User Image" />  --}}
            </div>
            <div class="pull-left info">
                {{ Auth::user()->name }}<br>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="active"><i class="fa fa-dashboard"></i> <a href=""><span>Home</span></a></li>

            <li class="header">Main Menu</li>
            <li class="treeview">
                <a href="#"><i class="fa fa-list-alt"></i><span>Assets</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('issued-assets') }}"><i class="fa fa-circle-o"></i> Issued</a></li>
            </li>
                </ul>
            </li>           
            <li class="treeview">
                    <a href="#"> <i class="fa fa-book"></i> <span>Project Management</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('projects.all') }}"><i class="fa fa-circle-o"></i> All Projects</a></li>
                        <li><a href="{{ route('list-all-completed-projects') }}"><i class="fa fa-circle-o"></i> Completed Projects</a></li>
                        <li><a href="{{ route('list-of-all-ongoing-projects') }}"><i class="fa fa-circle-o"></i> Ongoing Projects</a></li>
                    </ul>
                </li>
            @foreach(explode(',', Auth::user()->task) as $task)
                <?php
                $array=array($task);
                $tasks = array_flip( $array );
                ?>

                @if (isset($tasks['view_report']))

                    <li class="treeview">

                        <a href="#">
                            <i class="fa fa-book"></i> <span>Settings</span>

                            <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                        </span>

                        </a>

                        <ul class="treeview-menu">
                            <li class="active"><a href=""><i class="fa fa-circle-o"></i> Assign Roles</a></li>
                            <li><a href=""><i class="fa fa-circle-o"></i>  Mail Settings</a></li>
                            <li><a href=""><i class="fa fa-circle-o"></i>  Sms Setting</a></li>
                        </ul>
                    </li>
                @else

                @endif

            @endforeach

        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
