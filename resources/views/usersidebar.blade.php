<aside>
    <div id="sidebar" class="nav-collapse">
        <!-- sidebar menu start-->
        <div class="leftside-navigation">
            <ul class="sidebar-menu" id="nav-accordion">
                <li>
                    <a class="active" href="index.html">
                        <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
            
                <li class="sub-menu">
                    <a href="javascript:;">
                        <i class="fa fa-envelope"></i>
                        <span>Assets </span>
                    </a>
                    <ul class="sub">
                        <li><a href="mail.html">View Assigned Assets</a></li>
                        {{-- <li><a href="mail_compose.html">Compose Mail</a></li> --}}
                    </ul>
                </li>
                <li class="sub-menu">
                    <a href="javascript:;">
                        <i class=" fa fa-bar-chart-o"></i>
                        <span>Projects</span>
                    </a>
                    <ul class="sub">
                        <li><a href="chartjs.html">All Projects</a></li>
                        <li><a href="flot_chart.html">Completed Projects</a></li>
                    </ul>
                </li>
               
                <li>
                    <a href="login.html">
                        <i class="fa fa-user"></i>
                        <span>Login Page</span>
                    </a>
                </li>
            </ul>            </div>
        <!-- sidebar menu end-->
    </div>
</aside>