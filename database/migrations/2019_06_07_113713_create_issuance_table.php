<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIssuanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('issuance', function (Blueprint $table) {
            $table->string('name')->after('id');
            $table->string('category');
            $table->string('asset');
            $table->string('modelnumber');
            $table->string('serialnumber');
            $table->string('location');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('issuance', function (Blueprint $table) {
            $table->string('name')->after('id');
            $table->string('category');
            $table->string('asset');
            $table->string('modelnumber');
            $table->string('serialnumber');
            $table->string('location');
        });
    }
}
