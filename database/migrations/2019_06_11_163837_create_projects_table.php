<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('projects', function (Blueprint $table) {
            $table->string('name')->after('id');
            $table->string('description');
            $table->string('financial_year');
            $table->string('contractor');
            $table->string('amount');
            $table->string('date_awarded');
            $table->string('amount_paid');
            $table->string('date_paid');
            $table->string('amount_remaining');
            $table->string('date_topay');
            $table->string('duration');
            $table->string('status');
            $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('projects', function (Blueprint $table) {
            $table->string('name')->after('id');
            $table->string('description');
            $table->string('financial_year');
            $table->string('contractor');
            $table->string('amount');
            $table->string('date_awarded');
            $table->string('amount_paid');
            $table->string('date_paid');
            $table->string('amount_remaining');
            $table->string('date_topay');
            $table->string('duration');
            $table->string('status');
            $table->timestamps();
            });
    }
}
