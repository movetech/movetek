<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReturnedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('returned', function (Blueprint $table) {
            $table->string('name')->after('id');
            $table->string('category');
            $table->string('asset');
            $table->string('modelnumber');
            $table->string('serialnumber');
            $table->string('location');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('returned', function (Blueprint $table) {
            $table->string('name')->after('id');
            $table->string('category');
            $table->string('asset');
            $table->string('modelnumber');
            $table->string('serialnumber');
            $table->string('location');
            $table->timestamps();
        });
    }
}
