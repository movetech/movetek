<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAssetsFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assets', function (Blueprint $table) {
            $table->string('name')->after('id');
            $table->integer('available_balance')->after('name')->default(0);
            $table->integer('given_out')->after('available_balance')->nullable();
            $table->integer('total')->after('given_out')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assets', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->dropColumn('available_balance');
            $table->dropColumn('given_out');
            $table->dropColumn('total');
        });
    }
}
