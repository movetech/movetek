<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            'name' => Str::random(10),
            'email' => Str::random(10).'@gmail.com',
            'password' => bcrypt('12345678'),
            'token' => Str::random(10),
            'task' => Str::random(10),
            'status' => Str::random(10),
            'remember_token' => Str::random(10),
        ]);  //
    }
}
