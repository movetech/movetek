<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });



Auth::routes();

//Route::get('userlogin/smsbalance', 'HomeController@emailtemplate')->name('emailtemplate');
Route::get('/', 'HomeController@index')->name('welcomehome');

Route::get('/userregister',  'User\RegisterController@registerform')->name('userregister');
Route::post('/userregister', 'User\RegisterController@submit_registereduser')->name('submit_user_details');
Route::get('/userlogin',  'User\LoginController@loginform')->name('userlogin');
//Route::post('/userlogin', 'User\LoginController@confirm_login')->name('submit_conf_irm_token');
Route::post('/userlogin', 'User\LoginController@login')->name('submit_user_login');
//Route::get('/',  'User\LoginController@loginform')->name('userlogin');
//Route::post('/', 'User\LoginController@login')->name('submit_user_login');
Route::get('/userlogout',  'User\LoginController@logout')->name('userlogout');

//refresh captcha****************************
Route::get('refresh-captcha', 'User\RegisterController@refreshCaptcha')->name('refresh_captcha');

Route::get('/user-activation/{token}', 'User\LoginController@showactivateaccform')->name('show_activate_user_acc');
Route::post('/user-activation', 'User\LoginController@activateacc')->name('activate_user_acc');

//usere reset password *************************************
Route::get('/user-resetpassword',  'User\LoginController@user_reset')->name('user_reset');
Route::post('/user-resetpassword',  'User\LoginController@submit_user_reset')->name('submit_password_reset_from_user');
Route::get('/user-newpassword/{token}', 'User\LoginController@newpassworddd')->name('user_mail_resetpassword');
Route::post('/user-newpassword', 'User\LoginController@updatepassword')->name('user_submit_pass_reset');

Route::get('/acctivate-pending-accs',  'User\LoginController@showactivateformforpendingaccounts')->name('activateaccountforpendingapprovals');
Route::post('/acctivate-pending-accs',  'User\LoginController@saveactivateformforpendingaccounts')->name('submitactivateaccountforpendingapprovals');


//user login routes
Route::get('home', 'HomeController@index')->name('home');
Route::get('/home/logout', 'Auth\LoginController@userlogout')->name('user.logout');


//admin login routes
Route::get('/adminlogin',  'Admin\LoginController@showLoginForm')->name('admlogin');
Route::post('/adminlogin', 'Admin\LoginController@login')->name('admin_login');
Route::get('/logout', ['as' => 'admin_logout', 'uses' => 'Admin\LoginController@logout']);


Route::get('/passwordreset', ['as' => 'password_reset', 'uses' => 'Admin\LoginController@showresetpassword']);
Route::post('/resetpassword', 'Admin\LoginController@resetpass')->name('submit_password_reset');
Route::get('/newpassword/{email}/{token}', 'Admin\LoginController@newpassworddd')->name('mail_resetpassword');
Route::post('/newpassword', 'Admin\LoginController@updatepassword')->name('submit_pass_reset');


Route::prefix('admin')->group(function (){

    Route::get('/home', 'AdminController@index')->name('admin.home');
    //=========================== ASSETS ========================
    Route::get('/assets',['as'=>'assets.list','uses'=>'Assets\AssetsController@index']);
    Route::post('/asset',['as'=>'assets.post','uses'=>'Assets\AssetsController@store']);
    Route::put('/asset/{id}',['as'=>'asset.update','uses'=>'Assets\AssetsController@update']);
    Route::delete('/asset/{id}',['as'=>'asset.delete','uses'=>'Assets\AssetsController@destroy']);
    Route::get('/asset/{id}',['as'=>'asset.show','uses'=>'Assets\AssetsController@show']);
    Route::get('/asset/{id}/edit',['as'=>'asset.edit','uses'=>'Assets\AssetsController@edit']);
    Route::get('/assets/create',['as'=>'assets.create','uses'=>'Assets\AssetsController@create']);
    Route::get('/assets/assign',['as'=>'assets.assign','uses'=>'Assets\AssetsController@checkin']);
    Route::post('/assets',['as'=>'assets.Issue','uses'=>'Assets\AssetsController@Issue']);
    Route::get('/assets/issued',['as'=>'assets.show.issued','uses'=>'Assets\AssetsController@showIssueList']);
    Route::delete('/assets/issued/{id}',['as'=>'delete-issued-asset','uses'=>'Assets\AssetsController@removeIssuedAsset']);
    Route::get('/assets/issue/{id}',['as'=>'assets.each','uses'=>'Assets\AssetsController@viewEachIssuance']);
    Route::post('/assets/checkout',['as'=>'assets.checkout','uses'=>'Assets\AssetsController@checkOut']);
    Route::post('/assets/excel',['as'=>'importexcel','uses'=>'Assets\AssetsController@import']);
    Route::get('/assets/returned',['as'=>'assets.returned','uses'=>'Assets\AssetsController@showReturned']);
    Route::get('/assets/search',['as'=>'assets.search','uses'=>'Assets\AssetsController@search']);
    Route::get('/assets/searchall',['as'=>'assets.searchall','uses'=>'Assets\AssetsController@searchAll']);
    Route::get('/assets/inventory',['as'=>'assets.inventory','uses'=>'Assets\AssetsController@Inventory']);
    Route::post('/inventory/post',['as'=>'inventory.post','uses'=>'Assets\AssetsController@InventoryPost']);
    Route::get('/inventory/view',['as'=>'inventory.view','uses'=>'Assets\AssetsController@InventoryShow']);
    Route::get('/inventory/{id}/edit',['as'=>'inventory.edit','uses'=>'Assets\AssetsController@editInventory']);
    Route::put('/inventory/{id}',['as'=>'inventory.update','uses'=>'Assets\AssetsController@updateInventory']);
    Route::get('/inventory/{id}',['as'=>'inventory.detail','uses'=>'Assets\AssetsController@inventoryDetail']);
    Route::delete('/inventory/{id}',['as'=>'inventory.delete','uses'=>'Assets\AssetsController@deleteInventory']);
    Route::get('/searchassets',['as'=>'admin.searchassets','uses'=>'Assets\AssetsController@searchreturned']);
    Route::get('/assets/type/create',['as'=>'create-new-category','uses'=>'Assets\AssetsController@createNewAssetType']);
    Route::post('/assets/type',['as'=>'new-asset-type','uses'=>'Assets\AssetsController@addNewAssetType']);
    Route::get('/assets/non-deployable',['as'=>'list-all-non-deployable-assets', 'uses'=>'Assets\AssetsController@listAllNonDeployableAssets']);
    //pdf file download urls
    Route::get('/assets/downloads/all','Assets\AssetsController@downloadAllAssets')->name('download-all-assets');
    Route::get('/assets/downloads/returned','Assets\AssetsController@downloadReturnedAssets')->name('download-all-returned-assets');
    Route::get('/assets/downloads/issued','Assets\AssetsController@pdfDownloadOfIssuedAssets')->name('download-all-issued-assets');
    Route::get('/assets/downloads/stock','Assets\AssetsController@pdfDownloadOfStockedAssets')->name('download-all-stocked-assets');

    /************************* Asset statuses  ******************/
    Route::post('/assets/status',['as'=>'new-asset-status','uses'=>'Assets\AssetsController@addNewAssetStatus']);
    Route::get('/assets/status',['as'=>'get-assest-statuses','uses'=>'Assets\AssetsController@getAssetStatuses']);
    Route::get('/asset/status/create',['as'=>'show-asset-status-form','uses'=>'Assets\AssetsController@showAssetStatusForm']);
    Route::put('/assets/status/{id}',['as'=>'update-asset-status','uses'=>'Assets\AssetsController@updateAssetStatus']);
    Route::delete('/asset/status/{id}',['as'=>'delete-existing-asset-status','uses'=>'Assets\AssetsController@removeExistingAssetStatus']);
    Route::get('/asset/status/{id}',['as'=>'show-asset-status-details','uses'=>'Assets\AssetsController@getAssetStatus']);
    Route::get('/asset/status/{id}/edit',['as'=>'show-asset-status-edit-form','uses'=>'Assets\AssetsController@showAssetStatusEditForm']);
    //asset total budget plus project
    Route::get('/assets/expense',['as'=>'total-expense','uses'=>'AssetExpenseController@showEpenses']);
    Route::get('/assets/year/{year}',['as'=>'year','uses'=>'AssetExpenseController@filterYears']);
   
    
    /************************* End Asset Statuses ***************/
    //============================= END ASSETS =========================
//================== SETTINGS ==============================
    //Categories routes
    Route::get('settings/categories',['as'=>'categories.index','uses'=>'Settings\CategoriesController@index']);
    Route::get('settings/categories/data',['as'=>'categories.data','uses'=>'Settings\CategoriesController@get_category_data']);
    Route::post('settings/categories',['as'=>'categories.post','uses'=>'Settings\CategoriesController@store']);
    Route::delete('settings/category/{id}',['as'=>'category.delete','uses'=>'Settings\CategoriesController@destroy']);
    Route::put('settings/category/{id}',['as'=>'category.update','uses'=>'Settings\CategoriesController@update']);
    Route::get('settings/category/{id}/edit',['as'=>'category.edit','uses'=>'Settings\CategoriesController@edit']);
    Route::get('settings/category/{id}',['as'=>'category.show','uses'=>'Settings\CategoriesController@show']);
    Route::get('settings/categories/create',['as'=>'categories.create','uses'=>'Settings\CategoriesController@create']);

    //Manufacturers routes
    Route::get('settings/manufacturers',['as'=>'manufacturers.index','uses'=>'Settings\ManufacturersController@index']);
    Route::post('settings/manufacturers',['as'=>'manufacturers.post','uses'=>'Settings\ManufacturersController@store']);
    Route::delete('settings/manufacturer/{id}','Settings\ManufacturersController@destroy')->name('manufacturer.delete');
    Route::put('settings/manufacturer/{id}',['as'=>'manufacturer.update','uses'=>'Settings\ManufacturersController@update']);
    Route::get('settings/manufacturer/{id}',['as'=>'manufacturer.show','uses'=>'Settings\ManufacturersController@show']);
    Route::get('settings/manufacturer/{id}/edit',['as'=>'manufacturer.edit','uses'=>'Settings\ManufacturersController@edit']);
    Route::get('settings/manufacturers/create',['as'=>'manufacturers.create','uses'=>'Settings\ManufacturersController@create']);

    //Suppliers routes
    Route::get('settings/suppliers',['as'=>'suppliers.index','uses'=>'Settings\SuppliersController@index']);
    Route::post('settings/suppliers',['as'=>'suppliers.post','uses'=>'Settings\SuppliersController@store']);
    Route::delete('settings/supplier/{id}',['as'=>'supplier.delete','uses'=>'Settings\SuppliersController@destroy']);
    Route::put('settings/supplier/{id}',['as'=>'supplier.update','uses'=>'Settings\SuppliersController@update']);
    Route::get('settings/supplier/{id}','Settings\SuppliersController@show')->name('supplier.show');  
    Route::get('settings/supplier/{id}/edit',['as'=>'supplier.edit','uses'=>'Settings\SuppliersController@edit']);
    Route::get('settings/suppliers/create',['as'=>'suppliers.create','uses'=>'Settings\SuppliersController@create']);

    //Departments routes
    Route::get('settings/departments',['as'=>'departments.index','uses'=>'Settings\DepartmentsController@index']);
    Route::post('settings/departments','Settings\DepartmentsController@store')->name('departments.post');
    Route::delete('settings/department/{id}','Settings\DepartmentsController@destroy')->name('department.delete');
    Route::put('settings/department/{id}','Settings\DepartmentsController@update')->name('department.update');
    Route::get('settings/department/{id}','Settings\DepartmentsController@show')->name('department.show');
    Route::get('settings/department/{id}/edit','Settings\DepartmentsController@edit')->name('department.edit');
    Route::get('settings/departments/create',['as'=>'departments.create','uses'=>'Settings\DepartmentsController@create']);
    Route::get('settings/departments/data',['as'=>'departments.table-data','uses'=>'Settings\DepartmentsController@get_table_data']);

    //Locations/SubCounites routes
    Route::get('settings/subcounties',['as'=>'locations.index','uses'=>'Settings\LocationsController@index']);
    Route::post('settings/subcounties',['as'=>'locations.post','uses'=>'Settings\LocationsController@store']);
    Route::delete('/settings/subcounty/{id}',['as'=>'location.delete','uses'=>'Settings\LocationsController@destroy']);
    Route::put('/settings/subcounty/{id}',['as'=>'location.update','uses'=>'Settings\LocationsController@update']);
    Route::get('/settings/subcounty/{id}',['as'=>'location.show','uses'=>'Settings\LocationsController@show']);
    Route::get('/settings/subcounties/{id}/edit',['as'=>'location.edit','uses'=>'Settings\LocationsController@edit']);
    Route::get('/settings/subcounties/create',['as'=>'locations.create','uses'=>'Settings\LocationsController@create']);
    
    //Ministries
    Route::get('/settings/ministries',['as'=>'list-all-ministries','uses'=>'Settings\MinistryController@listAllMinistries']);
    Route::post('/settings/ministry',['as'=>'add-new-ministry','uses'=>'Settings\MinistryController@addNewMinistry']);
    Route::get('/settings/ministry/{id}',['as'=>'show-single-ministry','uses'=>'Settings\MinistryController@ministryDetail']);
    Route::get('/settings/ministries/create',['as'=>'ministry-creation-form','uses'=>'Settings\MinistryController@showMinistryCreationForm']);
    Route::get('/settings/ministry/{id}/edit',['as'=>'show-ministry-edit-form','uses'=>'Settings\MinistryController@showMinistryEditForm']);
    Route::put('/settings/ministry/{id}',['as'=>'update-existing-ministry','uses'=>'Settings\MinistryController@modifyMinistryDetails']);
    Route::delete('/settings/ministry/{id}',['as'=>'delete-existing-ministry','uses'=>'Settings\MinistryController@removeMinistry']);
    Route::get('/settings/ministries/search',['as'=>'search-ministry','uses'=>'Settings\MinistryController@searchMinistry']);

    //Asset Statuses
    Route::get('/save-status','Projects/ProjectController@saveProjectStatus')->name('save-status');

    //Financial Years
    Route::get('/settings/financial-years/create',['as'=>'show-financial-year-creation-form','uses'=>'Assets\AssetsController@yearCreationForm']);
    Route::post('/settings/financial-years',['as'=>'add-new-financial-year','uses'=>'Assets\AssetsController@addNewFinancialYear']);
    Route::get('/settings/financial-years','Assets\AssetsController@getAllFinancialYears')->name('financial-years-list');
    Route::delete('/settings/financial-years/{id}','Assets\AssetsController@removeExistingFinancialYear')->name('delete-financial-year');
    Route::post('/settings/financial-years/update',['as'=>'update-financial-year','uses'=>'Assets\AssetsController@updateExistingFinancialYear']);
//================== End SETTINGS  ==========================

//===================== MEMBERSHIP urls ===========================
    Route::get('/members/data',['as'=>'members.data.all','uses'=>'Members\MembersController@get_all_members']);
    Route::get('/members',['as'=>'members.all','uses'=>'Members\MembersController@index']);
    Route::post('/members',['as'=>'members.post','uses'=>'Members\MembersController@store']);
    Route::get('/member/{id}',['as'=>'member.show','uses'=>'Members\MembersController@show']);
    Route::get('/members/create',['as'=>'members.create','uses'=>'Members\MembersController@create']);
    Route::get('/member/{id}/edit',['as'=>'member.edit','uses'=>'Members\MembersController@edit']);
    Route::put('/member/{id}',['as'=>'member.update','uses'=>'Members\MembersController@update']);
    Route::delete('/member/{id}',['as'=>'member.delete','uses'=>'Members\MembersController@destroy']);

//=================== END MEMBERSHIP  urls===========================


//***************************** ROLES  *************************************


    Route::get('/roles','Roles\RolesController@index')->name('roles.all');
    Route::post('/roles','Roles\RolesController@store')->name('roles.post');
    Route::get('/role/{id}','Roles\RolesController@show')->name('role.detail');
    Route::get('/role/{id}/edit','Roles\RolesController@edit')->name('role.edit');
    Route::get('roles/create','Roles\RolesController@create')->name('roles.create');
    Route::delete('/role/{id}','Roles\RolesController@destroy')->name('role.delete');
    Route::put('/role/{id}','Roles\RolesController@update')->name('role.update');
    Route::post('/roles/assign',['as'=>'roles.assign','uses'=>'Roles\RolesController@assign_roles']);

//***************************** END ROLES ***********************************

    //========================  ROLES AND TASKS ASSIGNMENT ================
    
    //Route::delete('/role-delete','Roles\RolesController@deleterole')->name('deleterole');
    Route::get('/role-assign','Roles\RolesController@assigntask')->name('assigntask');
    Route::post('/role-assign','Roles\RolesController@submittaskrole')->name('submittaskrole');

    //admins
    Route::get('/admins','Members\MembersController@viewAdmins')->name('admins');
    Route::post('/admins','Members\MembersController@addAdmin')->name('addadmin');
    Route::get('/admin/{id}','Members\MembersController@showAdmin')->name('showadmin');
    Route::get('/admin/{id}/edit','Members\MembersController@editAdmin')->name('editadmin');
    Route::put('/admin/{id}','Members\MembersController@updateAdmin')->name('updateadmin');
    Route::delete('/admin/{id}','Members\MembersController@deleteAdmin')->name('deleteadmin');
    Route::get('/admins/create','Members\MembersController@createAdmin')->name('createadmin');


    /******************************* PROJECT MANAGEMENT***************************** */
    Route::get('/projects',['as'=>'all-projects','uses'=>'Projects\ProjectsController@index']);
    Route::get('/project/{id}','Projects\ProjectsController@show')->name('show-project');
    Route::get('/projects/create','Projects\ProjectsController@create')->name('creat-project');
    Route::post('/project',['as'=>'add-project','uses'=>'Projects\ProjectsController@store']);
    Route::put('/projects/{id}',['as'=>'update-project','uses'=>'Projects\ProjectsController@update']);
    Route::get('/projects/pending',['as'=>'pending-projects','uses'=>'Projects\ProjectsController@pending_projects']);
    Route::delete('/project-delete/{id}',['as'=>'delete-project','uses'=>'Projects\ProjectsController@destroy']);
    Route::get('/project/{id}/edit','Projects\ProjectsController@edit')->name('edit-project');
    Route::get('/projects/create',['as'=>'projects.create','uses'=>'Projects\ProjectsController@create']);
    Route::get('/projects/showpending',['as'=>'projects.showpending','uses'=>'Projects\ProjectsController@showPending']);
    Route::get('/projects/showall',['as'=>'projects.showall','uses'=>'Projects\ProjectsController@showAll']);
    Route::get('/projects/ongoing',['as'=>'projects.ongoing','uses'=>'Projects\ProjectsController@ongoingProjects']);
    Route::get('/projects/delayed',['as'=>'projects.delayed','uses'=>'Projects\ProjectsController@delayedProjects']);
    Route::get('/projects/completed',['as'=>'projects.completed','uses'=>'Projects\ProjectsController@completedProjects']);
    Route::get('/projects/show/{id}',['as'=>'projects.show','uses'=>'Projects\ProjectsController@show']);
    Route::get('/projects/mark/{id}',['as'=>'projects.mark','uses'=>'Projects\ProjectsController@Mark']);
    Route::get('/projects/search',['as'=>'search-all-projects','uses'=>'Projects\ProjectsController@search_project']);
    Route::get('/projects/downloads/all-county-projects', 'Projects\ProjectsController@downloadAllProjects')->name('downAllProjectsInPdf');
    Route::get('/projects/downloads/completed-projects','Projects\ProjectsController@downloadAllCompletedProjects')->name('downloadAllCompletedProjects');
    Route::get('/projects/downloads/ongoing',['as'=>'downloadAllOngoingProjects','uses'=>'Projects\ProjectsController@downloadAllOngoingProjects']);
   
    //********************  Supplementary Budget  ***********************/
    Route::get('/projects/{ref}/{count}/supplementary-budget',['as'=>'supplementary-budget.create','uses'=>'Projects\ProjectsController@create_supplementary_budget']);
    Route::post('/projects/{ref}/supplementary-budget',['as'=>'save-new-supplementary-budget','uses'=>'Projects\ProjectsController@addBudget']);
    //******************** End Supplementary Budget ************************* */
    Route::get('/phase/activate/{id}/{ref}',['as'=>'phase.activate','uses'=>'Projects\ProjectsController@ActivatePayment']);
    Route::get('/phase/markphase/{id}/{ref}',['as'=>'phase.mark','uses'=>'Projects\ProjectsController@MarkPhase']);
    Route::get('/budget/show',['as'=>'budget.show','uses'=>'Projects\ProjectsController@Budget']);
    //Route::get('/budget/report',['as'=>'budget.report','uses'=>'Projects\ProjectsController@pdfviewBudget']);
    Route::get('/budget/report',array('as'=>'budget.report','uses'=>'Projects\ProjectsController@pdfviewBudget'));
    Route::get('/projects/listall','Projects\ProjectsController@index')->name('display-all-projects');
    //======================= PROJECT MANAGEMENT ==============


    //************************* ADMIN PROFILE *******************************/
    Route::get('/profile',['as'=>'admin.profile','uses'=>'Admin\ProfileController@index']);
    Route::put('/profile/{id}','Admin\ProfileController@update')->name('update-profile');

    //************************ END ADMIN PROFILE ******************************/
});

//************************ USER URLS ********************************/
    Route::prefix('user')->group(function (){
    Route::get('/home', 'User\UserController@index')->name('userhome');
    Route::get('/projects',['as'=>'projects.all','uses'=>'User\UserController@showAllProjects']);
    Route::get('/profile','User\UserController@getUserProfile')->name('user-profile');
    Route::get('assets/issued', 'User\UserController@getIssuedAssets')->name('issued-assets');
    Route::put('/profile/{id}',['as'=>'update-user->profile','uses'=>'User\UserController@updateUserProfile']);
    Route::get('/projects/completed',['as'=>'list-all-completed-projects','uses'=>'User\UserController@getAllCompletedProjects']);
    Route::get('/projects/partially-complete',['as'=>'list-of-all-ongoing-projects','uses'=>'User\UserController@listAllPartiallyCompletedProjects']);
    Route::get('/projects/completed/download',['as'=>'download-completed-projects-pdf','uses'=>'User\UserController@downloadPdfViewOfCompletedProjects']);
    Route::get('/prjects/partially-complete/download','User\UserController@downloadPdfViewOfPartiallyCompletedProjects')->name('download-pdf-view-of-partially-completed-projects');

//***********************  ****************************************/
});
/*
Route::get('admin/editor', 'EditorController@index');
Route::get('admin/test', 'EditorController@test');

Route::get('adminn', 'HomeController@index2'); */
?>